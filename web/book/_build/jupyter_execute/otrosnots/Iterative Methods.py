# Iterative Methods for Linear and Nonlinear Systems

In this notebook, we will implement Jacobi and Gauss-Seidel iterative methods. Then, we will use them to approximate the solution of the implicit scheme for the Burgers equation.

To run each of the following cells, use the keyboard shortcut **SHIFT** + **ENTER**, press the button ``Run`` in the toolbar or find the option ``Cell > Run Cells`` from the menu bar. For more shortcuts, see ``Help > Keyboard Shortcuts``.

To get started, import the required Python modules by running the cell below.

# Configuration for visualizing the plots
%matplotlib notebook
%config InlineBackend.figure_format = 'retina'

# Required modules
import numpy as np
from numpy.linalg import inv
import matplotlib.pyplot as plt

# Import figure style and custom functions
import nbtools as nb

## Introduction: Some Useful Numpy Functions

Before diving into the implementation of iterative methods, let's explore some useful functions from ``numpy``. In the following cell, create the array 
\begin{equation}
    A = \begin{bmatrix}
    10 & 2 & 4 \\ 6 & 8 & 4  \\ 2 & 3 & 9 
\end{bmatrix}
\end{equation}

A = np.array([[10, 2, 4], [6, 8, 4], [2, 3, 9]])

We can extract the upper and lower triangles of A using the function ``np.triu`` and ``np.tril``, respectively. Run the following cell to see the output of ``np.triu(A)``.

np.triu(A)

Recall from the theory that we do not need the diagonal values of $A$ in matrix $U$. To achieve this, we can pass an argument to ``np.triu`` to give us the values starting from the ``m``-th diagonal and above. The main diagonal is assumed to be zero. 

Run the following cell where we have chosen ``m=1``

np.triu(A, 1)

Similarly, we can extract the lower triangle of the matrix using ``np.tril`` with ``m=-1``. In the following cell, extract the lower triangle of $A$ to get matrix $L$, as outlined in the textbook.



Similarly, we can extract the diagonal of $A$ using ``np.diag(A)``. As you will observe, the output of this function is a vector. To obtain a matrix with the diagonal entries of $A$, one way is to use ``np.diag(np.diag(A))``.

Run the following cells and observe the output of each function call.

np.diag(A)

np.diag(np.diag(A))

You can compute the inverse of a matrix using the inverse function of numpy. Run the following cell to obtain the inverse of ``A``

inv(A)

## Implementating Jacobi and Gauss-Seidel to solve linear systems
Now, we will write the functions for Jacobi and Gauss-Seidel iteration.

The functions should have the following arguments
- ``A``: matrix $A$,
- ``x0``: initial guess vector $\vec x_0$,
- ``b``: $\vec b$ vector,
- ``tol``: tolerance to admit solution,
- ``imax``: maximum number of iterations, avoids infinite looping.

The output of the functions will be
- ``x``: the approximated solution of the system
- ``i``: the number of performed iterations
- ``error``: the final error 

To compute the error, we provide the following $L_2$-norm

def l2_error(xa, xb):
    return np.mean(np.abs(xa**2 - xb**2))

### Jacobi
In the following cell, complete the ``jacobi`` function. You may use the functions explained above.

def jacobi(A, x0, b, tol, imax=1000):
    # Extract the upper and lower triangles of A
    U = 
    L = 
    
    # Extract diagonal of A and compute D^(-1),
    # taking advantage of its diagonal form
    D = 
    Dinv = np.eye(x0.size)*1/D
    
    
    # Initialize error and counter
    error = 1e16
    i = 0
    while error > tol:
        # Apply Jacobi iteration equation
        x = 
        
        # Compute L_2 error
        error = l2_error(x, x0)
        
        x0 = x
        i += 1
        
        # Avoid infinite loops
        if i >= imax:
            break
    return x, i, error

Consider a system with
\begin{equation}
    A = \begin{bmatrix} 10 & 2 & 4 \\ 6 & 8 & 4  \\ 2 & 3 & 9  \end{bmatrix},\quad
\vec b = \begin{bmatrix}1\\2\\3\end{bmatrix}.
\end{equation}
Using an initial guess 
\begin{equation}
    \vec x = \begin{bmatrix}0\\0\\0\end{bmatrix},
\end{equation}
compute the solution with ``tol=1e-5``. 

A =
b = 
x0 = 
tol = 

# Run Jacobi. (To specify the maximum number of iterations, add the argument imax with the desired number)
x, i_jac, error_jac = jacobi(A, x0, b, tol)

Run the following cell to see how many iterations were necessary to reach the specified tolerance.

f'After {i_jac} iterations, the error using Jacobi is {error_jac}'

### Gauss-Seidel
In the following cell, complete the function to approximate the solution using Gauss-Seidel. The input and output parameters of the function remain the same.

def gauss_seidel(A, x0, b, tol, imax=1000):
    # Extract upper and lower triangles of A
    U = 
    L = 
    
    # Extract diagonal matrix
    D = 
    
    # Compute inverse of (L + D)
    LDinv = inv(L + D)
    
    # Initialize error and counter
    error = 1e16
    i = 0
    while error > tol:
        # Apply Gauss-Seidel equation
        x = 
        
        # Compute L_2 error
        error = l2_error(x, x0)
        x0 = x
        
        i += 1
        if i >= imax:
            break
            
    return x, i, error

Consider the same input parameters as described above and run the following cell to obtain a result using Gauss-Seidel.

# Run Gauss-Seidel. (To specify the maximum number of iterations, add the argument imax with the desired number)
x, i_gs, error_gs = gauss_seidel(A, x0, b, tol)

f'After {i_gs} iterations, the error using Gauss-Seidel is {error_gs}'

You may verify your results with the textbook by setting ``imax=30`` for ``jacobi`` and ``imax=5`` for ``gauss_seidel``.

## Implementing Newton-Raphson to solve nonlinear systems

In this section of the notebook, we demonstrate an application of the iterative methods in the context of the Implicit Burgers equation.

Following the theory in the textbook, compute the Jacobian matrix $A(u)$ by completing the following function

def build_jacobian(u, dt, dx, n):
    A = np.zeros((n, n))
    for i in range(n):
        # Diagonal entries
        A[i, i] = 
        # Off-diagonal entries
        A[i, i - 1] = 
    return A

In addition, we will require the residual function, given by

def residual(u, dx):
    return -0.5*(u**2 - np.roll(u, 1)**2)/dx

To solve the nonlinear system of equations resulting from the Burgers equation, we use the Newton-Raphson method as described in the textbook. In the following cell, complete the ``newton_raphson`` function using a tolerance ``ktol`` and a maximum number of iterations ``kmax``. 

def newton_raphson(u0, dt, dx, n, ktol, kmax=100):
    # Initialize variables
    error = 1e16
    uk = 1.0*u0
    k = 0
    while (error > ktol) and (k < kmax):
        # Obtain residual at k-th iteration
        rk = residual(uk, dx)
        
        # Build RHS vector
        b = 
        
        # Obtain Jacobian matrix at k-th iteration
        Ak = build_jacobian(uk, dt, dx, n)
        
        # Solve linearized system using inv
        du = 
        
        # Obtain solution at k+1-th iteration
        ukk = 
        
        # Compute error at this iteration
        error = np.max(np.abs(du))
        
        uk = ukk
        k += 1
        
    uf = ukk
    return uf

We now run the implicit Burgers problem considering an initial condition $$u(x,0) = e^{-40(x-1/2)^2}$$ on a domain $x\in[0,1]$ and the following parameters
- Number of cells ``n = 100``,
- Time-step size ``dt = 0.0025``,
- Final time ``tf = 0.5``,
- Newton error tolerance ``ktol = 1e-6``.

We will time the solver by using the magic function ``%%time``.

%%time
L =
n = 
dt = 
tf = 
ktol = 

x = np.linspace(0, L, n)
dx = L/n
niter = int(tf/dt)

# Initialize solution
u = np.exp(-40*(x-1/2)**2)

# Advance nonlinear problem
for i in range(niter):
    u = newton_raphson(u, dt, dx, n, ktol)

# Plot solution
plt.plot(x, u)
plt.xlabel('$x$')
plt.ylabel('$u$')

Observe the resulting wall time and increase the number of cells to ``n = 200`` and then to ``n = 400``. How significant is the time increase? 