# Iteraciones
## Bucles usando For

A **for-loop** is a set of instructions that is repeated, or iterated, for every value in a sequence. Sometimes for-loops are referred to as **definite loops** because they have a predefined begin and end as bounded by the sequence. 

The general syntax of a for-loop block is as follows.

**CONSTRUCTION**: For-loop

```python
for looping variable in sequence:
    code block
```

A for-loop assigns the **looping variable** to the first element of the sequence. It executes everything in the code block. Then it assigns the looping variable to the next element of the sequence and executes the code block again. It continues until there are no more elements in the sequence to assign.

**TRY IT!** What is the sum of every integer from 1 to 3?

n = 0
for i in range(1, 4):
    n = n + i
    
print(n)

**WHAT IS HAPPENING?** 

0. First, the function *range(1, 4)* is generating a list of numbers beginning at 1 and ending at 3. Check the description of the function *range* and get familiar with how to use it. In a very simple form, it is *range(start, stop, step)*, and the *step* is optional with 1 as the default. 
1. The variable *n* is assigned the value 0.
2. The variable *i* is assigned the value 1.
3. The variable *n* is assigned the value *n + i* ($0 + 1 = 1$).
4. The variable *i* is assigned the value 2.
5. The variable *n* is assigned the value *n + i* ($1 + 2 = 3$).
6. The variable *i* is assigned the value 3.
7. The variable *n* is assigned the value *n + i* ($3 + 3 = 6$).
8. With no more values to assign in the list, the for-loop is terminated with 
*n = 6*.

We present several more examples to give you a sense of how for-loops work. Other examples of sequences that we can iterate over include the elements of a tuple, the characters in a string, and other sequential data types.

__EXAMPLE:__ Print all the characters in the string `"banana"`.

for c in "banana":
    print(c)

Alternatively, you could use the index to get each character. But it is not as concise as the previous example. Recall that the length of a string could be determined by using the *len* function. And we could ignore the start by only giving one number as the stop. 

s = "banana"
for i in range(len(s)):
    print(s[i])

**EXAMPLE**: Given a list of integers, *a*, add all the elements of *a*.

s = 0
a = [2, 3, 1, 3, 3]
for i in a:
    s += i # note this is equivalent to s = s + i
    
print(s)

The Python function *sum* has already been written to handle the previous example. However, assume you wish to add only the even numbers. What would you change to the previous for-loop block to handle this restriction?

s = 0
for i in range(0, len(a), 2):
    s += a[i]
    
print(s)

**NOTE!** We use *step* as 2 in the *range* function to get the even indexes for list *a*. Also, a Python shortcut that is commonly used is the operator *+=*. In Python and many other programming languages, a statement like *i += 1* is equivalent to *i = i + 1* and same is for other operators as *-=*, *\*=*, */=*.

**Example** Define a dictionary and loop through all the keys and values. 

dict_a = {"One":1, "Two":2, "Three":3}

for key in dict_a.keys():
    print(key, dict_a[key])

In the above example, we first get all the keys using the method *keys*, and then use the key to get access the value. Alternatively, we could use the *item* method in a dictionary, and get the key and value at the same time as show in the following example.

for key, value in dict_a.items():
    print(key, value)

Note that, we could assign two different looping variables at the same time. There are other cases that we could do things similarly. For example, if we have two lists with same length, and we want to loop through them, we could do as the following example using the *zip* function:

a = ["One", "Two", "Three"]
b = [1, 2, 3]

for i, j in zip(a, b):
    print(i, j)

**EXAMPLE:** Let the function *have_digits* has the input as a string. The output *out* should take the value 1 if the string contains digits, and 0 otherwise. You could use the *isdigit* method of the string to check if the character is a digit. 

def have_digits(s):
    
    out = 0
    
    # loop through the string
    for c in s:
        # check if the character is a digit
        if c.isdigit():
            out = 1
            break
            
    return out

out = have_digits('only4you')
print(out)

out = have_digits('only for you')
print(out)

The first step in the function *have_digits* assumes that there are no digits in the string *s* (i.e., the output is 0 or False). 

Notice the new keyword *break*. If executed, the *break* keyword immediately stops the most immediate for-loop that contains it; that is, if it is contained in a nested for-loop, then it will only stop the innermost for-loop. In this particular case, the break command is executed if we ever find a digit in the string. The code will still function properly without this statement, but since the task is to find out if there are any digit in *s*, we do not have to keep looking if we find one. Similarly, if a human was given the same task for a long string of characters, that person would not continue looking for digits if he or she already found one. Break statements are used when anything happens in a for-loop that would make you want it to stop early. A less intrusive command is the keyword *continue*, which skips the remaining code in the current iteration of the for-loop, and continues on to the next element of the looping array. See the following example, that we use the keyword *continue* to skip the *print* function to print 2:

for i in range(5):
    
    if i == 2:
        continue
        
    print(i)

**EXAMPLE:** Let the function *my_dist_2_points(xy_points, xy)*, where the input argument *xy_points* is a list of x-y coordinates of a point in Euclidean space, *xy* is a list that contain an x-y coordinate, and the output *d* is a list containing the distances from *xy* to the points contained in each row of *xy_points*. 

import math

def my_dist_2_points(xy_points, xy):
    """
    Returns an array of distances between xy and the points 
    contained in the rows of xy_points
    
    author
    date
    """
    d = []
    for xy_point in xy_points:
        dist = math.sqrt(\
            (xy_point[0] - xy[0])**2 + (xy_point[1] - xy[1])**2)
        
        d.append(dist)
        
    return d

xy_points = [[3,2], [2, 3], [2, 2]]
xy = [1, 2]
my_dist_2_points(xy_points, xy)

Just like if-statements, for-loops can be nested. 

**EXAMPLE:** Let *x* be a two-dimensional array, [5 6;7 8]. Use a nested for-loop to sum all the elements in *x*. 

x = np.array([[5, 6], [7, 8]])
n, m = x.shape
s = 0
for i in range(n):
    for j in range(m):
        s += x[i, j]
        
print(s)

**WHAT IS HAPPENING?**

1. *s*, representing the running total sum, is set to 0. 
2. The outer for-loop begins with looping variable, i, set to 0.
3. Inner for-loop begins with looping variable, j, set to 0.
4. *s* is incremented by x[i,j] = x[0,0] = 5. So s = 5. 
5. Inner for-loop sets j = 1.
6. *s* is incremented by x[i,j] = x[0,1] = 6. So s = 11. 
7. Inner for-loop terminates.
8. Outer for-loop sets i = 1.
9. Inner for-loop begins with looping variable, j, set to 0.
10. *s* is incremented by x[i,j] = x[1,0] = 7. So s = 18. 
11. Inner for-loop sets j = 1.
12. *s* is incremented by x[i,j] = x[1,1] = 8. So s = 26. 
13. Inner for-loop terminates.
14. Outer for-loop terminates with s = 26.

**WARNING!** Although possible, do not try to change the looping variable inside of the for-loop. It will make your code very complicated and will likely result in errors.

## Bucles usando While

A __while loop__ or __indefinite loop__ is a set of instructions that is repeated as long as the associated logical expression is true. The following is the abstract syntax of a while loop block.

**CONSTRUCTION:** While Loop

```python
while <logical expression>:
    # Code block to be repeated until logical statement is false
    code block
```

When Python reaches a while loop block, it first determines if the logical expression of the while loop is true or false. If the expression is true, the code block will be executed, and after it is executed, the program returns to the logical expression at the beginning of the while statement. If it is false, then the while loop will terminate.

__TRY IT!__ Determine the number of times 8 can be divided by 2 until the result is less than 1.

i = 0
n = 8

while n >= 1:
    n /= 2
    i += 1
    
print(f'n = {n}, i = {i}')

**WHAT IS HAPPENING?**

1. First the variable i (running count of divisions of n by 2) is set to 0.
2. n is set to 8 and represents the current value we are dividing by 2.
3. The while-loop begins.
4. Python computes n >= 1 or 8 >= 1, which is true so the code block is executed.
5. n is assigned n/2 = 8/2 = 4.
6. i is incremented to 1.
7. Python computes n >= 1 or 4 >= 1, which is true so the code block is executed.
8. n is assigned n/2 = 4/2 = 2.
9. i is incremented to 2.
10. Python computes n >= 1 or 2 >= 1, which is true so the code block is executed.
11. n is assigned n/2 = 2/2 = 1.
12. i is incremented to 3.
13. Python computes n >= 1 or 1 >= 1, which is true so the code block is executed.
14. n is assigned n/2 = 1/2 = 0.5.
15. i is incremented to 4.
16. Python computes n >= 1 or 0.5 >= 1, which is false so the while-loop ends with i = 4.

You may have asked, "What if the logical expression is true and never changes?" and this is indeed a very good question. If the logical expression is true, and nothing in the while-loop code changes the expression, then the result is known as an **infinite loop**. Infinite loops run forever, or until your computer breaks or runs out of memory.

**EXAMPLE:** Write a while-loop that causes an infinite loop.

n = 0
while n > -1:
    n += 1

Since *n* will always be bigger than âˆ’1 no matter how many times the loop is run, this code will never end. 

You can terminate the infinite while loop manually by pressing the *interrupt the kernel* - the black square button in the tool bar above, or the drop down menu - *Kernel* - *Interrupt* in the notebook. Or if you are using the Python shell, you need to press *cmd + c* on Mac or *ctrl + c* on PC. 

Can you change a single character so that the while-loop will run at least once but will not infinite loop?

Infinite loops are not always easy to spot. Consider the next two examples: one infinite loops and one does not. Can you determine which is which? As your code becomes more complicated, it will become harder to detect.

**EXAMPLE:** Which while-loop causes an infinite loop? 

# Example 1
n = 1
while n > 0:
    n /= 2

# Example 2
n = 2
while n > 0:
    if n % 2 == 0:
        n += 1
    else:
        n -= 1

**Answer:** The first example will not infinite loop because eventually n will be so small that Python cannot tell the difference between n and 0. More on this in *Chapter 9*. The second example will infinite loop because n will oscillate between 2 and 3 indefinitely.

Now we know two types of loops: *for-loops* and *while-loops*. In some cases, either can be used equally well, but sometimes one is better suited for the task than the other. In general, we should use *for-loops* when the number of iterations to be performed is well-defined. Conversely, we should use *while-loops* statements when the number of iterations to be performed is indefinite or not well known.