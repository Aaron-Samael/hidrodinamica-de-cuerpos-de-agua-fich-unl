#!/usr/bin/env python
# coding: utf-8

# # Advección lineal 2D
# ***

# In[2]:


import numpy as np
from plotly.subplots import make_subplots
import plotly.graph_objects as go


# Ahora resolveremos un problema de convección 2D, representado por el par de ecuaciones diferenciales parciales acopladas que se presentan a continuación:
# 
# $$\frac{\partial u}{\partial t} + u \frac{\partial u}{\partial x} + v \frac{\partial u}{\partial y} = 0$$
# 
# $$\frac{\partial v}{\partial t} + u \frac{\partial v}{\partial x} + v \frac{\partial v}{\partial y} = 0$$
# 
# Discretizando las ecuaciones con los métodos que hemos aplicado anteriormente se obtiene:

# $$\frac{u_{i,j}^{n+1}-u_{i,j}^n}{\Delta t} + u_{i,j}^n \frac{u_{i,j}^n-u_{i-1,j}^n}{\Delta x} + v_{i,j}^n \frac{u_{i,j}^n-u_{i,j-1}^n}{\Delta y} = 0$$
# 
# $$\frac{v_{i,j}^{n+1}-v_{i,j}^n}{\Delta t} + u_{i,j}^n \frac{v_{i,j}^n-v_{i-1,j}^n}{\Delta x} + v_{i,j}^n \frac{v_{i,j}^n-v_{i,j-1}^n}{\Delta y} = 0$$

# Reorganización de las dos ecuaciones, se resuelve por $u_{i,j}^{n+1}$ and $v_{i,j}^{n+1}$, respectivamente. Ten en cuenta que estas ecuaciones también están acopladas.

# $$u_{i,j}^{n+1} = u_{i,j}^n - \frac{\Delta t}{\Delta x} u_{i,j}^n (u_{i,j}^n - u_{i-1,j}^n) - \frac{\Delta t}{\Delta y} v_{i,j}^n (u_{i,j}^n - u_{i,j-1}^n) $$
# 
# $$v_{i,j}^{n+1} =  v_{i,j}^n - \frac{\Delta t}{\Delta x} u_{i,j}^n (v_{i,j}^n - v_{i-1,j}^n) - \frac{\Delta t}{\Delta y} v_{i,j}^n (v_{i,j}^n - v_{i,j-1}^n)$$

# In[ ]:


u[1:,1:]=un[1:,1:]-(dt/dx*un[1:,1:]*(un[1:,1:]-un[0:-1,1:]))-dt/dy*vn[1:,1:]*(un[1:,1:]-un[1:,0:-1]) 

v[1:,1:]=vn[1:,1:]-(un[1:,1:]*dt/dx*(vn[1:,1:]-vn[0:-1,1:]))-vn[1:,1:]*dt/dy*(vn[1:,1:]-vn[1:,0:-1])


# ### Condiciones iniciales

# Las condiciones iniciales son aplicadas tanto en la dirección x como en la dirección y. 
# 
# $$u,\ v\ = \begin{cases}\begin{matrix}
# 2 & \text{para } x,y \in (0.5, 1)\times(0.5,1) \cr
# 1 & \text{en el resto de puntos}
# \end{matrix}\end{cases}$$

# ### Condiciones de contorno

# Las condiciones de contorno son u y v igual a 1 a lo largo de los límites de la malla (cuadrícula).
# 
# $$u = 1,\ v = 1 \text{ para } \begin{cases} \begin{matrix}x=0,2\cr y=0,2 \end{matrix}\end{cases}$$

# In[3]:


###variable declarations
nx = 101#grilla en x
ny = 101 #grilla en y
nt = 15 #numero de pasos 
dx = 2.0/(nx-1)
dy = 2.0/(ny-1)
sigma = 0.25 #calculamos el dt probar 0.4
dt = sigma*dx

x = np.linspace(0,2,nx)#vector column
y = np.linspace(0,2,ny)#vector column

#Construimos malla de unos
u = np.ones((ny,nx)) 
v = np.ones((ny,nx))
un = np.ones((ny,nx))
vn = np.ones((ny,nx))

###Assign initial conditions
u[int(.5/dy):int(1/dy+1), int(.5/dx):int(1/dx+1)] = 2
v[int(.5/dy):int(1/dy+1), int(.5/dx):int(1/dx+1)] = 2 ##set hat function I.C. : u(.5<=x<=1 && .5<=y<=1 ) is 2


# In[4]:


# Initialize figure with 4 3D subplots
fig = make_subplots(
    rows=1, cols=2,
    specs=[[{'type': 'surface'}, {'type': 'contour'}]],subplot_titles=['Advección 2D. Condición inicial', 'Advección 2D plano (x,y)'],)

# adding surfaces to subplots.
fig.add_trace(
    go.Surface(x=x, y=y, z=u, colorbar_title_text='Velocidad (u)',showscale=True),
    row=1, col=1)

fig.add_trace(
    go.Contour(
        z=u, x=x, y=y,showscale=False,
    ),row=1, col=2)

#import plotly.io as pio
#fig.write_html("plotlygraph/adveccioncondicioninicial.html", include_plotlyjs="cdn", full_html=False, auto_open=True)


# Si no se ve, descargar el archivo de [aqui](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/web/book/notebooks/plotlygraph/adveccioncondicioninicial.html)

# In[5]:


#Creamos una función advección
def advection(nt):
    #Bucle de advección lineal 2D en el tiempo
    for n in range(nt+1): ##loop across number of time steps
        #Reemplazamos la variable bandera
        un = u
        vn = v
    
        #Aplicamoes la ecuación del metodo de diferencias finitas
        u[1:,1:]=un[1:,1:]-(un[1:,1:]*dt/dx*(un[1:,1:]-un[0:-1,1:]))-vn[1:,1:]*dt/dy*(un[1:,1:]-un[1:,0:-1]) 
        v[1:,1:]=vn[1:,1:]-(un[1:,1:]*dt/dx*(vn[1:,1:]-vn[0:-1,1:]))-vn[1:,1:]*dt/dy*(vn[1:,1:]-vn[1:,0:-1])
        
        #print(nt,u,v)
        
        #Imponemos las condiciones de borde para u
        u[0,:] = 1
        u[-1,:] = 1
        u[:,0] = 1
        u[:,-1] = 1
        #Imponemos las condiciones de borde para v
        v[0,:] = 1
        v[-1,:] = 1
        v[:,0] = 1
        v[:,-1] = 1


# In[8]:


#Cambiamos el paso de tiempo
nt=100 #probar 50, 100, 200
advection(nt)
#del u, v


# In[9]:


# Initialize figure with 4 3D subplots
fig = make_subplots(
    rows=1, cols=2,
    specs=[[{'type': 'surface'}, {'type': 'contour'}]],subplot_titles=['Advección 2D tiempo final', 'Advección 2D tiempo final plano (x,y)'],)

# adding surfaces to subplots.
fig.add_trace(
    go.Surface(x=x, y=y, z=u, colorbar_title_text='Velocidad (u)',showscale=True),
    row=1, col=1)

fig.add_trace(
    go.Contour(
        z=u, x=x, y=y,showscale=False,
    ),row=1, col=2)

#import plotly.io as pio
#fig.write_html("plotlygraph/adveccion.html", include_plotlyjs="cdn", full_html=False, auto_open=True)


# In[81]:


#Animation
for nt in range(0,20):
    advection(nt)
    # adding surfaces to subplots.
    fig.add_trace(
    go.Surface(x=x, y=y, z=u, colorbar_title_text='Velocidad (u)',showscale=True),
    row=1, col=1)


# Si no se ve, descargar el archivo de [aqui](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/web/book/notebooks/plotlygraph/adveccion.html)

# * Probar cambiando el número de elementos de la grilla
# * Probar cambiando sigma
# * Modificando las condiciones de borde

# In[ ]:




