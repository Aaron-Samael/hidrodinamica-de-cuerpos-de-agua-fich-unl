#!/usr/bin/env python
# coding: utf-8

# # Trabajo Práctico 2
# ## Conservación de Masa, Movimiento y Energía

# ## Ejercicios
# 
# $\bf{Ejercicio 1}$. Describa la Ecuación de Conservación. Alcance la ecuación de Cantidad de movimiento basándose en la hipótesis de Conservación de Cantidad de movimiento.
# 
# $\bf{Ejercicio 2}$. Obtenga la expresión de Navier-Stokes aplicando el modelo de cierre.
# 
# $\bf{Ejercicio 3}$. Vea y analice las siguientes películas educativas sobre la Mecánica de Fluidos:
# 
# (a) Kline movie, National Committee for Fluid Mechanics Films; Film notes for [FLOW VISUALIZATION](http://www.youtube.com/watch?v=nuQyKGuXJOs&list=PL0EC6527BE871ABA3&index=5&feature=plpp_video) by S.J. Kline, Stanford University.
# 
# $\bf{Ejercicio 4}$. Utilizando tensores cartesianos, demuestre las siguientes identidades vectoriales:
# 
# * $\nabla \times(\nabla \times \bf{F})=\nabla(\nabla \cdot \bf{F})-\nabla^2 \bf{F}$
# 
# * $ \nabla \cdot (\bf{F} \times \bf{G})=\bf{G} \cdot (\nabla \times \bf{F})-\bf{F} \cdot (\nabla \times \bf{G})$
# 
# * $\nabla \times \nabla(\phi)=0$
# 
# * $\nabla \cdot (\nabla \times \bf{F})=0$
# 
# * $\nabla \times(\bf{F} \times \bf{G})=(\bf{G} \cdot \nabla)\bf{F}-(\bf{F} \cdot \nabla)\bf{G}+(\nabla \cdot \bf{G})\bf{F}-(\nabla \cdot \bf{F})\bf{G}$
# 
# $\bf{Ejercicio 5}$. Considere el siguiente campo de velocidad
# 
# $u=1+R \frac{\sinh y}{\cosh y+\alpha \cos (x-t)}$
# 
# $v=R \frac{\alpha \sin (x-t)}{\cosh y+\alpha \cos (x-t)}$
# 
# $\bf{5.1}$ Demuestre que es solenoidal (es decir, satisface continuidad).
# 
# $\bf{5.2}$ Calcule la vorticidad del movimiento (en este caso, la función escalar $w$)
# 
# $\bf{5.3}$ Opcional: compute y grafique las líneas de contorno de $w$ para $R = 0.5$ y $\alpha=0.2$.

# In[ ]:




