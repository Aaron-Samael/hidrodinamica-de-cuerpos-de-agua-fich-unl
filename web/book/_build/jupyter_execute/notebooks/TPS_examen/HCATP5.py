#!/usr/bin/env python
# coding: utf-8

# # Trabajo Práctico 5
# ## Resistencia Hidráulica

# ## Ejercicios
# $\bf{Ejercicio 1}$. Demuestre que la expresión de la velocidad media, acorde a la recomendación de Keulegan, se escribe de la siguiente manera:

# $$ \frac{U_m}{U_*}=\frac{1}{\kappa}\ln\left(\frac{11H}{K_s}\right)$$

# 1.1 Grafique la expresión de $C_f$ (y de $C_z$), acorde a Keulegan y a Manning-Strickler en un plot semilogarítmico para varios ciclos de la rugosidad relativa $\frac{H}{D}$( esto es ,tomando $K_s=n_kD$).
# 
# 1.2 Con los datos experimentales:
# 
#     (i) Túnel de viento (Physics Fluids 12, 2000). ``wind_tunnel_A.dat``
#     (ii) Considere que el fluido es agua dado los parametros que se adjuntan en la tabla.``lab_flume_data.dat``
# Calcule y grafique para ambos casos :
#    * a) La ley de la pared (distribución lineal-log), ajustando la velocidad de corte $U_*$
#    * b) La ley de defecto de la velocidad, y el ajuste parabólico correspondiente,
#    * c) La distribución vertical de la viscosidad de remolinos,
#     
# | Parámetros | Aire | Agua | 
# | :---: | :---: | :---: | 
# | $\nu[m^2/s]$ |  0.14576 x 10-4 | 0.1 x 10-5|
# | $\kappa$| 0.4 | 0.41|
# | $A$ | 5.24 | 5.29| 
# 

# $\bf{Ejercicio 2}$. Resuelva usando el modelo TELEMAC-MASCARET 2D en planta el mismo ejercicio que se plantea en el Problema 3 del TP4. Utilice el modelo de cierre de turbulencia $k-\epsilon$ y considere utilizar la ley de rugosidad Manning. Escoja el tamaño de la malla y paso de tiempo considerando la condición de CFL<1. El tiempo final de modelación es 5min.
# 
# * Escenario 1: Condición de entrada de $Q=1m^3/s$ y salida 0.25m. Manning n=0.015
# 
# * Escenario 2: Misma condición previa con un valor de Manning n=0.5
# 
# * Escenario 3: Extienda el dominio hacia aguas abajo 15m más y deje el espigón en la misma ubicación (4.5m de la entrada).
# 
# a) Presente los resultados de los tres escenarios a través de vectores de velocidad media en planta (correspondiente al ultimo tiempo simulado). 
# b) Describa el comportamiento hidrodinámico general, observa algun patrón estudiado en flujos simples?

# In[ ]:




