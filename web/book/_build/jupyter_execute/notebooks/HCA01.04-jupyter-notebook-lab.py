# Introducción a Jupyter Notebook y Lab

## Jupyter notebook
Python puede usarse en entornos tipo shell como IPython. Estos entornoes permiten correr Python liena por linea. EL problema surge cuando se tiene mas lineas y la ejecuación se quiere hacer por bloques y luego compartirla con otros. Para esos objetivos IPython no es la mejor solución. Para ello existe Jupyter Notebook o Jupyter Lab. Este ultimo es el desarrollo mas reciente del [Proyecto Jupyter] (http://jupyter.org/).

> Jupyter Notebook es una aplicación web de código abierto que le permite crear y compartir documentos que contienen código, ecuaciones, visualizaciones y texto. Los usos incluyen: limpieza y transformación de datos, simulación numérica, modelado estadístico, visualización de datos, aprendizaje automático y muchiiiiiiiiiiiiisimoooooooo más.

Jupyter notebook se ejecuta en el navegador su navegador, podría ejecutarse localmente en su máquina como un servidor local o de forma remota en un servidor. La razón por la que se llama notebook es porque puede contener código en vivo, elementos de texto enriquecido como ecuaciones, enlaces, imágenes, tablas, etc. Por lo tanto, podría tener un notebook muy bueno para describir su idea y el código, todo en un solo documento. Por lo tanto, el Jupyter notebook se convierte en una forma muy popular de probar ideas, escribir blogs, artículos e incluso libros.

## Comencemos con Jupyter notebook

Abrimos Jupyter notebook desde el anaconda Navigator. Luego veremos el jupyter notebook en el navegadors, la dirección predeterminada es: http: // localhost: 8888, es decir, en el host local con el puerto 8888 como se muestra en la siguiente figura. Básicamente, se trata de crear un servidor local para que se ejecute en su navegador. Cuando navegue hasta el navegador, verá un panel de control. En este panel, puede ver algunas características importantes que están etiquetadas en rojo: puede ver todos los archivos en la carpeta actual, mostrar todos los notebooks en ejecución y crear un nuevo notebook u otros como archivo de texto, carpeta y una terminal. Podemos crear un nuevo notebook de Python seleccionando Python 3, generalmente esto se llama kernel de Python. También puede usar Jupyter para ejecutar algunos otros kernels (Bash y Julia).


![Jupyter_dashboard](images/jupyternote.png "The Jupyter notebook dashboard after launching the server")

## Sobre el notebook

Después de crear un nuevo notebook de Python. La barra de herramientas y el menú emiten un mensaje sore cuando se coloca el cursor sobre las mismas , le mostrará la función de la herramienta y presione el menú, le mostrará la lista desplegable. Lo más importante que debe saber de Jupyter notebook son las celdas y los diferentes tipos. En el notebook, una celda es un lugar donde puede escribir su código o texto, y ejecutar esta celda solo para ejecutar código dentro de este bloque de celdas. Dos tipos de celda importantes son el código y la Markdown, la celda de código es donde escribe su código y puede ejecutar el código en ella. La celda de Markdown es un lugar donde puede escribir la descripción en formato de texto enriquecido. Puede buscar 'Cheatsheet de Markdown' para comenzar rápidamente con Markdown. Para ejecutar cada celda simplemente presione Shift + Enter.

![Jupyter_dashboard](images/jupyternote1.png "The Jupyter notebook dashboard after launching the server")

En el notebook, puede mover la celda hacia arriba o hacia abajo, insertar o eliminar celda, etc. Hay muchas otras características interesantes sobre el Jupyter notebook.

## Apagamos el Jupyter notebook

Aclaramos que cerrar el Jupyter notebook no cerrará el cuaderno de Jupyter, ya que el servidor aún se estara ejecutando. Puede volver a abrir la dirección anterior en un navegador. Para apagarlo por completo, debemos cerrar la terminal asociada con la que inicias Jupyter noteobok. Como seria esto?

Si necesita cerrar completamente un notebook, vaya al panel superior y presiones 'Quit', esta es la forma correcta de cerrar un notebook.
 

## Jupyter Lab
No haremos un gran desarrollo ya que Jupter Lab se ejecuta de la misma manera que Jupyter-notebook. Se destaca asi que se pueden agregar plugins como se indico previamente. Para ver mas detalles ver [Jupyter-lab](https://jupyterlab.readthedocs.io/en/stable/)

