#!/usr/bin/env python
# coding: utf-8

# # Entornos (Ambientes de trabajo)

# Dado que Python y los paquetes que utilizamos en nuestro sistems operativo reciben actualizaciones y adicionalmente combinan el usos de unos con otros es recomendable realizar un ambiente de trabajo. Este espacio de trabajo tendrá las librerias y versiones necesarias para el buen funcionamiento de nuestras aplicaciones. A continuación se explica brevemente como crear un ambiente e instalar las librerias necesarias para el cursado. 

# Ya adentro del ambiente podemos instalar las librerias que se recomienda para el curso. Se debe descargar el archivo que se encuentra en el repositorio [requirements](https://gitlab.com/ldominguezruben/nuevas_herramientas_morfometria/-/blob/main/requirements.txt). Dentro del ambiente creado anteriormente tipear lo siguiente:

# ```conda create --name HCA --file requirements.txt```

# De esta manera hemos creado un ambiente llamado 'HCA' al cual le hemos instalado una version de todas las librerías que tenemos en requirements. El paso siguiente será la activación del ambiente

# ```conda activate HCA```

# Para desactivar el entorno (ambiente) debe tipear la siguiente linea:

# ```conda deactivate```
