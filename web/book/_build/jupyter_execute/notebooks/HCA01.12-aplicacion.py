#!/usr/bin/env python
# coding: utf-8

# # Ejercicio 
# 
# #### Manning-Strickler

# La fórmula de Gauckler-Manning-Strickler (o fórmula de Strickler) relaciona la profundidad del agua y la velocidad del flujo del canal abierto basándose en la suposición de características de flujo unidimensionales (promediadas en sección transversal). La fórmula de Strickler resulta de una gran simplificación de [Navier-Stokes](https://en.wikipedia.org/wiki/Navier%E2%80%93Stokes_equations) y las ecuaciones de continuidad. Aunque los enfoques unidimensionales (1d) han sido reemplazados en gran medida por modelos numéricos al menos bidimensionales (2d) en la actualidad, la fórmula de 1d Strickler todavía se usa con frecuencia como una primera aproximación para las condiciones de contorno.

# La forma básica de la fórmula de Strickler es:

# $$u=k_{st}⋅S^{1/2}⋅R_h^{2/3}$$

# u es la velocidad de flujo promediada en sección transversal en (m / s)
# 
# $kst$ es el coeficiente de Strickler en ficticio ($m^{1 / 3} / s$) correspondiente a la inversa de la $n_m$ de Manning.
# 
# $kst$ ≈ 20 (nm≈ 0.05) para ríos accidentados, complejos y casi naturales
# 
# $kst ≈ 90$ (nm≈ 0.011) para canales lisos revestidos de hormigón
# 
# $kst ≈ 26 / D_{90}^{1 / 6}$ (aproximación basada en el tamaño de grano D90, donde el 90% de los granos de sedimentos superficiales son más pequeños, según Meyer-Peter y Müller 1948
# 
# S es la pendiente de energía hipotética (m / m), que se puede suponer que corresponde a la pendiente del canal para condiciones de flujo constante y uniforme.
# 
# Rh es el radio hidráulico en (m)
# 
# El radio hidráulico Rh es la relación entre el área mojada A y el perímetro mojado P. Tanto A como P pueden calcularse en función de la profundidad del agua hy el ancho de la base del canal b. Muchas secciones transversales de canales se pueden aproximar con una forma trapezoidal, donde el ancho de la superficie del agua B = b + 2⋅h⋅m (siendo m la pendiente del banco como se indica en la figura siguiente).

# El radio hidráulico Rh es la relación entre el área mojada A y el perímetro mojado P. Tanto A como P pueden calcularse en función de la profundidad del agua hy el ancho de la base del canal b. Muchas secciones transversales de canales se pueden aproximar con una forma trapezoidal, donde el ancho de la superficie del agua B = b + 2⋅h⋅m (siendo m la pendiente del banco como se indica en la figura siguiente).

# ![IPython](images/fig2.png)

# Por tanto, A y P resultan de las siguientes fórmulas:

# $$A=h⋅0.5⋅(b+B)=h⋅(b+h⋅m)$$
# $$P=b+2h⋅(m^2+1)^{1/2}$$

# Finalmente, la descarga Q (m³ / s) se puede calcular como:

# $$Q=u⋅A=kst⋅S^{1/2}⋅R_h^{2/3}⋅A$$

# ## Hagamos una función que calcule el caudal

# Escriba un code que imprima el caudal en función del ancho de la base del canal b, la pendiente del banco m, la profundidad del agua h, la pendiente S y el coeficiente de Strickler kst.

# In[1]:


#Importamos libreria
import math

#$$Q=u⋅A=kst⋅S^{1/2}⋅R_h^{2/3}⋅A$$

#Data inicial

#Area

#Perimetro

#Rh

#Caudal


# In[2]:


import math

##initial data trapezoidal
b = 800          # bottom channel width (m)
m_l = 0        # left bank slope
m_r = 0        # right bank slope
n_m = 0.020      # Manning's n concrete
S_0 = 0.00002     # slope
h = 12.7            # W. E.

#calculate
#area = ((((h * m_l) + (h * m_r) + b) + b) / 2) * h
#perimeter = b + (h * (m_l * m_l + 1) ** 0.5) + (h * (m_r * m_r + 1) ** 0.5)
#ratio = area / perimeter

#discharge = 1/n_m *S_0**(0.5)*ratio**(2./3.)*area
#discharge


# In[ ]:




