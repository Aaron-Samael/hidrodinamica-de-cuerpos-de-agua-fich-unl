#!/usr/bin/env python
# coding: utf-8

# # Iniciando en Python
# 
# ### Python es un lenguaje de programación interpretado cuya filosofía hace hincapié en la legibilidad de su código. Se trata de un lenguaje de programación multiparadigma, ya que soporta parcialmente la orientación a objetos, programación imperativa y, en menor medida, programación funcional. Es un lenguaje interpretado, dinámico y multiplataforma.
# 

# ### Configuración del entorno Python
# Antes de comenzar a usar Python, es necesario configurar el entorno de trabajo en nuestras computadoras. Esta sección se describira paso a paso, la configuración de los ambientes necesarios para los códigos que se desarrollaran en el curso.
# 
# Hay diferentes metodos que permiten instalar (administrar) paquetes en nuestra computadora. Uno de los mas usados es el recomendado aqui conocido como [Anaconda](https://www.anaconda.com/download/) or [Miniconda](https://conda.io/miniconda.html). Ambos permiten un manejo sencillo en los paquetes Python de cualqueir ambito. Dependiendo del sistema operativo que tengamos en nuestra computadora, se deberá descargar e instalar el gestor de paquetes indicado anteriormente. La principal diferencia se encuentra en los entornos graficas de ambos. 
# A los fines que se proponen en el curso se recomienda la descarga de la versión completa de Anaconda. 
# 
# * **Anaconda** is a complete distribution framework that includes the Python interpreter, package manager as well as the commonly used packages in scientific computing. 
# 

# La instalación de Anaconda se detalla a continuación: 
# 
# **Paso 1: Descargar el instalador de Anaconda desde [website](https://www.anaconda.com/products/individual)** 
# 
# ![Anaconda_download](images/anaconda.png "The Anaconda download page, choose the installer based on your Operating System.")

# **Paso 2: El instalador se descarga en la carpeta asiganda para tales fines y debe ejecutarse dicho archivo.
# ![Anaconda_install](images/anacondainstall.PNG "The Anaconda download page")
# 

# **Paso 3: Una vez instalado ejecutamos el Anaconda Navigator.
# 
# 
# Por defecto Anaconda instala una serie de paquetes Python entre los que se destaca `ipython`, `numpy`, `scipy`, `pandas`, `matplotlib` and `jupyter notebook`. Todos estos y mas usaremos.
# 
# ! Aclaración: hablamos de ambientes como, espacios en donde uno instala paquetes (muchas funciones) que permiten ser ejecutadas y se encuentran orientadas a ciertos objetivos, fucniones matematicas (Numpy), manejo de datos tipo dateframe (Pandas) o ploteo de resultados (matplotlib).

# ### Cuatro maneras de ejecutar Python
# 
# Hay diferentes maneras de ejecutar un codigo en Python, en general depende de los objetivos que se persiguen (científico o desarrolladores).  
# 
# **1) Usar Python shell o IPython shell** 
# 
# De todas las manera que vamos a mostrar esta es la mas sencilla. Esta manera es corriendo Python shell o IPython. Esta ultima forma es mas amigable que la primera ya que permite autocompletar, emite mensajes de errores, entr eotras ventajas. Pero dejemos de hablar y usemos esta ultimo entorno. Para ejecutarlo vayan a sus escritorio donde se encuentre instalado Anaconda y ejecutemos 'Anaconda Prompt'. Para ejecutar IPython debemos escribir 'IPython' y presionar enter:
# 
# ![IPython](images/ipython.png )

# Lo primero que probaremos es ejecutar un comando que permite imprimir. Pero evitaremos usar el super trillado 'Hello World', en vez de eso imprimiremos lo siguiente `print("Una IPA, por favor")` (las comillas pueden ser simples o dobles):
# 
# ![Message](images/ipythonmess.png)

# **2) A través de un scrip/archivo desde la linea de comando**
# 
# La segunda opción es a través de la ejecución de un codigo Python, el cual puede construirse desde un programa como [Gedit](https://sourceforge.net/projects/gedit/), [Spyder](https://www.spyder-ide.org/) (viene con Anaconda) o [Visual Studio Code](https://code.visualstudio.com) (tambien viene con Anaconda). Guardar el archivo con el nombre que quieran (por ejemplo *print(UNA IPA)*):
# 
# ![Python_file](images/print.png)
# 
# ahora para ejecutarlo debemos acceder a la carpeta donde se guardó el archivo previo (ver comandos de manejo en terminal, depende de [Windows](https://www.lifewire.com/list-of-command-prompt-commands-4092302) o [Linux](https://www.ubuntubeginner.com/basic-ubuntu-commands-for-beginners/).
# 
# ![Python_file](images/pythonmess.png)

# **3) Usando Jupyter Notebook**
# 
# La tercer opción para correr Python es **Jupyter notebook**. Es un entorno Python basado en navegador muy poderoso, hablaremos más sobre él en detalles más adelante en este capítulo. Aquí vemos rápidamente cómo podríamos ejecutar el código desde un cuaderno Jupyter. Ejecutemos el `jupyter notebook` en la línea de comando de bash (en el terminal de Anaconda Prompt):
# 
# ```bash
# jupyter notebook
# ```
# 
# Luego veremos que se abre en el navegador, desde el botón superior derecho para crear un nuevo cuaderno de Python3:
# 
# ![Launching_jupyter](images/jupyternote.png "To Launch a Jupyter notebook server, type jupyter notebook in the command line, which will open a browser page as shown here. Click new button on the top write, and choose Python3 will create a Python notebook to run Python code.")
# 
# *!Aclaración Jupyter se ejecutará en la carpeta que tipeamos el comando.*
# 

# !Aclaración 1. Se puede ejecutar el Jupyter notebook ingresando en el Anaconda Navigator. Es más fácil que el método previo.
# 
# Por último, podemos imprimir el eppico mensaje previo. Para ejecutar cada celda del jupyter-notebook hay que teclear shift+enter
# 
# ![Launching_jupyter](images/jupyternote1.png "To Launch a Jupyter notebook server, type jupyter notebook in the command line, which will open a browser page as shown here. Click new button on the top write, and choose Python3 will create a Python notebook to run Python code.")

# **4) Usando Jupyter Lab**
# 
# El reciente desarrollo del Proyecto Jupyter ha puesto en escena un entorno que permite la ejecución de notebooks y a su vez terminales, IPython shell, visualizar variables, entre otras ventajas. Este desarrollo encuentra su parecido al Visual Studio ya que permite mediante la instalación de plugins construir un IDE. De la misma manera que se ejecuta el jupyter-notebook se puede correr el Jupyter Lab.
# 
# ```bash
# jupyter lab
# ```
# 
# ![Launching_jupyter-lab](images/jupyterlab.png)
# 
# !Aclaración Jupyter lab permite la instalación de plugins como se dijo. Se recomienda la instalación de los siguientes plugins:
# 
# *[Visualizador de Variables](https://github.com/lckr/jupyterlab-variableInspector)
# 
# *[Plotly](https://plotly.com/python/getting-started/)
# 
# Se pueden instalar desde el panel izquierdo cliqueando en el icono de la pieza de rompecabezas

# ## Para Linux (Distro Ubuntu Mint, Debian)

# Para las distribución en Lunix se debe descargar la ultima versión de Anaconda con la siguiente línea en la terminal:

# In[ ]:


wget -P /tmp https://repo.anaconda.com/archive/Anaconda3-2020.02-Linux-x86_64.sh


# El paso siguiente es correr la instalación. Para ello vamos a la carpeta de descarga por defecto que tenemos en nuestro sistema Linux

# In[ ]:


bash /tmp/Anaconda3-2020.02-Linux-x86_64.sh


# Aceptamos todos los terminos a lo largo de la instalación. Podemos actualizar a la ultima versión si no estamos seguro de haber descargado la ultima

# In[ ]:


conda update --all


# ### El Zen de Python
# 
# En la sección previa aprendimos a instalar el paquete de gestoría Anaconda y a ejecutar Python en sus diferentes formatos. Lo que continua es una serie de directrices que se proponen de manera de lograr la mayor eficiencia en los códigos. Posiblemente en esta instancia no logren capturar la idea pero estoy seguro que a medida que utilicen les será muy util.

# In[1]:


import this


# ### Referencias adicionales
# 1. Para la instalación del programa y consideraciones generales:
#     1. Instalación https://youtu.be/YYXdXT2l-Gg
#     2. Conditional statements https://youtu.be/DZwmZ8Usvnk
#     3. Bucles e iteraciones https://youtu.be/6iF8Xb7Z3wQ
#     4. Introducción a Matplotlib https://youtu.be/UO98lJQ3QGI
# 2. Curso corto, muy bueno (Crash course of Python) https://youtu.be/H1elmMBnykA
# 2. Otras referencias utiles (documentación)
#     1. Numpy basics https://numpy.org/doc/1.18/user/basics.html
#     2. Sympy basics https://docs.sympy.org/latest/tutorial/basic_operations.html
#     3. Scikit lear https://scikit-learn.org/stable/index.html 
