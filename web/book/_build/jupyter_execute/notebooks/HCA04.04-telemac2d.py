#!/usr/bin/env python
# coding: utf-8

# # Telemac-Mascaret
# ## Instalación

# ### Mallador
# Se descarga el mallador Bluekenue para generar las mallas del dominio computacional. 
# 
# [BLUEKENUE](https://nrc.canada.ca/en/research-development/products-services/software-applications/blue-kenuetm-software-tool-hydraulic-modellers)

# ### Modelo Numérico
# Descarga de TELEMAC 
# 
# [AQUI](http://opentelemac.org/index.php/download)

# Cliente SVN
# 
# Address: http://svn.opentelemac.org/svn/opentelemac/tags/v8p2r0
# 
# * Username: ot-svn-public
# * Password: telemac1
# 
# Configuración General [AQUI](http://wiki.opentelemac.org/doku.php?id=installation_notes_2_beta)

# Configuración de icono de ejecución [AQUI](http://wiki.opentelemac.org/doku.php?id=instructions_to_finish_installing_the_telemac_system_manually_after_a_failure_of_the_automatic_installation)

# Test Case
# 
# Buscar el case1 en la carpeta Telemac que se encuentra en el [repo](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/tree/master/web/book/notebooks/telemac/case1)

# Para el postprocesamiento se puede usar BLUEKENUE o [Pyteltools](https://github.com/CNR-Engineering/PyTelTools) 

# ## Modelo numérico TELEMAC-MASCARET

# Paper Teórico [AQUI](https://www.tandfonline.com/doi/full/10.1080/23863781.2021.1911609?src=)

# TELEMAC 2D resuelve las ecuaciones de Saint Venant usando el método de elementos finitos sobre una malla triangular, aplicando las siguientes 4 ecuaciones:

# Ecuación de continuidad:
# 
# $$\frac{\partial H}{\partial t}+\vec{u}\cdot\vec{\nabla}H+ H div \vec{u}=S_h$$

# Ecuación continuidad de movimiento

# Eje x $$\frac{\partial u}{\partial t}+\vec{u}\cdot\vec{\nabla}(u)=-g\frac{\partial Z_s}{\partial x}+S_x+\frac{1}{H} div(H\nu_f\vec{\nabla}u)$$

# Eje y $$\frac{\partial v}{\partial t}+\vec{u}\cdot\vec{\nabla}(v)=-g\frac{\partial Z_s}{\partial y}+S_y+\frac{1}{H} div(H\nu_f\vec{\nabla}v)$$

# donde H, u, v (velocidad promediada en la vertical en las direcciones x e y).
# Los términos de las ecuaciones previas son calculados en uno o más pasos (en el caso del término de advección por el método de las características, entre otros) de la siguiente manera: (i) Advección de H, u y v; (ii) Propagación, difusión y términos fuentes de la ecuación de conservación (Ec. 1); (iii) Difusión y términos fuentes de la ecuación de transporte de trazadores.

# ## Material de lectura

# Les dejo el siguiente material que incluye la clase de TELEMAC (presentación) [AQUI](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/web/book/notebooks/ref/Clase10.pdf), una guía para la [instalación](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/web/book/notebooks/ref/InstalacionTelemac.pdf).
# 
# Ademas le comparto una presentación por si les da ganas de indagar un poco mas en [TELEMAC](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/web/book/notebooks/ref/2_Lecture_03_Steering_Telemac2d.pdf).

# ## Material última clase (24-06)

# Les dejo la presentación de los dos casos que se encuentran en el repo:
# 
# **[PRESENTACION](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/web/book/notebooks/ref/Clase11.pdf)
#     
# **[CASO 2](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/tree/master/web/book/notebooks/telemac/case2)
#     
# **[CASO 3](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/tree/master/web/book/notebooks/telemac/case3)

# In[ ]:




