# Curso postgrado FICH-UNL: Hidrodinámica de Cuerpos de Agua

Docente Responsable: Dr. Lucas Dominguez Ruben

- Presentacion del curso: [presentacioncurso.pdf](presentacioncurso.pdf)
- Programa aprobado: [programa.pdf](programa.pdf)
- Webpage: [HCA](https://ldominguezruben.gitlab.io/hidrodinamica-de-cuerpos-de-agua-fich-unl/index.html)
- Sala de clases [SALA](https://meet.jit.si/hidrodinamicadecuerposdeagua) 

aportes, contacto, aqui: ldominguezruben@gmail.com | ldominguez@fich.unl.edu.ar

## Generalidades
- Para la aprobación se debe cumplimentar con la presentación de TPs (5) y un examen integrador final. 
- Las clases se darán una vez a la semana los dias Miércoles de 9 a 11:30  
- Inscripción: del 29 de Marzo al 8 de Julio de 2021
- Inicio cursado: 13/04/21
- Finalización primer cuatrimestre: 8 de Julio de 2021

## Bibliografía

- [Curso de Python para ciencias e ingenierías](https://github.com/mgaitan/curso-python-cientifico)
- [CFD An Open-Source Approach](https://users.encs.concordia.ca/~bvermeir/book/CFD%20-%20An%20Open-Source%20Approach.pdf)
- Hirsch, c. (2007), Numerical Computation of Internal and Extenral flows: The fundamentals of Computational Fluid Dynamics. Elsevier.
- Hervouet, J.M. (2007), Hydrodynamics of Free Surface Flows: modeling with the finite element method, John Wiley & Sons.
- Wu, W. (2008), Computational River Dynamics, Taylor and Francis.
- VanderPlas, J. ( 2017), Python Data Science Handbook, O’Reilly.
- Batchelor, G.K. (2000), An Introduction to Fluid Dynamics, Cambridge University Press.
- Stoker, J.J. (1992), Water Waves: The Mathematical Theory with Applications, Wiley Classics Library.
- Whitham, G.B. (1999), Linear and Nonlinear Waves, John Wiley & Sons.
- Vreugdenhil, C.B. (1994), Numerical Method for Shallow-Water Flow, Springer Science+Business Media, B.V., DOI: 10.1007/978-94-015-8354-1.
- [How to Think Like a Computer Scientist: Learning with Python 3](https://www.ict.ru.ac.za/Resources/cspw/thinkcspy3/thinkcspy3.pdf) (PDF)

## Dependencias
- numpy
- scipy
- matplotlib
- plotly
- pandas

