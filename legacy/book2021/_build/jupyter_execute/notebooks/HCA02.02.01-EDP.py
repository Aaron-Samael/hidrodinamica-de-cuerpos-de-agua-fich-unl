# Clasificación de Ecuaciones Diferenciales Parciales 

## Ecuaciones de Primer Orden

$$A\frac{\partial u}{\partial x}+B\frac{\partial u}{\partial y}=F(x,y,u)$$

Esta ecuación es una típica expresión de característica hiperbólica. Y que significa que se hiperbólica, que la información de la partícula viaja en una dirección definida. Recuerden la expresión de advección lineal y Ecuación de Burguers. Notese que $x$ o $y$ no necesitan ser solamente la variable de posición. 

## Ecuaciones de Segundo orden

$$A\frac{\partial^2 u}{\partial x^2}+B\frac{\partial^2 u}{\partial x\partial y}+C\frac{\partial^2u}{\partial y^2}=F(x,y,u,\frac{\partial u}{\partial x},\frac{\partial u}{\partial y})$$

Esta expresión dependiendo del valor que asuma A, B y C tendra diferentes comportamientos

$B^2-4AC>0$

Esta expresión tiene naturaleza hiperbólica. La solución muestra una forma de onda. 

$B^2-4AC=0$

Esta expresión serua de naturaleza parabólica. Esto es lo mas complejo difusion transitoria 

$B^2-4AC<0$

Esta expresión es de naturaleza elíptica. Es un proceso de difusión pero estacionario.

