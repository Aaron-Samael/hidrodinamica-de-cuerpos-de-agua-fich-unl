# TEMA 1. Conceptos Básicos de Python

---

* [**I.1 Iniciando el camino con Python**](HCA01.01-iniciando-el-camino-con-python.ipynb)
* [**I.2 Calculamos?**](HCA01.02-calculamos.ipynb)
* [**I.3 Paquetes y Entornos**](HCA01.03-paquetes-y-entornos.ipynb)
* [**I.4 Jupyter Notebook y Lab**](HCA01.04-jupyter-notebook-lab.ipynb)
* [**I.5 Expresiones Lógicas y Operadores**](HCA01.05-expresiones-logicas-y-operadores.ipynb)
* [**I.6 Estructura de Datos**](HCA01.06-estructura-de-datos.ipynb)
* [**I.7 Funciones**](HCA01.07-funciones.ipynb)
* [**I.8 Condicionantes**](HCA01.08-condicionantes.ipynb)
* [**I.9 Iteraciones. For y While**](HCA01.09-iteraciones.ipynb)
* [**I.10 Ploteo de datos 2D y 3D**](HCA01.10-plott2D-3D.ipynb)
* [**I.11 Lectura de datos**](HCA01.11-read-data.ipynb)
* [**I.12 Problemas**](HCA01.12-problemas.ipynb)

--- 

## Porque Python??
  

---

Las ventajas de usar python son inumerables ya que representa un lenguaje orientado a objeto multiproposito, con gran desarrolloen ambitos variados (cientificos, ingenieriles de sistema). La Ciencia lo ha empeza a incorporar como herramienta y plataforma al lenguaje Python. Esto puede deberse a que la curvas de aprendizaje son de pendientes bajas, en otras palabras, en poco tiempo uno puede alcanzar a programar que anteriormente debian dedicarle mucho tiempo. Ahora, a medida que nos introducimos en el lenguaje su dificultad puede ser igual de complejo que un lenguaje de alto nivel (C++ o Fortran).


