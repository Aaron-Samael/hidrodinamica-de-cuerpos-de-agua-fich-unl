# Condiciones de borde. Capa limite

Recordamos Navier Stokes

$$
\rho \frac{\partial \bar{u_i}}{\partial t}+\rho\frac{\partial}{\partial x_j}(\bar {u_j}\bar{u_i})=-\frac{\partial p}{\partial x_i}+\frac{\partial}{\partial x_j}\left(\bar{\tau_ji}-\rho \bar {u'_j}\bar{u'_i}\right)$$

Si consideramos el tiempo final de calculo y despreciamos el gradiente de presion  y consideramos que el flujo de velocidad se dominado en la dirección vertical la ecuación previa queda:

$$v\frac{\partial \bar{u}}{\partial y}-\bar{u'}\bar{v'}=\left. v\frac{\partial\bar{u}}{\partial y}\right\vert_{y=0}$$

Esto implica que la tasa de transporte de la cantidad de movimiento en sentido pared es constante a través de la capa límite, y que es igual a la tensión viscosa que la pared
se aplica al fluido. Además, existen dos mecanismos que se encargan de transportar este impulso en la capa límite. El primer término, es la aceleración advectiva actuante sobre el fluido, por su parte el segundo corresponde a los esfuerzos viscoso (turbulencia que se desarrollan en la misma. Estos son responsables de transportar el impulso a través de la capa límite por moviéndolo físicamente. Las fluctuaciones de velocidad ascendente tienden a mover el fluido de movimiento lento cerca de la pared.
en la capa límite. Por el contrario, las fluctuaciones de velocidad descendente tienden a moverse rápidamente fluido cerca de la corriente libre hacia abajo en la capa límite, transportando impulso. Basado en fisico observaciones, la capa límite se divide típicamente en tres regiones distintas.

![IPython](images/profile-velocity.png)

## Inner Layer

La capa interior (inner layer) es la región cercana al muro. La condición de límite de no deslizamiento que fuerza la velocidad hacia cero y, como se discutió anteriormente, los componentes de la velocidad fluctuante también se desvanecen aquí. En esta región el fluido es transportado por efectos viscosos.

$$v\frac{\partial\bar{u}}{\partial y}=\left. v\frac{\partial\bar{u}}{\partial y}\right\vert_{y=0}$$

## Outer Layer

En esta región de la capa límite, el impulso es transportado casi en su totalidad por la turbulencia. En esta región es posible indicar que el perfil de velocidades es del tipo log-law.

$$\bar{u'}\bar{v'}=\left. v\frac{\partial\bar{u}}{\partial y}\right\vert_{y=0}$$

## Buffer Layer

La capa de amortiguación (intermedia) es simplemente la región entre las capas interior y exterior. Ni lo viscoso ni los términos de tensión de Reynolds dominan aquí y, por lo tanto, la Ecuación inicial permanece sin cambios. Para comprender mejor el comportamiento de una capa límite, primero no dimensionaremos utilizando parámetros desde dentro de la capa límite, lo que da como resultado las unidades de pared adimensionales.
Comenzando con el esfuerzo cortante en la pared y la densidad, obtenemos la velocidad de fricción

$$u^{*}=\sqrt{\frac{\tau_w}{\rho}}$$

Altura adimensional ('y plus')

$$y^{+}=\frac{yu^{\tau}}{\nu}$$

Definimos 'u plus'

$$u^+=\frac{u}{u^*}$$

## Casos Prácticos

A continuación mostramos un mapa de colores con las velocidades instantaneas medidas en un cauce natural. El equipo de medición es un instrumento ADCP

![IPython](images/XS11.png)

Podemos ver que si promediamos los perfiles de velocidades tendremos un perfil mas suave como se muestran a continuación el cual puede ser parecido a una ley logaritmica

![IPython](images/perfiles.png)

