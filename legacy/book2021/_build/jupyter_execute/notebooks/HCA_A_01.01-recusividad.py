# Recursividad

A **recursive** function is a function that makes calls to itself. It works like the loops we described before, but sometimes it the situation is better to use recursion than loops. 

Every recursive function has two components: a __base case__ and a __recursive step__. The __base case__ is usually the smallest input and has an easily verifiable solution. This is also the mechanism that stops the function from calling itself forever. The __recursive step__ is the set of all cases where a __recursive call__, or a function call to itself, is made.

As an example, we show how recursion can be used to define and compute the factorial of an integer number. The factorial of an integer $n$ is $1 \times 2 \times 3 \times ... \times (n - 1) \times n$. The recursive definition can be written:

\begin{equation}
f(n) = \begin{cases}
    1 &\text{if $n=1$}\\
    n \times f(n-1) & \text{otherwise}\\
    \end{cases}
\end{equation}

The base case is $n = 1$ which is trivial to compute: $f(1) = 1$. In the recursive step, $n$ is multiplied by the result of a recursive call to the factorial of $n - 1$.

**TRY IT!** Write the factorial function using recursion. Use your function to compute the factorial of 3. 

def factorial(n):
    """Computes and returns the factorial of n, 
    a positive integer.
    """
    if n == 1: # Base cases!
        return 1
    else: # Recursive step
        return n * factorial(n - 1) # Recursive call     

factorial(3)

__WHAT IS HAPPENING?__ First recall that when Python executes a function, it creates a workspace for the variables that are created in that function, and whenever a function calls another function, it will wait until that function returns an answer before continuing. In programming, this workspace is called stack. Similar to a stack of plates in our kitchen, elements in a stack are added or removed from the top of the stack to the bottom, in a "last in, first out" order. For example, in the `np.sin(np.tan(x))`, `sin` must wait for `tan` to return an answer before it can be evaluated. Even though a recursive function makes calls to itself, the same rules apply.

1. A call is made to `factorial(3)`, A new workspace is opened to compute `factorial(3)`.
2. Input argument value 3 is compared to 1. Since they are not equal, else statement is executed.
3. `3*factorial(2)` must be computed. A new workspace is opened to compute `factorial(2)`.
4. Input argument value 2 is compared to 1. Since they are not equal, else statement is executed.
5. `2*factorial(1)` must be computed. A new workspace is opened to compute `factorial(1)`.
6. Input argument value 1 is compared to 1. Since they are equal, if statement is executed.
7. The return variable is assigned the value 1. `factorial(1)` terminates with output 1. 
8. `2*factorial(1)` can be resolved to $2 \times 1 = 2$. Output is assigned the value 2. `factorial(2)` terminates with output 2. 
9. `3*factorial(2)` can be resolved to $3 \times 2 = 6$. Output is assigned the value 6. `factorial(3)` terminates with output 6. 

The order of recursive calls can be depicted by a recursion tree shown in the following figure for `factorial(3)`. A recursion tree is a diagram of the function calls connected by numbered arrows to depict the order in which the calls were made. 

<img src="images/06.01.01-Recursion_tree_for_factorial.png" title="Recursion tree for factorial(3)" width="200"/>

Fibonacci numbers were originally developed to model the idealized population growth of rabbits. Since then, they have been found to be significant in any naturally occurring phenomena. The Fibonacci numbers can be generated using the following recursive formula. Note that the recursive step contains two recursive calls and that there are also two base cases (i.e., two cases that cause the recursion to stop).

\begin{equation}
F(n) = \begin{cases}
    1 &\text{if $n=1$}\\
    1 &\text{if $n=2$}\\
    F(n-1) + F(n-2) & \text{otherwise}\\
    \end{cases}
\end{equation}

**TRY IT!** Write a recursive function for computing the *n-th* Fibonacci number. Use your function to compute the first five Fibonacci numbers. Draw the associated recursion tree. 

def fibonacci(n):
    """Computes and returns the Fibonacci of n, 
    a postive integer.
    """
    if n == 1: # first base case
        return 1
    elif n == 2: # second base case
        return 1
    else: # Recursive step
        return fibonacci(n-1) + fibonacci(n-2) # Recursive call 

print(fibonacci(1))
print(fibonacci(2))
print(fibonacci(3))
print(fibonacci(4))
print(fibonacci(5))

![fibonacci(5)](images/06.01.02-Recursion_tree_for_fibonacci.png "Recursion tree for fibonacci(5)")

As an exercise, consider the following modification to `fibonacci`, where the results of each recursive call are displayed to the screen.

**EXAMPLE:** Write a function `fibonacci_display` that based on the Modification of `fibonacci`. Can you determine the order in which the Fibonacci numbers will appear on the screen for fibonacci(5)?

def fibonacci_display(n):
    """Computes and returns the Fibonacci of n, 
    a postive integer.
    """
    if n == 1: # first base case
        out = 1
        print(out)
        return out
    elif n == 2: # second base case
        out = 1
        print(out)
        return out
    else: # Recursive step
        out = fibonacci_display(n-1)+fibonacci_display(n-2)
        print(out)
        return out # Recursive call 

fibonacci_display(5)

Notice that the number of recursive calls becomes very large for even relatively small inputs for `n`. If you do not agree, try to draw the recursion tree for `fibonacci(10)`. If you try your unmodified function for inputs around 35, you will notice significant computation times.

fibonacci(35)

There is an iterative method of computing the *n-th* Fibonacci number that requires only one workspace.

**EXAMPLE:** Iterative implementation for computing Fibonacci numbers.

import numpy as np

def iter_fib(n):
    fib = np.ones(n)
    
    for i in range(2, n):
        fib[i] = fib[i - 1] + fib[i - 2]
        
    return fib

**TRY IT!** Compute the *25-th* Fibonacci number using `iter_fib` and `fibonacci`. And use the magic command `timeit` to measure the run time for each. Notice the large difference in running times. 

%timeit iter_fib(25)

%timeit fibonacci(25)

You can see in the previous example that the iterative version runs much faster than the recursive counterpart. In general, iterative functions are faster than recursive functions that perform the same task. So why use recursive functions at all? There are some solution methods that have a naturally recursive structure. In these cases it is usually very hard to write a counterpart using loops. The primary value of writing recursive functions is that they can usually be written much more compactly than iterative functions. The cost of the improved compactness is added running time.

The relationship between the input arguments and the running time is discussed in more detail later in the chapter on Complexity.

**TIP!** Try to write functions iteratively whenever it is convenient to do so. Your functions will run faster. 

**NOTE!** When we are using recursive call as showing above, we need to make sure that it can reach the base case, otherwise, it results to infinite recursion. In Python, when we execute a recursive function on a large output that can not reach the base case, we will encounter a "maximum recursion depth exceeded error". Try the following example, and see what do you get. 

factorial(5000)

We can handle the recursion limit using the `sys` module in Python and set a higher limit. Run the following code, and you will not see the error. 

import sys
sys.setrecursionlimit(10**5)

factorial(5000)

## Divide and Conquer

**Divide and conquer** is a useful strategy for solving difficult problems. Using divide and conquer, difficult problems are solved from solutions to many similar easy problems. In this way, difficult problems are broken up so they are more manageable. In this section, we cover two classical examples of divide and conquer: the Towers of Hanoi Problem and the Quicksort algorithm.

## Towers of Hanoi

The Towers of Hanoi problem consists of three vertical rods, or towers, and *N* disks of different sizes, each with a hole in the center so that the rod can slide through it. The disks are originally stacked on one of the towers in order of descending size (i.e., the largest disc is on the bottom). The goal of the problem is to move all the disks to a different rod while complying with the following three rules:


1. Only one disk can be moved at a time.
2. Only the disk at the top of a stack may be moved. 
3. A disk may not be placed on top of a smaller disk.

The following figure shows the steps of the solution to the Tower of Hanoi problem with three disks. 

![Tower](images/06.02.01-Illustration_Towers_of_Hanoi.png "Illustration of the Towers of Hanoi: In eight steps, all disks are transported from pole 1 to pole 3, one at a time, by moving only the disk at the top of the current stack, and placing only smaller disks on top of larger disks.")

A legend goes that a group of Indian monks are in a monastery working to complete a Towers of Hanoi problem with 64 disks. When they complete the problem, the world will end. Fortunately, the number of moves required is $2^{64} âˆ’ 1$ so even if they could move one disk per millisecond, it would take over 584 million years for them to finish.

The key to the Towers of Hanoi problem is breaking it down into smaller, easier-to-manage problems that we will refer to as **subproblems**. For this problem, it is relatively easy to see that moving a disk is easy (which has only three rules) but moving a tower is difficult (we cannot immediately see how to do it). So we will assign moving a stack of size *N* to several subproblems of moving a stack of size *N âˆ’ 1*.

Consider a stack of *N* disks that we wish to move from Tower 1 to Tower 3, and let *my_tower(N)* move a stack of size *N* to the desired tower (i.e., display the moves). How to write *my_tower* may not immediately be clear. However, if we think about the problem in terms of subproblems, we can see that we need to move the top *N-1* disks to the middle tower, then the bottom disk to the right tower, and then the *N-1* disks on the middle tower to the right tower. *my_tower* can display the instruction to move disk *N*, and then make recursive calls to *my_tower(N-1)* to handle moving the smaller towers. The calls to *my_tower(N-1)* make recursive calls to *my_tower(N-2)* and so on. A breakdown of the three steps is depicted in the following figure.


<img src="images/06.02.02-Break_down.png" alt="breakdown" title="Breakdown of one iteration of the recursive solution of the Towers of Hanoi problem." width="400"/>

Following is a recursive solution to the Towers of Hanoi problem. Notice its compactness and simplicity. The code exactly reflects our intuition about the recursive nature of the solution: First we move a stack of size *N-1* from the original tower 'from_tower' to the alternative tower 'alt_tower'. This is a difficult task, so instead we make a recursive call that will make subsequent recursive calls, but will, in the end, move the stack as desired. Then we move the bottom disk to the target tower 'to_tower'. Finally, we move the stack of size *N-1* to the target tower by making another recursive call.

**TRY IT!** Use the function *my_towers* to solve the Towers of Hanoi Problem for *N = 3*. Verify the solution is correct by inspection. 

def my_towers(N, from_tower, to_tower, alt_tower):
    """
    Displays the moves required to move a tower of size N from the
    'from_tower' to the 'to_tower'. 
    
    'from_tower', 'to_tower' and 'alt_tower' are uniquely either 
    1, 2, or 3 referring to tower 1, tower 2, and tower 3. 
    """
    
    if N != 0:
        # recursive call that moves N-1 stack from starting tower
        # to alternate tower
        my_towers(N-1, from_tower, alt_tower, to_tower)
        
        # display to screen movement of bottom disk from starting
        # tower to final tower
        print("Move disk %d from tower %d to tower %d."\
                  %(N, from_tower, to_tower))
        
        # recursive call that moves N-1 stack from alternate tower
        # to final tower
        my_towers(N-1, alt_tower, to_tower, from_tower)

my_towers(3, 1, 3, 2)

By using Divide and Conquer, we have solved the Towers of Hanoi problem by making recursive calls to slightly smaller Towers of Hanoi problems that, in turn, make recursive calls to yet smaller Towers of Hanoi problems. Together, the solutions form the solution to the whole problem. The actual work done by a single function call is actually quite small: two recursive calls and moving one disk. In other words, a function call does very little work (moving a disk), and then passes the rest of the work onto other calls, a skill you will probably find very useful throughout your engineering career.

## Quicksort

An list of numbers, *A*, is **sorted** if the elements are arranged in ascending or descending order. Although there are many ways of sorting a list, *quicksort* is a divide-and-conquer approach that is a very fast algorithm for sorting using a single processor (there are faster algorithms for multiple processors).

The *quicksort* algorithm starts with the observation that sorting a list is hard, but comparison is easy. So instead of sorting a list, we separate the list by comparing to a **pivot**. At each recursive call to *quicksort*, the input list is divided into three parts: elements that are smaller than the pivot, elements that are equal to the pivot, and elements that are larger than the pivot. Then a recursive call to *quicksort* is made on the two subproblems: the list of elements smaller than the pivot and the list of elements larger than the pivot. Eventually the subproblems are small enough (i.e., list size of length 1 or 0) that sorting the list is trivial.

Consider the following recursive implementation of *quicksort*.

def my_quicksort(lst):
    
    if len(lst) <= 1:
        # list of length 1 is easiest to sort 
        # because it is already sorted
        
        sorted_list = lst    
    else:
        
        # select pivot as teh first element of the list
        pivot = lst[0]
        
        # initialize lists for bigger and smaller elements 
        # as well those equal to the pivot
        bigger = []
        smaller = []
        same = []
        
        # loop through list and put elements into appropriate array
        
        for item in lst:
            if item > pivot:
                bigger.append(item)
            elif item < pivot:
                smaller.append(item)
            else:
                same.append(item)
        
        sorted_list = my_quicksort(smaller) + same + my_quicksort(bigger)
        
    return sorted_list

my_quicksort([2, 1, 3, 5, 6, 3, 8, 10])

Similarly to Towers of Hanoi, we have broken up the problem of sorting (hard) into many comparisons (easy). 