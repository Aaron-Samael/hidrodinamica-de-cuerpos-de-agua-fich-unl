## Esquema de MUSCL

# Required modules
import numpy as np
import matplotlib.pyplot as plt

Implementaremos métodos de volumen finito basados en el resolutor de  MUSCL para resolver

\begin{align}
	\frac{\partial u}{\partial t} + \frac{\partial u}{\partial x} = 0,
\end{align}
en una cuadrícula $ x \in [0,2] $ con condiciones de contorno periódicas.


Siguiendo el ejemplo del libro de texto, implementamos ``residual`` para calcular el residual utilizando el second-order upwind-biased method. Para el esquema de advección, usamos un solucionador de Riemann de la forma $ F (u_L, u_R) = F (u_L) $.

**Notece**: Solo se implementa ``dl`` debido al solucionador de Upwind de Riemann. Más tarde, deberá implementar la pendiente correcta ``dr``.

Complete las siguientes funciones. Para cualquier valor de ``u``, ``u0``, ``u1`` and ``u2`` se ordenan de izquierda a derecha en ``get_slope``. Por ejemplo, $ u_ {i-2}, u_ {i-1}, u_i $.

def get_slope(u0, u1, u2, b):
    # Linear reconstruction
    delta = # Complete definition for delta_i
    return delta

def residual(u, a, dx):
    # Initialize residual vector
    res = 0.0*u
    b = 0.0
    for i in range(n):
        # Interface i+1/2
        if i == n - 1:
            # Enforce periodic BC at x=L
            dl = get_slope(u[i - 1], u[i], u[0], b)
        else:
            dl = get_slope(u[i - 1], u[i], u[i + 1], b)
            
        # Compute u_i+1/2,L
        ul = 
        
        # Compute f_i+1/2=a*uL using the upwind Riemann solver
        fR = a*ul
        
        # Interface i-i/2
        dl = get_slope(u[i - 2], u[i - 1], u[i], b) 
        
        # Compute u_i-1/2,L
        ul = 
        
        # Computr f_i-1/2=a*uL using the upwind Rieman solver
        fL = 
        
        res[i] = -(fupw_R - fupw_L)/dx
    return res

Para hacer avanzar la solución en el tiempo, proporcionamos el método de punto medio de segundo orden del cuaderno Métodos de pasos en el tiempo.

def advance_solution(ut, a, dx, dt, tf):
    t = 0
    while t < tf:
        r = residual(ut, a, dx)
        um = ut + 0.5*dt*r
        rm = residual(um, a, dx)
        uf = ut + 1.0*dt*rm
        ut = 1.0*uf
        t += dt 
    return ut

Completmos la siguiente celda para establecer los parámetros iniciales del problema. Considerar
- ``a = 1.0``
- ``n = 300``
- ``L = 2.0``
- ``dt = 2e-3``
- ``tf = 2.0``
El perfil de solución inicial viene dado por
\begin{align}
	u(x, 0) &= 
	\begin{cases}
		e^{-20(x-0.5)^2} & \text{if } x < 1.2, \\ 
		1 & \text{if } 1.2 < x < 1.5,	 \\
		0 & \text{otro caso}.
	\end{cases}
\end{align}

a = 1
n = 300
L = 2
dt = 2e-3
tf = 2

# Calculate mesh spacing and grid points
dx = L/n 
x = np.arange(0, L, dx)

# Initialize the solution
u0 = np.zeros(n)
for i, xval in enumerate(x):
    if xval < 1.2:
        u0[i] = 
    elif 1.2 < xval < 1.5:
        u0[i] = 
    else:
        u0[i] = 

Ejecutemos la siguiente celda para avanzar la solución utilizando la función de paso de tiempo proporcionada anteriormente

u_adv = advance_solution(u0, a, dx, dt, tf)

Calculemos la variación total (TV) de la solución inicial y final. ¿Es este método TVD?

Completa el cálculo de TV de la solución final.

tv_u0 = np.sum(np.abs(u0[1:] - u0[:-1]))
tv_uf = 

if tv_uf <= tv_u0:
    print('Scheme is TVD')
else:
    print('Scheme is not TVD')

Tracemos la solución del esquema de ceñida de segundo orden y compárelo con la solución inicial

plt.figure(0)
plt.plot(x, u0, 'k', lw=1, label=r'$u(x,0)$')
plt.plot(x, u_adv, 'o', color='#bd0c00', ms=1.5, label=r'$u(x,t^*)$')
plt.legend()

Para preservar la monotonicidad del esquema, necesitamos definir funciones limitadoras. Complete los limitadores ``van_leer`` y ``superbee`` usando las fórmulas que se muestran en el libro de texto.

def minmod(r):
    return max([0, min([1, r])])
    
def van_leer(r):
    return 

def superbee(r):
    return 

Complete la función ``get_slope`` a continuación para incluir los limitadores siguiendo la teoría en el libro de texto, luego ejecute la celda.
** Nota: **, deberá agregar ``ep`` a cualquier división por ``r`` para evitar el infinito.

def get_slope(u0, u1, u2, b):
    ep = 1e-20
    # Compute ratio of slopes
    r = 
    phi = limiter(r)
    # Compute limited slope
    delta = 
    return delta

Para cada una de las funciones de limitación implementadas anteriormente, genere un gráfico de la solución para comparar los resultados.

fig, ax = plt.subplots(figsize=(8, 2.5), ncols=3, sharex=True, sharey=True)

# Minmod limiter
limiter = minmod
u_adv = advance_solution(u0, a, dx, dt, tf) 
ax[0].plot(x, u0, 'k', lw=1)
ax[0].plot(x, u_adv, 'o', markersize=1.5, color='#bd0c00')
ax[0].set_ylabel('$u$')
ax[0].set_xlabel('$x$')
ax[0].set_title('minmod')

# van Leer limiter
limiter = van_leer
u_adv = advance_solution(u0, a, dx, dt, tf) 
ax[1].plot(x, u0, 'k', lw=1)
ax[1].plot(x, u_adv, 'o', markersize=1.5, color='#bd0c00')
ax[1].set_ylabel('$u$')
ax[1].set_xlabel('$x$')
ax[1].set_title('van Leer')

# Superbee limiter
limiter = superbee
u_adv = advance_solution(u0, a, dx, dt, tf) 
ax[2].plot(x, u0, 'k', lw=1)
ax[2].plot(x, u_adv, 'o', markersize=1.5, color='#bd0c00')
ax[2].set_ylabel('$u$')
ax[2].set_xlabel('$x$')
ax[2].set_title('superbee')

plt.tight_layout()

def riemann_solver(ul, ur):
    return ul
    
def residual(u, a, dx):
    res = 0.0*u
    b = 0.0
    for i in range(n):
        # Interface i+1/2
        if i == n - 1:
            # Enforce periodic BC at x=L
            dl = get_slope(u[i - 1], u[i], u[0], b)
            dr = 
        elif i == n - 2:
            dl = 
            dr = 
        else:
            dl = get_slope(u[i - 1], u[i], u[i + 1], b)
            dr = 
            
        # Compute u_i+1/2,L and u_i+1/2,R
        ul = u[i] + 0.5*dl
        ur = 
        
        # Compute f_i+1/2 using the upwind Riemann solver
        fR = riemann_solver(ul, ur)
        
        # Interface i-i/2
        dl = get_slope(u[i - 2], u[i - 1], u[i], b) 
        dr = 
        
        # Compute u_i-1/2,L and u_i-1/2,R
        ul = u[i - 1] + 0.5*dl
        ur = 
        
        # Computr f_i-1/2 using the upwind Rieman solver
        fL = riemann_solver(ul, ur)
        
        res[i] = -(fR - fL)/dx
    return res

Vuelva a escribir ``riemann_solver`` para que utilice un solucionador de Roe para la ecuación de Burgers. Luego, ejecute la siguiente celda para ver su implementación

n = 100
L = 1.0
dt = 0.001
tf = 0.5

# Calculate mesh spacing and grid points
dx = L/n 
x = np.arange(0, L, dx)

# Define limiter to use
limiter = minmod

u0 = np.exp(-40 * (x - 1/2)**2)
u_burgers = advance_solution(u0, 1, dx, dt, tf) 

plt.figure()
plt.plot(x, u_burgers)

