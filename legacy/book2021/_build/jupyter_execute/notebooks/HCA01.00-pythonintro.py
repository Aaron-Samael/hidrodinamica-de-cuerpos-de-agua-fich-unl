# TEMA I. Introducción a Python

---

* [**I.1 Iniciando el camino con Python**](HCA01.01-iniciando-el-camino-con-python.ipynb)
* [**I.2 Paquetes y Entornos**](HCA01.02-paquetes-y-entornos.ipynb)
* [**I.3 Jupyter Notebook y Lab**](HCA01.03-jupyter-notebook-lab.ipynb)
* [**I.4 Expresiones Lógicas y Operadores**](HCA01.04-expresiones-logicas-y-operadores.ipynb)
* [**I.5 Estructura de Datos**](HCA01.05-estructura-de-datos.ipynb)
* [**I.6 Funciones**](HCA01.06-funciones.ipynb)
* [**I.7 Condicionantes**](HCA01.07-condicionantes.ipynb)
* [**I.8 Iteraciones. For y While**](HCA01.08-iteraciones.ipynb)
* [**I.9 Calculamos?. Librería de computo Científico**](HCA01.09-calculamos.ipynb)
* [**I.10 Ploteo de datos 2D y 3D**](HCA01.10-plott2D-3D.ipynb)
* [**I.11 Lectura de datos**](HCA01.11-read-data.ipynb)

--- 

## Motivación
  

---

Python es un lenguaje de programación interpretado cuya filosofía hace hincapié en la legibilidad de su código. Se trata de un lenguaje de programación multiparadigma, ya que soporta parcialmente la orientación a objetos, programación imperativa y, en menor medida, programación funcional. Es un lenguaje interpretado, dinámico y multiplataforma.

Es lo mas....

![IPython](images/pythonvolar.jpg )

