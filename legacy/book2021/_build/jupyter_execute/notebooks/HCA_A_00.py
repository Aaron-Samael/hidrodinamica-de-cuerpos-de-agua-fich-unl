# TEMAS ADICIONALES

* [Mas sobre python](HCA_A_01.00-mas-sobre-python.ipynb)
* [Control de versiones. Git](HCA_A_02-git.ipynb)
* [Notación Tensorial](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/ref/notaciontensorial.pdf)
* [TELEMAC-MASCARET](http://opentelemac.org/)

