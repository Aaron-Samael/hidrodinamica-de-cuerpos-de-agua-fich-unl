# Difusión Lineal 2D


import plotly.graph_objects as go
import pandas as pd
import numpy as np
from plotly.subplots import make_subplots

Y aqui está la ecuación de difusión en 2D:

$$\frac{\partial u}{\partial t} = \nu \frac{\partial ^2 u}{\partial x^2} + \nu \frac{\partial ^2 u}{\partial y^2}$$


Utilizamos el mismo esquema que analizamos en la clase previa


$$\frac{u_{i,j}^{n+1} - u_{i,j}^n}{\Delta t} = \nu \frac{u_{i+1,j}^n - 2 u_{i,j}^n + u_{i-1,j}^n}{\Delta x^2} + \nu \frac{u_{i,j+1}^n-2 u_{i,j}^n + u_{i,j-1}^n}{\Delta y^2}$$

Una vez más, reorganizamos la ecuación de datos discretos y resolvemos para $u_{i,j}^{n+1}$

$$u_{i,j}^{n+1} = u_{i,j}^n + \frac{\nu \Delta t}{\Delta x^2}(u_{i+1,j}^n - 2 u_{i,j}^n + u_{i-1,j}^n) + \frac{\nu \Delta t}{\Delta y^2}(u_{i,j+1}^n-2 u_{i,j}^n + u_{i,j-1}^n)$$

u[1:,1:]=un[1:-1,1:-1]+nu*dt/dx**2*(un[2:,1:-1]-2*un[1:-1,1:-1]+un[0:-2,1:-1])
+nu*dt/dy**2*(un[1:-1,2:]-2*un[1:-1,1:-1]+un[1:-1,0:-2])

### Declaración de variables
nx = 101
ny = 101
nt = 17#para correr la primera vez
nu=.05
dx = 2.0/(nx-1)
dy = 2.0/(ny-1)
sigma = .25
dt = sigma*dx*dy/nu

x = np.linspace(0,2,nx)
y = np.linspace(0,2,ny)

u = np.ones((ny,nx)) ## crea un vector 1xn de unos
un = np.ones((ny,nx)) ##

### Asignar variables inciales
u[int(.5/dy):int(1/dy+1), int(.5/dx):int(1/dx+1)]=2 ## establece función de sobrero como C.I. : u(.5<=x<=1 && .5<=y<=1 ) is 2


### Función que ejecuta la solución hasta un tiempo nt
def diffuse(nt):
    u[int(.5/dy):int(1/dy+1), int(.5/dx):int(1/dx+1)]=2 #condición initial
    
    for n in range(nt+1): 
        un = u
        u[1:-1,1:-1]=un[1:-1,1:-1]+nu*dt/dx**2*(un[2:,1:-1]-2*un[1:-1,1:-1]+un[0:-2,1:-1])
        +nu*dt/dy**2*(un[1:-1,2:]-2*un[1:-1,1:-1]+un[1:-1,0:-2])
        u[0,:]=1
        u[-1,:]=1
        u[:,0]=1
        u[:,-1]=1

#Aplico la ecuación de la difusióm
nt = 100 #probar con 1000, 2000, 3000
diffuse(nt)

#Ahora graficamos para la condición final
# Initialize figure with 4 3D subplots
fig = make_subplots(
    rows=1, cols=2,
    specs=[[{'type': 'surface'}, {'type': 'contour'}]],subplot_titles=['Difusión 2D', 'Difusión 2D plano (x,y)'],)

# adding surfaces to subplots.
fig.add_trace(
    go.Surface(x=x, y=y, z=u, colorbar_title_text='Velocidad (u)',showscale=True),
    row=1, col=1)

fig.add_trace(
    go.Contour(
        z=u, x=x, y=y,showscale=False,
    ),row=1, col=2)

#import plotly.io as pio
#fig.write_html("plotlygraph/difusion2D.html", include_plotlyjs="cdn", full_html=False, auto_open=True)


Si no se ve, descargar el archivo [aqui](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/web/book/notebooks/plotlygraph/difusion2D.html)

