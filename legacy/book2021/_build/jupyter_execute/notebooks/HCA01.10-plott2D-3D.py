# Ploteo 
## Graficas 2D

En Python, *matplotlib* es el paquete más importante que para hacer un diagrama, puede echar un vistazo a la [Matplotlib](https://matplotlib.org/gallery/index.html#gallery) y tener una idea de lo que se podría hacer allí. Por lo general, lo primero que debemos hacer para crear un gráfico es importar el paquete matplotlib. En el Jupyter notebook, podríamos mostrar la figura directamente dentro del cuaderno y también tener las operaciones interactivas como panorámica, acercar/alejar, etc. usando el comando mágico - *% matplotlib notebook *. 

#Leemos los paquetes que usaremos en esta sección
import numpy as np
import matplotlib.pyplot as plt
%matplotlib inline#Esta linea hace que se plote en jupyter

La función de trazado básica es *plot (x, y)*. La función *plot* toma dos listas / matrices, x e y, y produce una representación visual de los puntos respectivos en *x* e *y*.

#Ejemplo de ploteo basico 2D
x = [0, 1, 2, 3] 
y = [0, 1, 4, 9]
plt.plot(x, y)
plt.show()

La figura anterior que, de forma predeterminada, conecta cada punto con una línea azul (por defecto). Para que la función se vea suave, use puntos de discretización más finos. La función *plt.plot* trazó la linea, y *plt.show ()* le dice a Python que hemos terminado de trazar y muestre la figura.

**Intentemos!** Hacer una función $f(x) = x^2 for -5\le x \le 5$. 

x = np.linspace(-5,5, 100)
plt.plot(x, x**2)
plt.show()

Para cambiar el scatter o la línea, puede poner un tercer argumento de entrada en el gráfico, que es una cadena que especifica el color y el estilo de línea que se utilizará en el gráfico. Por ejemplo, *plot (x, y, 'ro')* trazará los elementos de x contra los elementos de y usando rojo, r, círculos, 'o'. Las posibles especificaciones se muestran a continuación en la tabla.


| Symbol  | Description  | Symbol  |  Description |
|:---:|:---:|:---:|:---:|
|  b | blue  |  T |  T |
|  g | green |  s | square  |
|  r |  red |  d |  diamond |
|  c |  cyan |  v | triangle (down)  |
|  m |  magenta | ^  | triangle (up)   |
|  y |  yellow | <  | triangle (left)   |
|  k | black  |  > |  triangle (right)  |
|  w |  white | p  | pentagram  |
|  . |  point |  h |  hexagram |
|  o |  circle | -  |  solid |
|  x | x-mark  |  : |  dotted |
|  + |  plus | -.  | dashdot  |
|  * |  star |  --  | dashed  |

**Probemos!** Hagamos el mismo ploteo previo pero modificado $f(x) = x^2 for -5\le x \le 5$. 

x = np.linspace(-5,5, 100)
plt.plot(x, x**2, 'g--')
plt.show()

Agregamos otra linea, a través de otra función.

**Intentamos** $f(x) = x^2 and g(x) = x^3 for -5\le x \le 5$. Editamos el grafico agregando el titulo y ejes

plt.figure(figsize = (10,6))

x = np.linspace(-5,5,20)
plt.plot(x, x**2, 'ko')
plt.plot(x, x**3, 'r*')
plt.title(f'Graficamos de las funciones entre {x[0]} to {x[-1]}')
plt.xlabel('Eje X', fontsize = 18)
plt.ylabel('Eje Y', fontsize = 18)
plt.show()

Podemos ver que podríamos cambiar cualquier parte de la figura, como el tamaño de la etiqueta de los ejes x e y, especificando un argumento *fontsize* en la función *plt.xlabel*. Pero hay algunos estilos predefinidos que podríamos usar para cambiar el estilo automáticamente. Aquí está la lista de estilos. 

print(plt.style.available)

plt.style.use('seaborn-poster')

plt.figure(figsize = (10,6))

x = np.linspace(-5,5,20)
plt.plot(x, x**2, 'ko',label = 'quadratic')
plt.plot(x, x**3, 'r*',label = 'cubic')
plt.title(f'Plot of Various Polynomials from {x[0]} to {x[-1]}')
plt.xlabel('X axis')
plt.ylabel('Y axis')
plt.legend(loc = 2)
plt.xlim(-6.6)
plt.ylim(-10,10)
plt.grid()
plt.show()

Python permite crear subplots, (varias gráficas ordenadas tipo matriz). Para ello es necesario usar la función *subplot*. Esta función toma tres entradas: el número de filas, el número de columnas y en qué indice se debe dibujar las función plot. 

*Aclaración*. Aparte de *line* existen otros tipo de graficos. Algunos de ellos son *scatter*, *bar*, *loglog*, *semilogx* y *semilogy*. *scatter* funciona exactamente igual que plot, excepto que por defecto son círculos rojos (es decir, *plot (x, y, 'ro')* es equivalente a *scatter (x, y)*). La función *bar* traza barras centradas en *x* con altura *y*. Las funciones *loglog*, *semilogx* y *semilogy* trazan los datos en x e y con los ejes x e y en una escala logarítmica, el eje x en una escala logarítmica y el eje y en una escala lineal, y el eje y en una escala logarítmica y el eje x en una escala lineal, respectivamente.

**Ejemplo** Plotiemos un subplot de dos filas usano linea y scatter.

x = np.arange(11)
y = x**2

plt.figure(figsize = (14, 8))

plt.subplot(2, 1, 1)
plt.plot(x,y)
plt.title('Plot')
plt.xlabel('X')
plt.ylabel('Y')
plt.grid()

plt.subplot(2, 1, 2)
plt.scatter(x,y)
plt.title('Scatter')
plt.xlabel('X')
plt.ylabel('Y')
plt.grid()

plt.tight_layout()

plt.show()

Ademas podemos guardar la imagene generada en diferentes formatos: pdf, jpeg, o png usando el metodo `plt.savefig`. 

plt.figure(figsize = (8,6))
plt.plot(x,y)
plt.xlabel('X')
plt.ylabel('Y')
plt.savefig('image.jpeg')

Finalmente, existen otras funciones para trazar datos en 2D. La función `errorbar` traza datos x versus y pero con barras de error para cada elemento. La función `polar` grafica Î¸ contra r en lugar de x contra y. La función `stem` traza los tallos en x con la altura en y. La función `hist` crea un histograma de un conjunto de datos; "boxplot" ofrece un resumen estadístico de un conjunto de datos; y `pie` hace un gráfico circular. El uso de estas funciones se deja a su exploración. Recuerde consultar los ejemplos en la [galería de matplotlib](https://matplotlib.org/gallery/index.html#gallery).

## Ploteo 3D

Veremos rapidamente el ploteo 3D

#Cargamos la librerias para plotear 3D
import numpy as np
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
plt.style.use('seaborn-poster')

Creamos nuestra primera figura 3D 

fig = plt.figure(figsize = (8,8))
ax = plt.axes(projection='3d')
ax.grid()
t = np.arange(0, 10*np.pi, np.pi/50)
x = np.sin(t)
y = np.cos(t)

ax.plot3D(x, y, t)
ax.set_title('3D Parametric Plot')

# Set axes label
ax.set_xlabel('x', labelpad=20)
ax.set_ylabel('y', labelpad=20)
ax.set_zlabel('t', labelpad=20)

plt.show()

También podríamos trazar un diagrama de dispersión 3D usando la función *scatter*.

x = np.random.random(50)
y = np.random.random(50)
z = np.random.random(50)

fig = plt.figure(figsize = (10,10))
ax = plt.axes(projection='3d')
ax.grid()

ax.scatter(x, y, z, c = 'r', s = 50)
ax.set_title('3D Scatter Plot')

# Set axes label
ax.set_xlabel('x', labelpad=20)
ax.set_ylabel('y', labelpad=20)
ax.set_zlabel('z', labelpad=20)

plt.show()

Muchas veces necesitamos diagrama de superficie en lugar de un diagrama de líneas cuando se traza en tres dimensiones. En el trazado de superficies tridimensionales, deseamos hacer una gráfica de alguna relación *f (x, y)*. En el trazado de superficie se deben dar todos los pares (x, y). Esto no es sencillo de hacer usando vectores. Por lo tanto, en el trazado de superficies, la primera estructura de datos que debe crear se llama malla. Dadas listas / matrices de valores x e y, una malla es una lista de todas las combinaciones posibles de x e y. En Python, la malla se da como dos matrices X e Y donde X (i, j) e Y (i, j) definen posibles pares (x, y). Entonces se puede crear una tercera matriz, Z, de manera que Z (i, j) = f (X (i, j), Y (i, j)). Se puede crear una malla usando la función *np.meshgrid* en Python. La función *meshgrid* tiene las entradas x e y son listas que contienen el conjunto de datos independientes. Las variables de salida X e Y son las descritas anteriormente.
**PROBEMOS MESH GRID**

x = [1, 2, 3, 4]
y = [3, 4, 5]

X, Y = np.meshgrid(x, y)
print(X)

print(Y)

fig = plt.figure(figsize=(12,6))

x = np.arange(-5, 5.1, 0.2)
y = np.arange(-5, 5.1, 0.2)

X, Y = np.meshgrid(x, y)
Z = np.sin(X)*np.cos(Y)

ax = fig.add_subplot(1, 2, 1, projection='3d')
ax.plot_wireframe(X,Y,Z)
ax.set_title('Wireframe plot')

ax = fig.add_subplot(1, 2, 2, projection='3d')
ax.plot_surface(X,Y,Z)
ax.set_title('Surface plot')

plt.tight_layout()

plt.show()

Podemos encontrar más ejemplos de gráficos 3D de diferentes tipos en el [sitio web del tutorial de mplot3d](https://matplotlib.org/mpl_toolkits/mplot3d/tutorial.html).

## Ploteo Interactivo (Plotly)

La librería de gráficos Python de Plotly crea gráficos interactivos con calidad de publicación. Ejemplos de cómo hacer diagramas de líneas, diagramas de dispersión, gráficos de áreas, gráficos de barras, barras de error, diagramas de caja, histogramas, mapas de calor, subtramas, múltiples ejes, gráficos polares y gráficos de burbujas.
[Plotly.py](https://plotly.com/python/) es gratuito y de código abierto y puede ver el código fuente, informar problemas o contribuir en [GitHub](https://github.com/plotly/plotly.py).

#Hacemos una grafica ejemplos
import plotly.express as px

fig = px.line(x=["a","b","c"], y=[1,3,2], title="Ejemplo de figura")
#print(fig)
fig.show()

#Otra mas
import plotly.express as px
df = px.data.iris()
fig = px.scatter(df, x="sepal_width", y="sepal_length", color="species",
                 size='petal_length', hover_data=['petal_width'])
fig.show()

### Ejemplos de paginas hechas en Plotly

* [ARCOVID](https://arcovid19.gitlab.io/viz/)

* [Obras Hidráulicas I](https://ldominguezruben.gitlab.io/obrashidraulicasi/)

* [Proyecto Victimas](https://proyecto-v-ctimas-de-la-dictadura.gitlab.io/mapeos/index2.html)

