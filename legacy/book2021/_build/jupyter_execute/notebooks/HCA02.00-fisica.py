# TEMA II. Mecánica de fluídos

---
* II.1 Funciones de variables complejas
* [**II.2 Ley de Conservación**](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/web/book/notebooks/ref/Ley_de_Conservacion_CH.pdf)
* [**II.3 Sistemas simplificados**](HCA02.02-sistemas-simplificados.ipynb)
* [***II.3.1 Clasificación de Ecuaciones Diferenciales Parciales (EDP)***](HCA02.02.01-EDP.ipynb)
* **II.4 Turbulencia**
* [***II.4.1 Caos***](HCA02.03.01-caos.ipynb)
* [**II.5 Ecuación de Navier Stokes. Resumen**](HCA02.04-Navier-Stokes.ipynb)
* [***II.5.1 Modelos de Cierre de turbulencia. Resumen***](https://www.youtube.com/watch?v=zIQpxmLwbXQ)
* [**II.6 Condiciones de borde**](HCA02.05-condiciones-de-borde.ipynb)


--- 

## Motivación
  

---

Algunas de las herramientas más poderosas de la mecánica clásica, incluida la mecánica de fluidos, son la ley de conservación. A partir de los conocimientos físicos de Newton, Leibniz y otros, estas leyes garantizan que se conserva la cantidad total de determinadas cantidades físicas dentro de un volumen. Por ejemplo, la conservación de la masa asegura que la masa total de un sistema permanece constante, la conservación del movimiento asegura que éste sistema permanezca constante, y la conservación de
energía asegura que la energía total de un sistema permanezca constante. Si bien estos conceptos pueden ser aplicado para problemas relativamente sencillos como colisiones elásticas/inelásticas entre partículas, su aplicación a la mecánica de fluidos es menos trivial. Sin embargo, los conceptos fundamentales de conservación de masa, momento y energía todavía se aplican a los fluidos tan bien como a
partículas individuales, sólo las matemáticas se vuelven más complejas. Se espera que
los estudiantes hayan realizado ya el curso de mecánica de fluidos y esten familiarizado con las leyes de conservación. No obstante, en esta parte revisaremos los conceptos para garantizar su integridad, y establecer la notación utilizada en el resto del curso.

