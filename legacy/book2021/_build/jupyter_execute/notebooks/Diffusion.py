# Linear Diffusion

In this notebook, we will explore the second-order central scheme for the linear diffusion equation.

To run each of the following cells, use the keyboard shortcut **SHIFT** + **ENTER**, press the button ``Run`` in the toolbar or find the option ``Cell > Run Cells`` from the menu bar. For more shortcuts, see ``Help > Keyboard Shortcuts``.

To get started, import the required Python modules by running the cell below.

# Configuration for visualizing the plots
%matplotlib notebook
%config InlineBackend.figure_format = 'retina'

# Required modules
import numpy as np
import matplotlib.pyplot as plt

# Import figure style and custom functions
import nbtools as nb

Run the cell containing the function ``diffusion``. Read the comments describing each of the steps.

def diffusion(b, L, n, dt, tf):
    # Build grid
    dx = L/n
    x = np.linspace(0, L - dx, n)
    
    # Initialize solution
    u = np.exp(-40*(x - 1/2)**2)
    ut = np.zeros(u.shape)

    # Advance solution in time
    t = 0
    c = b*dt/dx**2
    while(t < tf):
        for i in range(n):
            if i == 0:
                # Enforce periodic boundary condition at x=0
                ut[i] = u[i] + c*(u[n - 1] - 2*u[i] + u[i + 1])
            elif i == n - 1:
                # Enforce periodic boundary condition at x=L
                ut[i] = u[i] + c*(u[i - 1] - 2*u[i] + u[0])
            else:
                ut[i] = u[i] + c*(u[i - 1] - 2*u[i] + u[i + 1])
        u[:] = ut[:]
        t += dt
    
    plt.plot(x, u, 'o-', markersize=2, label=f'$n={n}$')
    plt.legend()

Create a matplotlib figure and add labels to each axis. Plots will appear in this figure.

**Note**: You can always run this cell again to clear the figure.

plt.figure(0)
plt.xlabel('$x$')
plt.ylabel('$u(x,t)$')

Now run the function ``advection`` providing
   - ``b``: The diffusion coefficient equal to ``1e-2``,
   - ``L``: The domain length equal to ``1``,
   - ``n``: The number of grid points equal to ``10``,
   - ``dt``: The time step size equal to ``0.001``,
   - ``tf``: The final time equal to ``5``.

# Make sure plot uses figure above
plt.figure(0)

# Assign the corresponding values to the following variables
b =
L = 
n = 
dt = 
tf =

diffusion(b, L, n, dt, tf)

Now, call the function ``diffusion`` again with 20, 40, 80 and 160 grid points. Keep the rest of the inputs the same. What behaviour do you observe? The result must be equal to the figure shown in the Linear Diffusion section of the Finite Difference chapter.

# Make sure plot uses figure above
plt.figure(0)

# Call diffusion in this cell as many times as required. 
# You may use a for loop. 
# Note that the figure will be updated above.

Increase the time-step size by a factor of 2. Is the simulation stable?. What can you conclude about the size of the time-step for this scheme relative to the grid spacing?

