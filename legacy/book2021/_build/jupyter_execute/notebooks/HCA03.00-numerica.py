# TEMA III. Numérica

---

* [**III.1 Serie de Taylor**](HCA03.01-serie-de-taylor.ipynb)
* [**III.2 Metodo de Diferencias Finitas**](HCA03.02-metodos-de-diferencias-finitas.ipynb)
* [***III.2.1 MDF Problema de Advección Lineal 2D***](HCA03.02.01-mdf-adveccion-lineal-2D.ipynb)
* [***III.2.2 MDF Problema de Difusión Lineal 2D***](HCA03.02.02-mdf-difusion-lineal-2D.ipynb)
* [***III.2.3 MDF Problema de Burgers 2D***](HCA03.02.03-mdf-burgers-2D.ipynb)
* [**III.3 Metodo de Volumenes Finitas**](HCA03.03-metodos-de-volumenes-finitos.ipynb)
* [**III.4 Consistencia, estabilidad y convergencia**](HCA03.04-consistencia-estabilidad-convergencia.ipynb)

--- 

## Motivación
  

---

En esta parte vamos a ver como resolvemos la ecuación de Navier Stokes usando aproximaciónes o esquemas resolutivos.

