# Introducción a Machine Learning

**Machine learning**, as the name suggest, are a group of algorithms that try to enable the learning capability of the computers, so that they can learn from the data or past experiences. The idea is that, as a kid, we gain many skills from learning. One example is that we learned how to recognize cats and dogs from a few cases that our parents showed to us. We may just see a few cats and dogs pictures, and next time on the street when we see a cat, even though it may be different from the pictures we saw, we know it is a cat. This ability to learn from the data that presented to us and later can be used to generalize to recognize new data is one of the things we want to teach our computers to do. 

Machine learning algorithms are used in many places in our life that you may not realize it. For example, your voice assistant on your smartphone, the ATM machine recognize the deposit checks, your email provider automatically separate the good emails from the junk ones, the self-driving cars drive without a driver, facial recognition at some of the security checks, and many more examples. Therefore, in this chapter, we will introduce you some of the basic concepts of machine learning as a way to motivate you to learn more beyond this book. Let's first start to see what makes of a machine learning algorithm.   

## Types of machine learning

There are different types of machine learning algorithms. One popular way to classify them is shown in the following figure:

<img src="images/25.01.01-types-of-ML.jpg" alt="ML types" title="Different types of machine learning" width="500"/>

Usually, we classify machine learning into two main categories, i.e. **supervised learning** and **unsupervised learning**. The supervised learning as the name suggested, is that during the training of the algorithm, we know the correct **label**, which means we know the answers of the problem, this prior information will be used in the training. Within the supervised learning, depending on the nature of the output, we can divide the algorithms into **classification** and **regression**. For example, if we are asked to design an algorithm to recognize apple and oranges, and we know which object is apple or orange, then this problem is the classification problem, since the output will be categorical data, either orange or apple. The different objects are usually called **classes**. The fact that we know the apple or orange makes it into a supervised learning. In another example, if we want to predict your final exam scores (0 - 100) based on your each homework score, the output is a quantity from 0 to 100, then this is a regression problem. The other category of the algorithms are unsupervised learning, which means we don't have the luxury of the labels. For example, if I give you a mixed apple and oranges, but don't tell you which one is apple, which one is orange. Then this will be a **clustering** problem, which you need to use some of the hidden characteristics of the objects to group them together. **Dimensionality reduction** is a group of algorithms within the category of unsupervised learning to reduce the higher dimension problems into lower dimension ones. As a human being, we like to work in lower dimensions, for example, our world is a 3-dimensional environment, and people reduce it to 2-dimension to plot it on the map for easier visualizations. Algorithms are the same, sometimes it is easier to work in the lower dimensions to make things easier, therefore, the algorithms that can reduce the dimensionality will be useful in these cases. 

Of course, there are many more other types of machine learning, such as reinforcement learning, we will not talk about it here. The above types are the most common ones, in this chapter, we will give you a brief tour of classification, regression and clustering and show you how to quickly do it in Python using one package. 

## The components of machine learning

<img src="images/25.01.02-components-of-ML.png" alt="ML components" title="The components of machine learning" width="500"/>

The above figure shows the main components of a machine learning algorithm:

* representation of the data
* a tunable model
* optimization algorithm to tune the model
* a trained model

**Representation of the data**

There are many types of data, such as images, time series, text documents, numerical data, and so on, we need to turn the data into some format that the computers can recognize it. For example, one way we can do to represent data for machine learning is shown as following figure:

<img src="images/25.01.03-representation_of_data.png" alt="Data representation" title="The representation of the data" width="500"/>

We can see that we usually represent the input data into a matrix, with each row represent one sample of the data (or instance of data). For example, if we have a problem to recognize apples and oranges, then each row can store one apple or one orange. The columns are different **features**, usually characterize the different objects, in this case, it can be color, texture, shape, smell and so on of the objects. This matrix is usually called **feature matrix** which represent our data in a way that can feed into the computer. We will also have a target array, which stores the output of the values. Either it will be the label of the different classes, or the quantities. There are also other representations of the data, for example, the data is still stored in an array, but each row is saving a 2D array for an image. 

**Tunable model**
After we have the input and output data ready, we need an algorithm that can learn from the data. The algorithms are usually have many different parameters that can be tuned in a way that the model can become better for our goal based on the data we present to it. There are many different tunable models such as artificial neural network support vector machine, random forest, logistic regression, and so on. Each model has different ideas behind it and be tuned differently. 

**Optimization algorithm**
This is the main working force behind the machine learning algorithms. We need to tune the model, but how? Usually we need to define some sort of **objective function** and use the optimization algorithms to either minimize or maximize it. The objective function could be calculated by the difference/error between the estimation and the target (true answer), therefore, usually the smaller the error is, the better. There are different optimization algorithms to tune the model, such as gradient descent, least square minimization, and so on. 

**Trained model**
After the tuning of the model, it has the capability of making a classification or prediction and so on. This trained model can be used on the new data for achieving our goal. 

## Classification

Classification is a very common problems in the real world. For example, we want to classify some products into good and bad quality, emails into good or junk, books into interesting or boring, and so on. As discussed before, two key factors make a problem into a classification problem, (1) the problem has correct answer (labels), and (2) the output we want is categorical data, such as Yes or No, or different categories.   

Let's again see an intuitive example, the classification of a group of apples and oranges. 

<img src="images/25.02.01-classification-example.png" alt="Classification" title="Classify apples and oranges" width="500"/>

We can first calculate features for each apple and orange and save it into the feature matrix as shown in the above figure. Since we only have two classes, this problem is usually called **binary classification** problem. Therefore, we can think that 0 represents orange, and 1 refers to apple. Since we have 5 features in the figure, it is not easy to visualize it. If we only plot the first two features, i.e. color and texture, we may see something as below:

<img src="images/25.02.01-classification-example_2.png" alt="Classification example" title="Plotting the first two features (color and texture) for the problem" width="500"/>

We can see that the blue dots (apples) and orange square (oranges) falls on the different parts in the figure. The classification problem essentially is a problem to find a **decision boundary**, either a straight line or other curves, to separate them. The tuning of the algorithm is basically to move this line or find out the shape of the boundary, but in a higher dimension (in this case 5 dimensions in total, but we can also do the job with only two features as show in the figure). 

## Support vector machine basics

One popular way to do the job is the **support vector machine** (SVM). It is a very intuitive algorithm based on how we make the decision. Let's look at the following figure, and ask the question "which line boundary is better?" The black dotted line or the red solid line?

<img src="images/25.02.02-svm_select.png" alt="SVM select" title="Which line boundary is better in the above figure" width="500"/>

Most people will choose the red solid line, because it is in the middle of the gap between the two groups. As shown in the following figure, if we have a new data point (the blue dot), then the black dotted line model will make the wrong decision. Therefore, the model which has a line close to the middle of the gap and far away from both classes are better ones. This intuition from us need to be formalized into a way that the computer can do this. This is the design of the SVM algorithm, it first forms a buffer from the boundary line to the points in both classes that close to the line (these are the support vectors, where the name comes from). Then the problem becomes given a set of these support vectors, which line has the maximum buffer.


<img src="images/25.02.03-svm_new_datapont.png" alt="SVM new data" title="A new data point added in" width="500"/>

As shown in the following figure, the black dotted line has a narrow buffer while the red solid line has a wider buffer. Based on the above discussion, we should choose the red solid line which matches our intuition. 

<img src="images/25.02.04-svm_buffer.png" alt="SVM buffer" title="Different buffers of the boundaries for two cases" width="500"/>

If we plot out the support vectors, it shows in the following figure. 

<img src="images/25.02.05-svm_support_vectors.png" alt="SVM support vectors" title="The support vectors associated with the red solid line model" width="300"/>

## Support vector machine in Python

There are many different packages in Python that can let you use different machine learning algorithms really easy. The most popular package for general machine learning is [scikit-learn](https://scikit-learn.org/stable/), which contains many different algorithms. There are also packages more towards deep learning, such as tensorflow, pytorch and so on, but we will not cover them here. In this chapter, we will only use scikit-learn to learn these basics. You can easily install scikit-learn use a package manager. 

Let's see an example how to use it. We start by loading some [pre-existing datasets](http://scikit-learn.org/stable/modules/classes.html#module-sklearn.datasets) in the scikit-learn, which comes with a few standard datasets. For example, the [iris](https://en.wikipedia.org/wiki/Iris_flower_data_set) and [digits](http://archive.ics.uci.edu/ml/datasets/Pen-Based+Recognition+of+Handwritten+Digits) datasets for classification and the [boston house prices](http://archive.ics.uci.edu/ml/datasets/Housing) dataset for regression. Using these existing datasets, we can easily test the algorithms that we are interested in. We will use the iris dataset for this section.

A dataset is a dictionary-like object that holds all the data and some metadata about the data. This data is stored in the .data member, which is a n_samples, n_features array. In the case of supervised problem, one or more target variables are stored in the .target member.

**Load iris data**

The iris dataset consists of 50 samples from each of three species of Iris (Iris setosa, Iris virginica and Iris versicolor). Four features were measured from each sample: the length and the width of the sepals and petals, in centimetres.


| [![Iris Setosa](https://upload.wikimedia.org/wikipedia/commons/5/56/Kosaciec_szczecinkowaty_Iris_setosa.jpg)](https://en.wikipedia.org/wiki/Iris_setosa)  | [![Iris Virginica](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Iris_virginica.jpg/1920px-Iris_virginica.jpg)](https://en.wikipedia.org/wiki/Iris_virginica) | [![Iris Versicolor](https://upload.wikimedia.org/wikipedia/commons/2/27/Blue_Flag%2C_Ottawa.jpg)](https://en.wikipedia.org/wiki/Iris_versicolor) |
|:---:|:---:|:---:|
| Iris Setosa| Iris Virginica| Iris Versicolor|

Now let's use scikit-learn to train a SVM model to classify the different species of Iris. In order to have a better visualization, we will only use two features that can characterize the differences between the classes. 

import numpy as np
import itertools
import matplotlib.pyplot as plt
from sklearn import svm, datasets
from sklearn.metrics import classification_report

plt.style.use('seaborn-poster')
%matplotlib inline

# import the iris data
iris = datasets.load_iris()

print(iris.feature_names)
# only print the first 10 samples
print(iris.data[:10])
print('We have %d data samples with %d \
    features'%(iris.data.shape[0], iris.data.shape[1]))

The data is always a 2D array, shape (n_samples, n_features), although the original data may have had a different shape. The following prints out the target names and the representatoin of the target using 0, 1, 2. Each of them represent a class. 

print(iris.target_names)
print(set(iris.target))

Let's prepare the feature matrix $X$ and also the target $y$ for our problem. 

# let's just use two features, so that we can 
# easily visualize them
X = iris.data[:, [0, 2]]
y = iris.target
target_names = iris.target_names
feature_names = iris.feature_names
# get the classes
n_class = len(set(y))
print('We have %d classes in the data'%(n_class))

If we can, we always want to plot the data out first to explore it. We can plot it as a scatter plot with different symbols for different classes. We can see with this two features, we can actually see they separate out from each other. Also, the boundary between these classes are fairly linear, thus all we need to do is to find a linear boundary between them. 

# let's have a look of the data first
colors = ['b', 'g', 'r']
symbols = ['o', '^', '*']
plt.figure(figsize = (10,8))
for i, c, s in (zip(range(n_class), colors, symbols)):
    ix = y == i
    plt.scatter(X[:, 0][ix], X[:, 1][ix], \
                color = c, marker = s, s = 60, \
                label = target_names[i])

plt.legend(loc = 2, scatterpoints = 1)
plt.xlabel('Feature 1 - ' + feature_names[0])
plt.ylabel('Feature 2 - ' + feature_names[2])
plt.show()

We now use the SVM in scikit-learn. The API is quite simple, for most of the algorithms they are similar. The use of the different algorithms are usually the following steps:

**Step 1:** initialize the model
**Step 2:** train the model using the *fit* function
**Step 3:** predict on the new data using the *predict* function

# Initialize SVM classifier
clf = svm.SVC(kernel='linear')

# Train the classifier with data
clf.fit(X,y)

The above print out from the *fit* function is the parameters used in the model, we can see that usually for a model there are many different parameters that you may need to tune. For SVM, two most important parameters are _C_ and *gamma*. We won't go into the details here, but a good advice is that before you use the model, always try to understand what these parameters are to get a good model. Now let's use the predict function on the training data, usually we don't do this, we need to split the data into training and testing dataset. For the testing dataset, which is not used in training at all, it is only saved for evaluation purposes. Here for simplicity, we just have a look of the results on the training data we used. 

# predict on the data
clf.predict(X)

We can plot the decision boundary for the model. The following function plot the decision boundary. 

# Plotting decision regions
def plot_desicion_boundary(X, y, clf, title = None):
    '''
    Helper function to plot the decision boundary for the SVM
    '''
    
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.01),
                         np.arange(y_min, y_max, 0.01))

    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    
    plt.figure(figsize = (10, 8))
    plt.contourf(xx, yy, Z, alpha=0.4)
    
    for i, c, s in (zip(range(n_class), colors, symbols)):
        ix = y == i
        plt.scatter(X[:, 0][ix], X[:, 1][ix], \
                    color = c, marker = s, s = 60, \
                    label = target_names[i])
    
    if title is not None:
        plt.title(title)
    
    plt.xlabel('Feature 1')
    plt.ylabel('Feature 2')
    plt.show()
    
plot_desicion_boundary(X, y, clf)

We can see the linear boundaries found by the SVM for the 3 classes are generally good, and can separate most of the samples. 

Now we see how we can train a model to do the classification in Python, there are also many other models that you can use in scikit-learn, we leave this for you to explore. For example, you can use an artificial neural network (ANN) to do the same job (hint: use the *MLPClassifier* for the ANN classifier). 

# Regression

Regression is the set of algorithms in supervised learning that the output is quantity numbers instead of categorical data. We have covered least-square regression in chapter 16 for simple cases that we have an analytic form to fit the data. But machine learning approach are more flexible that you can fit any functions of data without knowing the underlying analytic form at all. For example, you can use random forest, artificial neural network, support vector machine and so on to do regression. In this section, we will introduce artificial neural network (ANN) approach, which is a very flexible model that can work on classification and regression easily. 

## Artificial Neural Network Basics
The **artificial neural network** method is developed to try to mimic how our brain works, i.e. neurons in our brain get information and process it, then some process controls whether the neuron will fire a signal to neighboring or specific neurons. The following figure shows a common structure of a multi-layer artificial neural network. It has 3 different layers, i.e. the input layer, the hidden layer, and the output layer. In each layer, there are neurons that are shown as circles in the figure. We can see from the figure that the input layer has 3 neurons in this case as the 3 features input into the network (you can definitely have more features, thus more neurons in the input layer). The middle layer is the hidden layer, you can have many hidden layers as well, in this figure, we only have 1 hidden layer for illustration purposes. The neurons in the hidden layers are the main processing units in the network to process the data, usually in each neuron, it does two things, (1) sum of the information passed from the previous layer, (2) pass the summation of the information to an activation function to decide if it fires a signal. The output layer has neurons as well to generate the output, for classification, it generate a number between 0 and 1, and for regression problem, we can generate any numbers. We see there are many arrows between different layers, these are called links in neural network, they are responsible to pass the information from one layer to another. Each of the link has a weight associated with it, these are the tunable parameters that we can tune the model. When we have information passing in from the input layer, it flows through the links to the hidden layer to get processed and then to the output layer to generate the results. Usually this is called **forward propagation**. 


<img src="images/25.03.01-ANN.png" alt="ANN" title="The structure of an Artificial Neural Network" width="500"/>

As described above, the training of the ANN algorithm is to find a way to tune the weights on each of the links. The way we do this in ANN is to have an objective function that calculate the errors between the model estimations and the true targets. We can first do the forward propagation to get an estimation and calculate the error, and propagate this error backwards from the the output layer to the input layer. Depending on the contributions to the errors for each link, we can update the weights on the links to make sure that next time when we forward pass the information, the error will decrease. This is called **backpropagation**, we can pass the same information forward and backward for multiple times (this is usually called epochs) to continuously decrease the error until it satisfies our goal. This is how we train an ANN algorithm. Usually we use an optimization algorithm called **gradient descent** to update the weights. 

Let's look at an example for ANN in scikit-learn. 

**Generate data** 

First, we need to generate a toy dataset that we will use to train the model. We generate a periodical dataset using two sine wave with different period, and then add some noise to it. It can be visualized in the following figure:

import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
plt.style.use('seaborn-poster')
%matplotlib inline

np.random.seed(0)
x = 10 * np.random.rand(100)

def model(x, sigma=0.3):
    fast_oscillation = np.sin(5 * x)
    slow_oscillation = np.sin(0.5 * x)
    noise = sigma * np.random.randn(len(x))

    return slow_oscillation + fast_oscillation + noise

plt.figure(figsize = (10,8))
y = model(x)
plt.errorbar(x, y, 0.3, fmt='o')

We can then use ANN to fit the data, the basic idea is that to use each data point in $x$ as the input feature for ANN, and output will be a sequence - $y$. In scikit-learn, the ANN model for regression is *MLPRegressor*, which standards for Multi-layer Perceptron regressor. Let's use the above noisy data as the input into the ANN algorithm.  

from sklearn.neural_network import MLPRegressor

mlp = MLPRegressor(hidden_layer_sizes=(200,200,200), \
                   max_iter = 2000, solver='lbfgs', \
                   alpha=0.01, activation = 'tanh', \
                   random_state = 8)

xfit = np.linspace(0, 10, 1000)
ytrue = model(xfit, 0)
yfit = mlp.fit(x[:, None], y).predict(xfit[:, None])

plt.figure(figsize = (10,8))
plt.errorbar(x, y, 0.3, fmt='o')
plt.plot(xfit, yfit, '-r', label = 'predicted', \
         zorder = 10)
plt.plot(xfit, ytrue, '-k', alpha=0.5, \
         label = 'true model', zorder = 10)
plt.legend()
plt.show()

We can see the fitting of the data actually is not bad. For the above model, we used 3 hidden layers and in each layer, we used 200 neurons. We use an optimization algorithm - 'lbfgs', which is an optimizer in the family of quasi-Newton methods. The parameter *max_iter* defines that we will do at most 2000 iterations. 

## Clustering

Clustering is a set of unsupervised learning algorithms. They are useful when we don't have any labels of the data, and the algorithms will try to find the patterns of the internal structure or similarities of the data to put them into different groups. Since they are no labels (true answer) associated with the data points, we can not use these extra bits of information to constrain the problem. But instead, there are other ways that we can solve the problem, in this section, we will take a look of a very popular clustering algorithm - K-means and understand  

## K-means Basics
There are many different clustering algorithms, K-means is a commonly used clustering algorithm due to its simple idea and effectiveness.  

**The idea of K-means**  

The basic idea behind K-means is that if I have two points are close to each other than the rest points, they will be similar. Therefore, it is based on the distance between two points. For a 2 dimensional problem (can be in higher dimensions as well), K-means uses the **Euclidean distance** to calculate the distance between two given points:

$$dist = \sqrt{(x_1 - x_2)^2 + (y_1 - y_2)^2}$$

The "K" in the name means that there will be K clusters. K-means is an iterative algorithm to update the centroid of the clusters until it reaches the best solution. Let's see an example how it works using only 2 dimensional problem. 

**Step 1 Randomly drop K centroids** 

The first step of K-means is randomly drop K centroids for the data, as shown in the following figure, which the data points are plotted on the 2 dimensional features, we don't know which data points belong to which cluster, therefore, we drop two initial centroids as shown as the two triangles. 
<img src="images/25.04.01-clustering_0.png" alt="K-means" title="Random centroid" width="500"/>

**Step 2 Assign points to the centroids** 

In this step, we assign the points to the closest centroid. We use the above distance calculation to estimate the distance of a point to the two centroids and assign the point to the closest centroid. We do this for all the data points as shown in the following figure. 

<img src="images/25.04.01-clustering_1.png" alt="K-means" title="Assign points to current centroid" width="500"/>

**Step 3 Updates the centroids** 

Since we have an initial assignments of the points, we have two clusters. They are not perfect, but we can re-calculate the centroid for each cluster. The figure below shows the newly calculated centroids.  

<img src="images/25.04.01-clustering_2.png" alt="K-means" title="Update centroid to a new location based on the assigned points" width="500"/>

**Step 4 Repeat step 2 and step 3 until the centroids not move** 

We can repeat the above two steps iteratively to re-assign the points and update the centroids based on the re-assigned points. 

<img src="images/25.04.01-clustering_3.png" alt="K-means" title="Assign points to the updated centroid" width="500"/>

This process continues until the calculated centroids not change anymore. This means that we are reaching a stable state that all the points are assigned to the closest centroids until no points changes their association/clusters.  

<img src="images/25.04.01-clustering_4.png" alt="K-means" title="Update centroid to a new location based on the assigned points" width="500"/>

## K-means in Python

Let's use the Iris data we used before, but this time, assume that we don't know the label for each data point. Therefore, when we plot the data points without the labels, it is shown as the following. 

import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets
plt.style.use('seaborn-poster')
%matplotlib inline

# import the iris data
iris = datasets.load_iris()
# let's just use two features, so that we can 
# easily visualize them
X = iris.data[:, [0, 2]]
y = iris.target
target_names = iris.target_names
feature_names = iris.feature_names
# get the classes
n_class = len(set(y))

# let's have a look of the data first
plt.figure(figsize = (10,8))

plt.scatter(X[:, 0], X[:, 1], \
            color = 'b', marker = 'o', s = 60)

plt.xlabel('Feature 1 - ' + feature_names[0])
plt.ylabel('Feature 2 - ' + feature_names[2])
plt.show()

Now we can use the K-means as before, i.e. initialize the model and use *fit* function to train the algorithm. 

from sklearn.cluster import KMeans

kmean = KMeans(n_clusters=3, random_state = 0)
kmean.fit(X)

The results of the found clusters are saved in the *labels* attribute and the centroids are in the *cluster_centers_*. Let's plot the clustering results and the real species in the following figure. The left figure shows the clustering results with the bigger symbol as the centroids of the clusters. 

# let's have a look of the data first
colors = ['b', 'g', 'r']
symbols = ['o', '^', '*']
fig = plt.figure(figsize = (10,8))
ax = fig.add_subplot(121)
ax2 = fig.add_subplot(122)
for i, c, s in (zip(range(n_class), colors, symbols)):
    ix = kmean.labels_ == i
    ax.scatter(X[:, 0][ix], X[:, 1][ix], \
                color = c, marker = s, s = 60, \
                label = target_names[i])
    loc = kmean.cluster_centers_[i]
    ax.scatter(loc[0], loc[1], color = 'k', \
               marker = s, linewidth = 5)
    
    ix = y == i
    ax2.scatter(X[:, 0][ix], X[:, 1][ix], \
                color = c, marker = s, s = 60, \
                label = target_names[i])
    

plt.legend(loc = 4, scatterpoints = 1)
ax.set_xlabel('Feature 1 - ' + feature_names[0])
ax.set_ylabel('Feature 2 - ' + feature_names[2])
ax2.set_xlabel('Feature 1 - ' + feature_names[0])
ax2.set_ylabel('Feature 2 - ' + feature_names[2])
plt.tight_layout()
plt.show()

We can see from the above figure, the results are not too bad, they are actually quite similar to the true classes. But remember, we get this results without the labels only based on the similarities between data points. We can also predict new data points to the clusters using the *predict* function. The following predict the cluster label for two new points. 

new_points = np.array([[5, 2], 
                      [6, 5]])
kmean.predict(new_points)