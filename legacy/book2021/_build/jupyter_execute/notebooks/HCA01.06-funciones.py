# Funciones

En programación, una **function** es una secuencia de instrucciones que realiza una tarea específica. Una función es un bloque de código que se puede ejecutar cuando se llama. Una función puede tener **argumentos de entrada**, que el usuario, la entidad que llama a la función, pone a su disposición. Las funciones también tienen **parámetros de salida**, que son los resultados de la función que el usuario espera recibir una vez que la función ha completado su tarea. Por ejemplo, la función *math.sin* tiene un argumento de entrada, un ángulo en radianes y un argumento de salida, una aproximación a la función sin calculada en el ángulo de entrada. La secuencia de instrucciones para calcular esta aproximación constituye el **cuerpo de la función**, que hasta este punto no se ha mostrado.

Ya vimos muchas funciones integradas de Python, como *type*, *len*, etc. También vimos las funciones de algunos paquetes, por ejemplo, *math.sin*, *np.array* entre otras. 

type(len)

## Estructura de funciones

Podemos definir nuestras propias funciones. Una función se puede especificar de varias formas. Aquí presentaremos la forma más común de definir una función que se puede especificar usando la palabra clave *def*, como se muestra a continuación:

```python
def nombre_de_funcion(argumento_1, argumento_2, ...): #Encabezado
                                                      #Cuerpo de función
    '''                                               
    Descripción: Se explica que hace la función
    '''
    # Cuerpo o declaraciones de la función
    estructura_de_función
    # Salida
    return parametro_de_salida (opcional)

```

Pudimos ver que la definición de una función de Python necesita los siguientes dos componentes:

1. **Encabezado de función:** El encabezado de una función comienza con una palabra clave *def*, seguida de un par de paréntesis con los argumentos de entrada dentro y termina con dos puntos (:)

2. **Cuerpo de la función:** Un bloque con sangría (generalmente cuatro espacios en blanco) para indicar el cuerpo principal de la función. Consta de 3 partes:

    * Descripción: Una cadena que describe la función a la que podría acceder la función *help()* o el signo de interrogación. Puede escribir cualquier cadena dentro, podrían ser varias líneas.
    * Cuerpo y declaración de función: Estas son las instrucciones paso a paso que la función ejecutará cuando la llamemos. También puede notar que hay una línea que comienza con '#', esta es una línea de comentario, lo que significa que la función no la ejecutará.
    * Salida: Una función podría devolver algunos parámetros después de que se llama a la función, pero esto es opcional, podríamos omitirlo. Se puede devolver cualquier tipo de datos, incluso una función, explicaremos más adelante.
    
**TIP!!** Cuando su código se vuelve más largo y complicado, los comentarios le ayudarán a usted y a quienes lean el código a navegar a través de él y comprender lo que está tratando de hacer. Adquirir el hábito de comentar con frecuencia lo ayudará a evitar que cometa errores de codificación, a comprender a dónde va su código cuando lo escribe y a encontrar errores cuando comete errores. También es habitual poner una descripción de la función, el autor y la fecha de creación en la cadena descriptiva debajo del encabezado de la función, aunque es opcional (puede omitir la cadena descriptiva). Recomendamos encarecidamente que comente mucho en su propio código.

Hacemos una función simple que sume 3 numeros. 

def mi_suma(a, b, c):
    """
    funcion que suma 3 numeros
    Entrada: 3 numeros como a, b, c
    Output: la suma de los 3 numeros a, b, y c
    author: LGDR
    date:
    """
    
    # esta es la operación de suma
    out = a + b + c
    
    return out

OJO con la identación!!!

def mi_suma(a, b, c):
    """
    funcion que suma 3 numeros
    Entrada: 3 numeros como a, b, c
    Output: la suma de los 3 numeros a, b, y c
    author: LGDR
    date:
    """
    
# esta es la operación de suma
out = a + b + c
    
return out

**TIP!!!** Los espacios en blanco de tipo 4 son un nivel de sangría, puede tener una sangría de nivel más profundo cuando tiene una función anidada o una declaración if (veremos esto en el próximo sección). Además, a veces es necesario incorporar sangría o quitarla de un bloque de código. Puede hacer esto seleccionando primero todas las líneas en el bloque de código y presionando *Tab* y *Shift + Tab* para aumentar o disminuir un nivel de sangría.

Ejemplo de función que no se entiende nada.

def abc(a, s2, d):
    z = a + s2
    z = z + d
    x = z
    return x

Las funciones deben ajustarse a un esquema de nomenclatura similar a las variables. Solo pueden contener caracteres alfanuméricos y guiones bajos, y el primer carácter debe ser una letra.

**TIP!!** Convencionalmente, como los nombres de las variables, los nombres de las funciones deben estar en minúsculas, con palabras separadas por guiones bajos según sea necesario para mejorar la legibilidad.

**TIP** Es una buena práctica de programación guardar con frecuencia mientras escribe su función. De hecho, muchos programadores informan que ahorran usando el atajo ctrl + s (PC) cada vez que dejan de escribir.

Probemos la función previa

d = mi_suma(1, 2, 3)
d

help(mi_suma)

**Que hizo Python** Primero recuerde que el operador de asignación trabaja de derecha a izquierda. Esto significa que *mi_suma(1,2,3)* se resuelve antes de la asignación a *d*.

1. Python encuentra la función *mi_suma*.
2. *mi_suma* toma el primer valor de argumento de entrada 1 y lo asigna a la variable con el nombre *a* (primer nombre de variable en la lista de argumentos de entrada).
3. *mi_suma* toma el valor 2 del segundo argumento de entrada y lo asigna a la variable con el nombre *b* (nombre de la segunda variable en la lista de argumentos de entrada).
4. *mi_suma* toma el tercer valor de argumento de entrada 3 y lo asigna a la variable con el nombre *c* (tercer nombre de variable en la lista de argumentos de entrada).
5. *mi_suma* calcula la suma de *a*, *b* y *c*, que es 1 + 2 + 3 = 6.
6. *mi_suma* asigna el valor 6 a la variable *out*.
7. *mi_suma* genera el valor contenido en la variable de salida *out*, que es 6.
10. *mi_suma (1,2,3)* es equivalente al valor 6, y este valor se asigna a la variable con el nombre *d*.

Python le da al usuario una enorme libertad para asignar variables a diferentes tipos de datos. Por ejemplo, es posible darle a la variable x un valor de diccionario o un valor flotante. En otros lenguajes de programación, este no es siempre el caso, debe declarar al comienzo de una sesión si x será un diccionario o un tipo flotante, y luego se quedará con él. Por ejemplo, *mi_suma* se construyó asumiendo que los argumentos de entrada eran de tipo numérico, ya sea int o float. Sin embargo, el usuario puede ingresar accidentalmente una lista o cadena en *mi_suma*, lo cual no es correcto. Si intenta ingresar un argumento de entrada de tipo no numérico en *mi_suma*, Python continuará ejecutando la función hasta que algo salga mal.

Errores comunes.

d = mi_suma('1', 2, 3)
d

d = mi_suma(1, 2, [2, 3])
d

**TIP!!** Recordemos hacer una lectura compresiva de los errores que te da Python. Por lo general, dicen exactamente dónde esta el problema. En este caso, el error dice *---> 11 out = a + b + c*, lo que significa es que hubo un error en mi_suma en la undécima línea. La razón por la que hubo un error es **TypeError**, porque *unsupported operand type(s) for +: 'int' and 'list'*, lo que significa que no pudimos agregar int y list.

En este punto, no tiene ningún control sobre lo que el usuario asigna a su función como argumentos de entrada y si corresponden a lo que pretendía que fueran esos argumentos de entrada. Entonces, por el momento, escriba sus funciones asumiendo que se usarán correctamente.

Puede componer funciones asignando llamadas de función como entrada a otras funciones. En el orden de las operaciones, Python ejecutará primero la llamada a la función más interna. También puede asignar expresiones matemáticas como entrada a funciones. En este caso, Python ejecutará primero las expresiones matemáticas.

Ejemplo de uso de funciones matematicas dentro de su función

import numpy as np

d = mi_suma(np.sin(np.pi), np.cos(np.pi), np.tan(np.pi))
d

d = mi_suma(5 + 2, 3 * 4, 12 / 6)
print(d)

#Comparamos con la notación previa
d = (5 + 2) + 3 * 4 + 12 / 6
d

Una función que no es necesario argumento:

def print_hola():
    print('Hola')

print_hola()

**TIP** Incluso si no hay un argumento de entrada, cuando llamamos a una función, todavía necesita los paréntesis.

Para la entrada del argumento, también podemos tener el valor predeterminado. Veamos el siguiente ejemplo.

**EJEMPLO:** Ejecutamos la siguiente función con y sin una entrada.

def print_saludos(day = 'Martes', name = 'Lucas'):
    print(f'Saludos, {name}, hoy es {day}')

print_saludos()

print_saludos(name = 'Martin', day = 'Miercoles')

print_saludos(name = 'Juan')