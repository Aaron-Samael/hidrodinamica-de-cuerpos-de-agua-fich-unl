# TEMA IV. Aplicaciones

---

### [Tema IV. Aplicaciones](notebooks/HCA04.00-aplicaciones.ipynb)
* [**IV.1 Streamline, streakline y pathline**](HCA04.01-streamline-streakline-pathline.ipynb)
* [**IV.2 Resolutor de ecuación de Navier-Stokes**](HCA04.02-ecuacion-de-navier-stokes.ipynb)
* [**IV.3 Resistencia hidráulica**](HCA04.03-resistencia-hidraulica.ipynb)
* [**IV.4 Flujo en canales. TELEMAC2D**](HCA04.04-telemac2d.ipynb)
* [**IV.5 Casos de estudio**](HCA04.05-casos-de-estudio.ipynb)
* [***IV.5.1 Obstrucción de pila y espigones***](HCA04.05-casos-de-estudio.ipynb)
* [***IV.5.2 Flujo en curvas***](HCA04.06-flujo-en-curvas.ipynb)
* [***IV.5.3 Flujo en condición de desborde***](HCA04.07-condición-de-desborde.ipynb)


--- 

## Motivación
  

---

Ahora se viene la mejor parte de la materia.

