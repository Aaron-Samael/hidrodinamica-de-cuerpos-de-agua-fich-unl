# Calculamos con Python

La primera etapa es conocer los alcances del uso de Python y en especial los que involucra al curso. A continuación introduciremos algunas demostraciones respecto a operaciones aritmeticas como adición, resta, multiplicación, división y potencia. Un operador **arithmetic operator** es un simbolo que Python reserva para las operaciones indicadas anteriormente. Los simbolos en Python usados son + para la suma, - para la resta, * para multiplicación, / para division, y \** para potencia.

Hagamos un par de ejemplos de dichas operaciones en Jupyter-Lab. Ejecutemos en en Anaconda Navigator Jupyter-Lab.

**Intentemos sumar, restar y multiplicar. 

5+6

10-9

15*2

Un **orden de operaciones** es un orden de precedencia estándar que tienen diferentes operaciones en relación entre sí. Python utiliza el mismo orden de operaciones que aprendiste en la escuela primaria. Los poderes se ejecutan antes de la multiplicación y la división, que se ejecutan antes de la suma y la resta. Los paréntesis, ( ), también se pueden usar en Python para reemplazar el orden estándar de operaciones.


**Intentamos computar** $\frac{3*4}{(2^2+8/2)}$.

(3*4)/(2**2 + 8/2)

**TIP!** Se puede usar el símbolo _ para representar este resultado para dividir expresiones complicadas en comandos más simples.

Python puede manipular las funciones aritmeticas como `sin`, `cos`, `tan`, `asin`, `acos`, `atan`, `exp`, `log`, `log10` y `sqrt`, las cuales pueden ser ejecutadas con la libreria **math**. Importemos la libreria para poder acceder a las funciones.

import math

**TIP!** En el Jupyter notebook y en Ipython, puede tener una vista rápida de lo que hay en el módulo escribiendo el `nombre del módulo + punto + TAB`. Además, si escribe las primeras letras de la función y presiona TAB, completa automáticamente la función.

La forma en que usamos estas funciones matemáticas es `modulo.function`, los argumentos se colocan entre paréntesis. Para funciones trigonométricas, es útil tener disponible el valor de $\ pi$. Se puede para ello llamar en cualquier momento escribiendo `math.pi` en la celda de código.

math.sqrt(9)

**Intentemos computar**  $cos(\frac{\pi}{5})$.

math.cos(math.pi/5)

Python compondrá funciones como es de esperar, con la función más interna ejecutándose primero. Lo mismo ocurre con las llamadas a funciones que se componen con operaciones aritméticas.

Tenga en cuenta que la función `log` en Python es $ log_e $, o el logaritmo natural. No es $ log_ {10} $. Si desea usar $ log_ {10} $, debe usar `math.log10`.

math.exp(math.log(10))

Muchas veces, cuando usamos una función en Python, queremos saber que hace. En el cuaderno Ipython o Jupyter, podemos solicitar esa información (de manera resumida) escribiendo `function?`, Es decir, el signo de interrogación es un atajo para obtener ayuda.

math.factorial?

Cuantdo tengamos una expresion dividida por cero, cuyo resultado es infinito. Python generará un `ZeroDivisionError`.

1/0

Puede escribir `math.inf` en el símbolo del sistema para indicar infinito o` math.nan` para indicar algo que no es un número que desea que se maneje como un número. Si esto es confuso, esta distinción se puede omitir por ahora; se explicará con más claridad cuando sea importante.

 10/math.inf

math.inf * 8

math.inf/math.inf

 Finalmente, Python también puede manejar el número imaginario.

2+ 8j

complex(2,8)

Python también permite manejar la notación científica usando la letra e entre dos números. Por ejemplo, $ 1e6 = 1000000 $ y $ 1e-3 = 0.001 $.

3e0*3.65e2*2.4e1*3.6e3

** TIP! ** Cada vez que escribimos la función en el módulo *math*, siempre escribimos `math.function_name`. Alternativamente, hay una forma más sencilla, por ejemplo, si queremos usar `sin` y` log` del módulo `math`, podríamos importarlos de esta manera:` from math import sin, log`. Entonces todo lo que necesita hacer al usar estas funciones es usarlas directamente, por ejemplo, `sin (10)` o `log (10)`.

from math import sin

sin(10)

## Libreria Numpy 

Numpy es una libreria que permite manipular arreglos multidimension. Es hora de manejra matrices y adisionamente permite trabajr con la semantica de matrices. Es la posta para el procesamiento numérico.


[REFERENCIA](https://numpy.org/doc/stable/user/quickstart.html)

#importamos la librería
import numpy as np

### El Array multidimensional 
----

- Es un arreglo (*array*)
- Indexados por enteros (lo mismo que antes)

**Ejemplos**

- Vectores, Matrices, Imágenes o Planillas

Como tipo de dato se llama `ndarray`

- `ndarray.ndim`: cantidad de dimensiones
- `ndarray.shape`: una tupla indicando el tamaño del array.
- `ndarray.size`: la cantidad de elementos en el array.
- `ndarray.dtype`: el tipo de elemento que el array contiene.

#ejemplo de aplicación

a = np.arange(16).reshape(4,4)
a

#imprimimos la forma del arreglo (fila, columna)
a.shape

#imprimimos las dimensiones de arreglo
a.ndim

#imprimimos el tamaño
a.size

#Distintas formas de crear una arreglo
np.array([2,3,4,8,12], dtype=np.float64)

np.arange(5)

np.zeros((4, 4))

np.zeros((4, 3), dtype=bool)

np.ones((3, 2), dtype=bool)

np.linspace(-np.pi, np.pi, 5)

### Manejando Arrays

a = np.arange(6)
a

a.shape = (3, 2)
a

a = np.arange(20, 60, 10)
a

b = a + 1
b

a * 2

a += 6
a

a-b

a * b

Hay muchas funciones que nos dan info del array. Son muy valiosas para el desarrollo de los códigos

```all, alltrue, any, argmax, argmin, argsort, average, bincount, ceil, clip,conj, conjugate, corrcoef, cov, cross, cumprod, cumsum, diff, dot, floor,inner, inv, lexsort, max, maximum, mean, median, min, minimum,nonzero, outer, prod, re, round, sometrue, sort, std, sum, trace,transpose, var, vdot, vectorize, where...```

a.mean(), a.sum(axis=0)

#Mas funciones para manipular arrays
a.transpose()

a = np.arange(8).reshape((2,4))
a

#convertimos el array en un vector fila
a.flatten() 

a = np.array([2, 3, -5, 0, 2])
np.argsort(a)

np.argsort?

### Ajuste de polinomios

p = np.poly1d([2, 3, 4])
print(p)

print(p * p)

A continuación vamos a genera una muestra de datos y aplicaremos los ajustes que hace la librería numpy para esta serie de datos para diferentes ordenes 

%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt

# Tus datos
x = np.array([0,0,0,1,1,1.5,2,2,2,3,3,5,5,5])
y = np.array([3,4,5,3,5,4,2,3,5,3,4,1,2,3])

# Calcular ajustes para diferentes grados. Hata grado 4
sols = {}
for grado in range(1,4):
    z = np.polyfit(x, y, grado, full=True)
    sols[grado] = z

# Ploteamos datos
plt.plot(x, y, 'o')

# Ploteamos curvas de ajuste
xp = np.linspace(0, 5.2, 100)
for grado, sol in sols.items():
    coefs, error, *_ = sol
    p = np.poly1d(coefs)
    plt.plot(xp, p(xp), "-", label="Gr: %s. Error %.3f" % (grado, error) )
    plt.legend()

