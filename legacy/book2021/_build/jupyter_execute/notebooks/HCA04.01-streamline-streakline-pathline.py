# Pathline, Streakline y Streamline

La partícula roja se mueve en un fluido que fluye; su pathline se traza en rojo; la punta del rastro de tinta azul liberada desde el origen sigue a la partícula, pero a diferencia de la línea de pathline estática (que registra el movimiento anterior del punto), la tinta liberada después de que el punto rojo se aleja continúa moviéndose hacia arriba con el flujo. (Esta es una streakline). Las líneas de trazos representan los contornos del campo de velocidad (streamline), mostrando el movimiento de todo el campo al mismo tiempo.

![giphy](images/Streaklines_and_pathlines_animation.gif)
https://en.wikipedia.org/wiki/Streamlines,_streaklines,_and_pathlines

Una $streamline$ es una curva o familia en todas partes tangente al vector de velocidad local en un instante dado. Líneas instantáneas muestran la dirección en la que viajará un elemento fluido sin masa en cualquier momento.

Por definición, debemos tener $Vx dr=0$ lo que al expandirse produce la ecuación de streamline para un tiempo determinado $t=t_1$

$$\frac{dx}{u}=\frac{dy}{v}=\frac{dz}{w}=ds$$   

donde $s$: es el parametro de integración

Asi si $(u,v,w)$ son conocidas, debemos integrar con respecto a **s** para $t=t_1$ con la condición inicial $(x_0,y_0,z_0,t_0)$ sobre $s=0$ y de esa manera luego se eleimina $s$

Una $pathline$ es la trayectoria real recorrida por una partícula de fluido individual durante un período de tiempo. Generado como el paso del tiempo; convinent para generar experimentalmente.

La pathline se define mediante la integración de la relación entre la velocidad y el desplazamiento.

$$\frac{dx}{dt}=u;\frac{dy}{dt}=v; \frac{dz}{dt}=w$$

Integrando $u,v,w$ con respecto a $t$ usando la condición inicial $(x_0,y_0,z_0,t_0)$ y luego elimanamos $t$

Un $Streakline$ es el lugar geométrico de las partículas que han pasado anteriormente por un punto prescrito. El tinte inyectado de manera constante en el fluido en un punto fijo se extiende a lo largo de una línea de rayas

Para encontrar la línea de trayectoria, utilice el resultado integrado para el tiempo de retención de la línea de trayectoria como parámetro. Ahora, encontramos la constante de integración que hace que la pathline pase por $(x_0,y_0,z_0)$ por una secuencia de tiempos $\tau<t$. Entonces eliminamos $\tau$.

Las $Timeline$ son las líneas formadas por un conjunto de partículas fluidas que se marcaron en un instante anterior en el tiempo, creando una línea o una curva que se desplaza en el tiempo a medida que las partículas se mueven.

![IPython](images/timeline.png)

Notar que streamline, pathline y streakline son identicas en flujo uniforme
Para flujo no uniforme, el patrón de la streamline cambia con el tiempo, mientras pathlines y streaklines son generadas en cada paso de tiempo

# Ejercicio

Hallar la streamline, pathline y streakline de la siguiente distribución de velocidad:

$$ u=\frac{x}{1+t}  ; v=\frac{y}{1+2t}  ; w=0$$

### Dibujamos las funciones encontradas anteriormente
### Streamline


$$y=y_0\left(\frac{x}{x_0}\right)^n$$
con $$n=\frac{1+t}{1+2t}$$

### Pathline

$$y=y_0\left[1+2\left(\frac{x}{x_0}-1\right)\right]^{1/2}$$

### Streakline

Para $t=0$ 
$$\frac{y}{y_0}=\left[1+2\left(\frac{x_0}{x}-1\right)\right]^{-1/2}$$

import numpy as np
import matplotlib.pyplot as plt

## Ploteamos
x0 = 1
y0 = 1
t  = 0

x = np.linspace(0,10,20)

n = 1+t/(1+2*t) 
y1 = np.zeros((len(x),1))
m = 0
for i in x:
    y1[m] = y0*(i/x0)**n
    m+=1
    
plt.plot(np.transpose(x), y1, 'or')
#plt.show()
plt.xlabel('X')
plt.ylabel('Y')

#PATHline
import numpy as np
import matplotlib.pyplot as plt
del y
## Ploteamos
x0 = 1
y0 = 1
t  = 0

x = np.linspace(0,10,20)

y2 = np.zeros((len(x),1))
m = 0
for i in x:
    y2[m] = y0*(1+2*((i/x0)-1))**(1/2)
    m+=1

plt.plot(np.transpose(x), y2, 'ok')
#plt.show()
plt.xlabel('X')
plt.ylabel('Y')

#STREAKLINE
import numpy as np
import matplotlib.pyplot as plt

## Ploteamos
x0 = 1
y0 = 1
t  = 0

x = np.linspace(0,10,20)

y3 = np.zeros((len(x),1))
m = 0
for i in x:
    y3[m] = y0*(1+2*(i/x0-1))**(-0.5)
    m+=1
    
plt.plot(np.transpose(x), y3, 'ob')
#plt.show()
plt.xlabel('X')
plt.ylabel('Y')

plt.plot(np.transpose(x), y1, 'or', label ='streamline')
plt.plot(np.transpose(x), y2, 'ok', label ='pathline')
plt.plot(np.transpose(x), y3, 'ob', label ='streakline')
plt.xlabel('X')
plt.ylabel('Y')
plt.legend(loc = 2)

![IPython](images/stream.png)

