# Problema de Sod en un conducto

# Required modules
import numpy as np
import matplotlib.pyplot as plt

Completemos la definición del vector de flujo para las ecuaciones de Euler usando las variables conservadas ``u``. 

def euler_flux(u):
    f = 0.0*u
    p = (gamma - 1)*(u[2] - 0.5*u[1]**2/u[0])
    f[0] = u[0]
    f[1] = 
    f[2] = 
    return f

**La solución de Riemann**

Primero, completaremos la definición del resolutor de Roe Riemann siguiendo la teoría del libro de texto.

def roe_scheme(ul, ur):
    # Get density
    rhol = ul[0]
    rhor = ur[0]

    # Compute velocity
    vl = ul[1]/ul[0]
    vr = ur[1]/ur[0]
    
    # Compute pressure
    pl = (gamma - 1)*rhol*(ul[2]/ul[0] - 0.5*vl**2)
    pr = (gamma - 1)*rhor*(ur[2]/ur[0] - 0.5*vr**2)

    # Compute enthalpy
    hl = 
    hr = 
    
    # Compute Roe averages
    vm = 
    hm = 
    cm = 
    
    # Compute averaged eigenvalues
    l1 = vm - cm 
    l2 = vm
    l3 = vm + cm
    
    # Compute right averaged eigenvectors
    k = np.array([[        1,          1,          1],
                  [   vm - cm,        vm,    vm + cm],
                  [hm - vm*cm, 0.5*vm**2, hm + vm*cm]])
    
    # Compute wave strengths (alpha)
    du = ur - ul
    a2 = (gamma - 1)/cm**2 *(du[0]*(hm - vm**2) + vm*du[1] - du[2])
    a1 = 
    a3 = 
 
    # Check for rarefaction waves (Harten's entropy fix)
    tol = 0.001
    
    # Left wave
    if abs(l1) < tol:
        l1 = (l1**2 + tol**2)/(2*tol)
    
    # Right wave
    if abs(l3) < tol:
        l2 = (l2**2 + tol**2)/(2*tol)
        
    # Compute left and right Euler fluxes
    fl = euler_flux(ul)
    fr = euler_flux(ur)
    
    # Compute Riemann flux
    fm = 0.5*(fl + fr - (a1*abs(l1)*k[:, 0] + a2*abs(l2)*k[:, 1] + a3*abs(l3)*k[:, 2]))
    return fm

Para avanzar la solución en el tiempo, usaremos el método del punto medio, que se da a continuación.

def advance_solution(ut, dt, tf):
    t = 0
    while t < tf:
        r = residual(ut)
        um = ut + 0.5*dt*r
        rm = residual(um)
        uf = ut + 1.0*dt*rm
        ut = uf
        t += dt 
    return ut.T

Ahora, definimos la función ``residual`` siguiendo el esquema de Godunov

def residual(u):
    r = 0.0*u
    for i in range(1, n - 1):
        fl = roe_scheme(u[i - 1], u[i])
        fr = roe_scheme(u[i], u[i + 1])
        r[i] = -(fr - fl)/dx
    return r

Definimos la constante global ``gamma=1.4``

gamma = 1.4

Para inicializar la solución, será útil definir una función ``initialize_sod``. Completa las líneas que faltan 

def initialize_sod(x, rhol, vl, pl, rhor, vr, pr):
    rho = 0.0*x
    v = 0.0*x
    p = 0.0*x

    # Initialize discontinuous solution on grid
    for j, xval in enumerate(x):
        if xval <= L/2:
            rho[j] = 
            v[j] = 
            p[j] = 
        else:
            rho[j] = 
            v[j] = 
            p[j] = 
    
    # Compute specific energy
    e = p/(gamma*rho-rho)
    E = 

    # Initialize vector of conserved variables
    u0 = np.array([rho, rho*v, rho*E])
    return u0.T

DAtos de la grilla
- ``n=50``
- ``L=1.0``

n = 50
L = 1

dx = L/n
x = np.linspace(0, L, n)

Condiciones de borde de izquierda y derecha:

\begin{align}
	\vec u_L = 
	\begin{bmatrix}
		\rho_L \\ v_L \\ p_L 
	\end{bmatrix}
	= 
	\begin{bmatrix}
		1 \\ 0 \\ 1
	\end{bmatrix},
	\quad \text{and} \quad 
	\vec u_R = 
	\begin{bmatrix}
		\rho_R \\ v_R \\ p_R
	\end{bmatrix}
	= 
	\begin{bmatrix}
		\frac{1}{8} \\ 0 \\ \frac{1}{10}
	\end{bmatrix}.
\end{align}


# Left state
rhol = 1
vl = 0
pl = 1

# Right state
rhor = 1/8
vr = 0
pr = 1/10

u0 = initialize_sod(x, rhol, vl, pl, rhor, vr, pr)

 Usando un tamaño de intervalo de tiempo $ \Delta t = 0.002 $, avance la solución hasta $ t = 0.2 $.

dt = 0.002
tf = 0.2

u_sod = advance_solution(u0, dt, tf)

Ploteamos el resultado del esquema implementado y compáramos con la solución exacta. Ejecute la siguiente celda para definir la función de ploteo y generar el primer diagrama.

def plot_sod(u, title):
    fig, ax = plt.subplots(figsize=(8, 2.5), ncols=3, sharex=True, sharey=True)

    # Read file in notebooks/data directory
    ue = np.loadtxt('data/sod-shock-tube.txt')

    # Extract rho, v and p from file
    x_e = ue[:, 0]
    rho_e = ue[:, 1]
    v_e = ue[:, 2]
    p_e = ue[:, 3]

    # Plot exact solution
    ax[0].plot(x_e, rho_e, 'k', lw=1)
    ax[1].plot(x_e, v_e, 'k', lw=1)
    ax[2].plot(x_e, p_e, 'k', lw=1)

    # Compute density, velocity and specific energy
    rho = u_sod[0]
    v = u_sod[1]/rho
    E = u_sod[2]/rho

    # Compute pressure using rho, v, E
    p = (gamma - 1)*rho*(E - 0.5*v**2)

    # Plot numerical results
    ax[0].plot(x, rho, 'o', markersize=1.5, color='#bd0c00')
    ax[1].plot(x, v, 'o', markersize=1.5, color='#bd0c00')
    ax[2].plot(x, p, 'o', markersize=1.5, color='#bd0c00')

    ax[0].set_ylabel(r'$\rho$')
    ax[1].set_ylabel(r'$v$')
    ax[2].set_ylabel(r'$p$')

    ax[0].set_xlabel(r'$x$')
    ax[1].set_xlabel(r'$x$')
    ax[2].set_xlabel(r'$x$')
    
    fig.suptitle(title)
    
plot_sod(u_sod, 'Roe Scheme')

Aumente el número de celdas a 100 y vuelva a ejecutar las celdas anteriores. ¿La solución ha mejorado significativamente?

** Implementación de MUSCL **

Comparemos ahora los resultados anteriores con esquemas reconstruidos de MUSCL de orden superior.

Primero, reescribiremos la función ``residual`` usando la estructura de los esquemas MUSCL. Completa la función.

*(Consulte el cuaderno Esquemas MUSCL para obtener una introducción a estos métodos y su implementación en la advección lineal)*.

**Notemos**, consideraremos las condiciones de contorno de Dirichlet y, por lo tanto, ignoraremos el primer y los dos últimos nodos de la cuadrícula. Consulte la implementación del ciclo ``for``.

def residual(u):
    res = 0.0*u
    b = 0
    for i in range(2, n - 2):
        # Interface i+1/2
        dl = get_slope(u[i - 1], u[i], u[i + 1], b)
        dr = 

        # Compute u_i+1/2,L and u_i+1/2,R
        ul = 
        ur = 

        # Compute f_i+1/2 using the upwind Riemann solver
        fR = roe_scheme(ul, ur)
        
        # Interface i-i/2
        dl = get_slope(u[i - 2], u[i - 1], u[i], b)
        dr = 
        
        # Compute u_i-1/2,L and u_i-1/2,R
        ul = 
        ur = 
        
        # Computr f_i-1/2 using the upwind Rieman solver
        fL = roe_scheme(ul, ur)
        res[i] = -(fR - fL)/dx
    return res

Complete la función ``get_slope`` a continuación con limitadores siguiendo la teoría en el libro de texto, luego ejecute la celda. Para cualesquiera tres valores de ``u``, ``u0``, ``u1`` and ``u2`` se ordenan de izquierda a derecha. Por ejemplo, $ u_ {i-2}, u_ {i-1}, u_i $.

def get_slope(u0, u1, u2, b):
    r = 
    phi = limiter(r)
    delta = 
    return delta

Las funciones del limitador se definirán a continuación. Siguiendo el ejemplo del limitador minmod, complete la definición de ``van_leer`` y ``superbee``.

def minmod(r):
    l = np.zeros(3)
    for i in range(3):
        l[i] = max([0, min([1, r[i]])])
    return l

def superbee(r):
    pass

def van_leer(r):
    pass

Considerando las mismas condiciones iniciales definidas previamente, iniciamos el problema usando `` initialize_sod``.

# Left state
rhol = 
vl = 
pl = 

# Right state
rhor =
vr = 
pr = 

u0 = initialize_sod(x, rhol, vl, pl, rhor, vr, pr)

Ahora generaremos un gráfico para cada uno de los limitadores implementados.

limiter = minmod
u_sod = advance_solution(u0, dt, tf)
plot_sod(u_sod, 'MUSCL Scheme - minmod limiter')

limiter = van_leer
u_sod = advance_solution(u0, dt, tf)
plot_sod(u_sod, 'MUSCL Scheme - van Leer limiter')

limiter = superbee
u_sod = advance_solution(u0, dt, tf)
plot_sod(u_sod, 'MUSCL Scheme - superbee limiter')