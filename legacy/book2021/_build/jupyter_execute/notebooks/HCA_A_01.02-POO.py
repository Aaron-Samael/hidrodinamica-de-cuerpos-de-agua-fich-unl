# Introducción a Programación Orientada a Objetos

So far, all the codes we have written belong to the category of **procedure-oriented programming (POP)**, which consists of a list of instructions to tell the computer what to do; these instructions are then organized into functions. The program is divided into a collection of variables, data structures, and routines to accomplish different tasks. Python is a multi-paradigm programming language, which means it supports different programming approach. One different way to program in Python is **object-oriented programming (OOP)**. The learning curve is steeper, but it is extremely very powerful and worth the time invested in mastering it. Note: you do not have to use OOP when programming in Python. You can still write very powerful programs using the POP. That said, the POP is good for simple and small programs, while the OOP is better suited for large programs. Let us take a closer look at object-oriented programming.

The object-oriented programming breaks the programming task into **objects**, which combine data (known as attributes) and behaviors/functions (known as methods). Therefore, there are two main components of the OOP: **class** and **object**.

The class is a blueprint to define a logical grouping of data and functions. It provides a way to create data structures that model real-world entities. For example, we can create a `people` class that contains the data such as name, age, and some behavior functions to print out ages and genders of a group of people. While class is the blueprint, an object is an `instance` of the class with actual values. For example, a person named 'Iron man' with age 35. Put it another way, a class is like a template to define the needed information, and an object is one specific copy that filled in the template. Also, objects instantiated from the same class are **independent** from each other. For example, if we have another person - 'Batman' with age 33, it can be instantiated from the `people` class, but it is an independent instance. 

Let us implement the above example in Python. Do not worry if you do not understand the syntax below; the next section provide more helpful examples.

class People():
    def __init__(self, name, age):
        self.name = name
        self.age = age
        
    def greet(self):
        print("Greetings, " + self.name)

person1 = People(name = 'Iron Man', age = 35)
person1.greet()
print(person1.name)
print(person1.age)

person2 = People(name = 'Batman', age = 33)
person2.greet()
print(person2.name)
print(person2.age)

In the above code example, we first defined a class - People, with *name* and *age* as the data, and a method *greet*. We then initialized an object - `person1` with the specific name and age. We can clearly see that the *class* defines the whole structure, while the object is just an `instance` of the `class`. Later, we instantiated another object, `person2`. It is clear that `person1` and `person2` are independent with each other, though they are all instantiated from the same class. 

The concept of OOP is to create reusable code. There are three key principles of using OOP: 

* **Inheritance** - a way of creating new classes from existing class without modifying it.
* **Encapsulation** - a way of hiding some of the private details of a class from other objects. 
* **Polymorphism** - a way of using common operation in different ways for different data input. 

With the above principles, there are many benefits of using OOP: It provides a clear modular structure for programs that enhances code re-usability. It provides a simple way to solve complex problems. It helps define more abstract data types to model real-world scenarios. It hides implementation details, leaving a clearly defined
interface. It combines data and operations.

There are also other advantages to use OOP in a large project. We encourage you can search online to find out more. At this point, you may still not fully understand OOP's advantages until you are involved in complex large projects. We will continue to learn more about OOP during the course of this book, and its usefulness will become apparent.

## Class and Object

The previous section introduced the two main components of OOP: **Class**, which is a blueprint used to define a logical grouping of data and functions, and **Object**, which is an instance of the defined class with actual values. In this section, we will get into greater detail of both of these components.

## Class

A **class** is a definition of the structure that we want. Similar to a function, it is defined as a block of code, starting with the class statement. The syntax of defining a class is:

```python
class ClassName(superclass):
    
    def __init__(self, arguments):
        # define or assign object attributes
        
    def other_methods(self, arguments):
        # body of the method

```

Note: the definition of a class is very similar to a function. It needs to be instantiated first before you can use it. For the class name, it is standard convention to use "CapWords." The **superclass** is used when you want create a new class to **inherit** the attributes and methods from another already defined class. We will talk more about **inheritance** in the next section. The **\_\_init\_\_** is one of the special methods in Python classes that is run as soon as an object of a class is instantiated (created). It assigns initial values to the object before it is ready to be used. Note the two underscores at the beginning and end of the
`init`, indicating this is a special method reserved for special use in the language.
In this `init` method, you can assign attributes directly when you create the object. The `other_methods`
functions are used to define the instance methods that will be applied on the attributes, just like functions we discussed before. You may notice that there is a parameter `self` for defining this method in the class. Why? A class instance method must have this extra argument as the first argument when you define it. This particular argument refers to the object itself; conventionally, we use `self` to name it. Through this self parameter, instance methods can freely access attributes and other methods in the same object. When we define or call an instance method within a class, we need to use this self parameter. Let us see an example below.

**EXAMPLE:** Define a class named `Student`, with the attributes `sid` (student id), `name`, `gender`, `type` in the `init` method and a method called `say_name` to print out the student's name. All the attributes will be passed in except `type`, which will have a value as 'learning'. 

class Student():
    
    def __init__(self, sid, name, gender):
        self.sid = sid
        self.name = name
        self.gender = gender
        self.type = 'learning'
        
    def say_name(self):
        print("My name is " + self.name)

From the above example, we can see this simple class contains all the necessary parts mentioned previously. The `__init__` method will initialize the attributes when we create an object. We need to pass in the initial value for `sid`, `name`, and `gender`, while the attribute `type` is a fixed value as "learning".

These attributes can be accessed by all the other methods defined in the class with `self.attribute`, for example, in the `say_name` method, we can use the `name` attribute with `self.name`. The methods defined in the class can be accessed and used in other different methods as well using `self.method`. Let us see the following example. 

**TRY IT!** Add a method **report** that print not only the student name, but also the student id. The method will have another argument *score*, that will pass in a number between 0 - 100 as part of the report. 

class Student():
    
    def __init__(self, sid, name, gender):
        self.sid = sid
        self.name = name
        self.gender = gender
        self.type = 'learning'
        
    def say_name(self):
        print("My name is " + self.name)
        
    def report(self, score):
        self.say_name()
        print("My id is: " + self.sid)
        print("My score is: " + str(score))

## Object

As mentioned before, an **object** is an instance of the defined class with actual values. We can have many instances of different values associated with the class, and each of these instances will be independent with each other as we saw previously. Also, after we create an object, and call this instance method from the object, we do not need to give value to the `self` parameter since Python automatically provides it. See the following example:

**EXAMPLE:** Create two objects ("001", "Susan", "F") and ("002", "Mike", "M"), and call the method *say_name*. 

student1 = Student("001", "Susan", "F")
student2 = Student("002", "Mike", "M")

student1.say_name()
student2.say_name()
print(student1.type)
print(student1.gender)

In the above code, we created two objects, `student1` and `student2`, with two different sets of values. Each object is an instance of the `Student` class and has a different set of attributes. Type `student1.+TAB` to see the defined attributes and methods. To get access to one attribute, type `object.attribute`, e.g., `student1.type`. In contrast, to call a method, you need the parentheses because you are calling a function, such as `student1.say_name()`.


**TRY IT!** Call method *report* for student1 and student2 with score 95 and 90 individually. Note: we do not need the "self" as an argument here. 

student1.report(95)
student2.report(90)

We can see both methods calling to print out the data associated with the two objects. Note: the score value we passed in is only available to the method `report` (within the scope of this method). We can also see that the method `say_name` call in the `report` also works, as long as you call the method with the `self` in it.

## Class vs instance attributes

The attributes we presented above are actually called instance attributes, which means that they are only belong to a specific instance; when you use them, you need to use the `self.attribute` within the class. There are another type of attributes called class attributes, which will be shared with all the instances created from this class. Let us see an example how to define and use a class attribute.

**EXAMPLE:** Modify the `Student` class to add a class attribute `n`, which will record how many object we are creating. Also, add a method `num_instances` to print out the number.

class Student():
    
    n_instances = 0
    
    def __init__(self, sid, name, gender):
        self.sid = sid
        self.name = name
        self.gender = gender
        self.type = 'learning'
        Student.n_instances += 1
        
    def say_name(self):
        print("My name is " + self.name)
        
    def report(self, score):
        self.say_name()
        print("My id is: " + self.sid)
        print("My score is: " + str(score))
        
    def num_instances(self):
        print(f'We have {Student.n_instances}-instance in total')

In defining a class attribute, we must define it outside of all the other methods `without` using `self`. To use the class attributes, we use `ClassName.attribute`, which in this case is `Student.n`. This attribute will be shared with all the instances that are created from this class. Let us see the following code to show the idea.

student1 = Student("001", "Susan", "F")
student1.num_instances()
student2 = Student("002", "Mike", "M")
student1.num_instances()
student2.num_instances()

As before, we created two objects, the instance attribute `sid`, `name`, but `gender` only belongs to the specific object. For example, `student1.name` is "Susan" and `student2.name` is "Mike". But when we print out the class attribute `Student.n_instances` after we created object `student2`, the one in the `student1` changes as well. This is the expectation we have for the class attribute because it is shared across all the
created objects.

Now that we understand the difference between class and instance, we are in good shape to use basic OOP in Python. Before we can take full advantage of OOP, we still need to understand the concept of
inheritance, encapsulation and polymorphism. Let us start next section!


# Inheritance, Encapsulation and Polymorphism

We have already seen the modeling power of OOP using the class and object functions by combining data and methods. There are three more important concept, **inheritance**, which makes the OOP code more modular, 
easier to reuse and build a relationship between classes. **Encapsulation** can hide some of the private details of a class from other objects, while **polymorphism** can allow us to use a common operation in different ways. In this section, we will briefly discuss them. 

## Inheritance

Inheritance allows us to define a class that inherits all the methods and attributes from another class. Convention denotes the new class as **child class**, and the one that it inherits from is called **parent class** or **superclass**. If we refer back to the definition of class structure, we can see the structure for basic inheritance is **class ClassName(superclass)**, which means the new class can access all the attributes and methods from the superclass. Inheritance builds a relationship between the child class and parent class, usually in a way that the parent class is a general type while the child class is a specific type. Let us try to see an example.

**TRY IT!** Define a class named `Sensor` with attributes `name`, `location`, and `record_date` that pass from the creation of an object and an attribute `data` as an empty dictionary to store data. Create one method *add_data* with `t` and `data` as input parameters to take in timestamp and data arrays. Within this method, assign `t` and `data` to the `data` attribute with 'time' and 'data' as the keys. In addition, it should have one `clear_data` method to delete the data. 

class Sensor():
    def __init__(self, name, location, record_date):
        self.name = name
        self.location = location
        self.record_date = record_date
        self.data = {}
        
    def add_data(self, t, data):
        self.data['time'] = t
        self.data['data'] = data
        print(f'We have {len(data)} points saved')        
        
    def clear_data(self):
        self.data = {}
        print('Data cleared!')

Now we have a class to store general sensor information, we can create a sensor object to store some data. 

**EXAMPLE:** Create a sensor object. 

import numpy as np

sensor1 = Sensor('sensor1', 'Berkeley', '2019-01-01')
data = np.random.randint(-10, 10, 10)
sensor1.add_data(np.arange(10), data)
sensor1.data

### Inherit and extend new method

Say we have one different type of sensor: an accelerometer. It shares the same attributes and methods as `Sensor` class, but it also has different attributes or methods need to be appended or modified from the original class. What should we do? Do we create a different class from scratch? This is where inheritance can be used to make life easier. This new class will inherit from the `Sensor` class with all the attributes and
methods. We can whether we want to extend the attributes or methods. Let us first create this new class, `Accelerometer`, and add a new method, `show_type`, to report what kind of sensor it is.

class Accelerometer(Sensor):
    
    def show_type(self):
        print('I am an accelerometer!')
        
acc = Accelerometer('acc1', 'Oakland', '2019-02-01')
acc.show_type()
data = np.random.randint(-10, 10, 10)
acc.add_data(np.arange(10), data)
acc.data

Creating this new `Accelerometer` class is very simple. We inherit from `Sensor` (denoted as a superclass), and the new class actually contains all the attributes and methods from the superclass. We then add a new method, `show_type`, which does not exist in the `Sensor` class, but we can successfully extend the child
class by adding the new method. This shows the power of inheritance: we have reused most part of the `Sensor` class in a new class, and extended the functionality. Besides, the inheritance sets up a logical relationship for the modeling of the real-world entities : the `Sensor` class as the parent class is more general and passes all the characteristics to the child class `Accelerometer`.

### Inherit and method overriding

When we inherit from a parent class, we can change the implementation of a method provided by the parent class, this is called method overriding. Let us see the following example. 

**EXAMPLE:** Create a class `UCBAcc` (a specific type of accelerometer that created at UC Berkeley) that inherits from `Accelerometer` but replace the `show_type` method that prints out the name of the sensor. 

class UCBAcc(Accelerometer):
    
    def show_type(self):
        print(f'I am {self.name}, created at UC Berkeley!')
        
acc_ucb = UCBAcc('UCBAcc', 'Berkeley', '2019-03-01')
acc_ucb.show_type()

We see that, our new `UCBAcc` class actually overrides the method `show_type` with new features. In this example, we are not only inheriting features from our parent class, but we are also modifying/improving some methods.

### Inherit and update attributes with super

Let us create a class `NewSensor` that inherits from `Sensor` class, but with updated the attributes by adding a new attribute `brand`. Of course, we can re-define the whole `__init__` method as shown below and overriding the parent function. 

class NewSensor(Sensor):
    def __init__(self, name, location, record_date, brand):
        self.name = name
        self.location = location
        self.record_date = record_date
        self.brand = brand
        self.data = {}
        
new_sensor = NewSensor('OK', 'SF', '2019-03-01', 'XYZ')
new_sensor.brand

However, there is a better way to achieve the same. We can use the `super` method to avoid referring to the parent class explicitly. Let us see how to perform this in the following example:

**EXAMPLE:** Redefine the attributes in inheritance. 

class NewSensor(Sensor):
    def __init__(self, name, location, record_date, brand):
        super().__init__(name, location, record_date)
        self.brand = brand
        
new_sensor = NewSensor('OK', 'SF', '2019-03-01', 'XYZ')
new_sensor.brand

Now we can see with the *super* method, we avoid to list all the definition of the attributes, this helps keep your code maintainable for the foreseeable future. But it really useful when you are doing multiple inheritance, which is beyond the discussion of this book. 

## Encapsulation

**Encapsulation** is one of the fundamental concepts in OOP. It describes the idea of restricting access to methods and attributes in a class. This will hide the complex details from the users, and prevent data being modified by accident. In Python, this is achieved by using private methods or attributes using underscore as prefix, i.e. single "\_" or double "\_\_". Let us see the following example.     

**EXAMPLE:**  

class Sensor():
    def __init__(self, name, location):
        self.name = name
        self._location = location
        self.__version = '1.0'
    
    # a getter function
    def get_version(self):
        print(f'The sensor version is {self.__version}')
    
    # a setter function
    def set_version(self, version):
        self.__version = version

sensor1 = Sensor('Acc', 'Berkeley')
print(sensor1.name)
print(sensor1._location)
print(sensor1.__version)

The above example shows how the encapsulation works. With single underscore, we defined a private variable, and it should not be accessed directly. But this is just convention, nothing stops you from doing that. You can still get access to it if you want to. With double underscore, we can see that the attribute `__version` can not be accessed or modify it directly. Therefore, to get access to the double underscore attributes, we need to use getter and setter function to access it internally, as shown in the following example. 

sensor1.get_version()

sensor1.set_version('2.0')
sensor1.get_version()

The single and double underscore also apply to private methods as well, we will not discuss these as they are similar to the private attributes. 

## Polymorphism

**Polymorphism** is another fundamental concept in OOP, which means multiple forms. Polymorphism allows us to use a single interface with different underlying forms such as data types or classes. For example, we can have commonly named methods across classes or child classes. We have already seen one example above, when we override the method `show_type` in the `UCBAcc`. For parent class `Accelerometer` and child class `UCBAcc`, they both have a method named `show_type`, but they have different implementation. This ability of using single name with many forms acting differently in different situations greatly reduces our complexities. We will not expand to discuss more of Polymorphism, if you are interested, check more online to get a deeper understanding. 