
# Git & GitHub

### Creditos Juan Cabral Curso Diseño de Software Cientifico (FAMAF UNC)

##¿Qué es git?

Supongamos que trabajan en el **Proyecto X**
- Crean el directorio *ProyectoX* para guardar los archivos de código, recursos, etc.
----

### Su Jefe/Director/Cliente les pide hacer cambios: ¿Qué hacen?


 - Modifican los codigos originales? (Con la esperanza de que el Jefe sabe lo que pide)

 - Crean el directorio *ProyectoX_v2*? (Con la esperanza de que no les pidan 18 cambios más adelante)

Git es un sistema de control de versiones.
 - Guardar el historial de revisiones de un proyecto en un directorio
 - Retroceder a cualquier versión del proyecto en cualquier momento
 - Implementar nuevas características sin romper el codigo principal (Branches)
 - Facilita la colaboración con varias personas. Permite que cada una de ellas tenga su propia versión de los archivos en su computadora.

Creado por Linus Torvalds en 2005 para mantener el kernel de Linux.

## Cómo funciona?

![IPython](images/comogit.png)

 - Git lleva el registro de cambios del repositorio. El repositorio es un directorio, todos sus archivos y subdirectorios.
 - Los cambios se registran con **commits**.

## Configuración incial
----
Sitio oficial: https://git-scm.com/

Probablemente ya lo tengan instalado, sino: <code>sudo apt install git</code>

git --version  # La última versión estable es 2.28. 

### Nombre y Correo

git config --global user.name "Your Name"
git config --global user.email "yourname@email.com"

git config --list    # Verificar la configuracion

## Crear repositorio
----
En el directorio del Proyecto: <code>git init</code>

mkdir ProyectoX    
cd ProyectoX
git init

Comenzamos a trabajar en el proyecto. Creamos archivos y agregamos código.
El comando <code>git status</code> nos permite ver el estado de las modificaciones.

### Agregar al Stage
-----
Se pueden agregar archivos individuales o todos al mismo tiempo con <code>git add .</code>

git add filename

### Commits
-----
Acompañar los cambios con un mensaje lo más claro posible.

git commit -m "Mensaje corto y especifico"

## Deshacer cambios
-----
Para volver atras necesitamos primero el **id** del commit al cual queremos volver.
Usamos <code>git log</code> para ver el historial de commits.

Hay **tres** formas de volver atras:

**1)** <code>git checkout **id**</code>: No cambia nada, solo lleva el HEAD a ese commit. Podemos ver los codigos como eran en ese momento. Podemos abrir un nuevo branch. Para volver el HEAD al punto actual: <code>git checkout master</code>. (O el nombre de la rama en la que estemos.)

**2)** <code>git revert **id**</code>: Inserta un nuevo commit que regresa el estado del codigo a como era en el commit especificado. No se borra nada!

## Deshacer cambios
-----

**3)** <code>git reset **id**</code>: Borra todo el historial de commits posterior al commit especificado. **Warning:** borra solo el historial de commits, no los cambios en los archivos. Todas las modificaciones siguen estando! Se puede volver a hacer un commit con todos los cambios juntos.

**3.soft)** <code>git reset **id** --soft</code>: Borra historial de commits. Los cambios vuelven al Stage.

**3.hard)** <code>git reset **id** --hard</code>: Borra historial de commits y cambios en los archivos **permanentemente**.

## Abrir Ramas
-----
Podemos abrir ramas para desarrollar nuevas características sin romper nada de la rama principal o **master**.

![IPython](images/ramagit.png)

Comandos para gestionar ramas:

<code>git branch nombre-rama</code>: Abre una rama nueva.
<br/>
<code>git checkout nombre-rama</code>: Cambia el HEAD a la rama especificada. 
<br/>
<code>git branch -a</code>: Lista las ramas existentes. El asterisco <code>*</code> indica la rama a la que apunta el HEAD.

## Merge
-----
Supongamos que queremos mergear dos ramas, **RamaA** y **master**. 

<code>git checkout master</code>: Nos ubicamaos en la rama receptora del merge.

<code>git merge RamaA</code>: Pedimos el merge de la RamaA. Trae todos los commits de la RamaA. Se puede hacer

### Conflictos
Si hay conflictos en el merge, el código aparece con el siguiente formato:

<<<<<<< HEAD
This is an edit on the master branch
=======
This is an edit on the branch
>>>>>>> RamaA

Luego de solucionar el conflicto hacen <code>add</code> y <code>commit</code>:

 - Servicio online que hospeda nuestro repositorio de codigo
 - Compartir codigo con otros desarrolladores
 - Cualquiera puede descargar el código y modificarlo
 - Cualquiera puede subir sus cambios y unirlos al código

## Crear Repositorio Remoto
-----
### Opción A. Ya tenemos un Repo Local y lo queremos en GitHub
Creamos un nuevo repo en GitHub con **el mismo nombre** que el repositorio Local. Luego:

<code>git remote add origin https://github.com/USUARIO/NOMBRE_REPO</code>
<br/>
<code>git push origin master</code>



### Opción B. Creamos primero el Repo Remoto y lo clonamos localmente.

<code>git clone https://github.com/USUARIO/NOMBRE_REPO</code>

## Sincronizar Local y Remoto
-----

Ahora podemos introducir cambios en los archivos localmente y sincronizar con el remoto:
<br/>
<code>git push origin master</code>

Si trabajan en equipo probablemente se la pasen trabajando en alguna rama:
<br/>
<code>git push origin NOMBRE_RAMA</code>

Si algun colaborador hizo cambios en el remoto, lo sincronizan con el local:
<br/>
<code>git pull origin NOMBRE_RAMA</code>