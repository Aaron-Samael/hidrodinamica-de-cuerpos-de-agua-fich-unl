# Manejo de Paquetes

Una característica que hace que Python sea realmente genial son los diversos paquetes / módulos desarrollados por la comunidad. La mayoría de las veces, cuando deseamos usar algunas funciones o algoritmos, encontraremos que quizás ya haya varios paquetes de la comunidad codificados, y todo lo que necesita hacerse es instalar los paquetes y usarlos en su código. Por lo tanto, administrar paquetes es una de las habilidades más importantes que necesitamos aprender para aprovechar al máximo Python. En esta sección, le mostraremos cómo administrar los paquetes en Python.

## Manejo de paquetes por gestores

Pip es un administrador de paquetes que automatiza el proceso de instalación, actualización y eliminación de paquetes. Podría instalar paquetes publicados en [Python Package Index (PyPI)](https://pypi.org/). Cuando instalamos el instalador de Anaconda, también instaló pip para su uso.


Usamos pip help para conocer sobre el administrador de paquetes


pip help

Pero los comandos más utilizados generalmente incluyen: install, upgrade y uninstall un paquete.

### Instalar paquetes

Para instalar ultima versión:
```bash
pip install nombre_paquete

```

Para instalar una versión específica:

```bash
pip install nombre_paquete==1.5

```

Pip instalará el paquete en la dependencia que Ud use (en nuestro caso anaconda base).

### Actualización de paquete

Para actualizar la versión del paquete.

```bash
pip install --upgrade nombre_paquete
```
o simplemente

```bash
pip install -U nombre_paquete
```

### Desistalación de paquetes

```bash
pip uninstall nombre_paquete
```

### Otros comandos utiles

Hay otros comandos útiles que se suele utilizar para obtener información sobre los paquetes instalados. Por ejemplo, si desea obtener una lista de todos los paquetes instalados, puede usar el comando:


pip list

Si se quiere conocer sobre un paquete en particular, hay que usar el siguiente comando.


pip show nombre_paquete

> Hay otros administradores de paquetes, como conda que se desarrolló con la distribución de Anaconda, es similar a pip, por lo tanto, no hablaremos mucho aquí, puede encontrar más información leyendo la página (https: //conda.io/docs/user-guide/getting-started.html).

## Instalación desde codigo fuente

Ocasionalmente, se necesita descargar el archivo fuente para algún proyecto que no está en PyPI y luego instalarlo de forma directa. Después de descomprimir el archivo que se descargó, generalmente puede ver que la carpeta contiene un script de configuración *setup.py* y un archivo llamado README, que documenta cómo compilar e instalar el módulo. En la mayoría de los casos, solo necesita ejecutar el siguiente comando desde la terminal para instalar el paquete:


setup.py install