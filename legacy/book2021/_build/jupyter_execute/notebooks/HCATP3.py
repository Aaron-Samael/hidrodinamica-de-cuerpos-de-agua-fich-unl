# Trabajo Práctico 3
## Tipos de Flujos y Método de Diferencias Finitas

## Ejercicios

$\bf{Ejercicio 1}$. Mediante una expansión en serie de Taylor en torno a un punto de equilibrio $x_0 = (x^0_1,x^0_2)$ donde el flujo esta en reposo, es decir $ u = \left .\frac{\partial x}{\partial t}\right\vert_{x_0}=(0,0)$, es posible clasificar una serie de flujos simples acorde a la terminología del algebra lineal. Aplicando la matriz M a un vector arbitrario $x = (x_1; x_2)^T$ clasifique la transformación $Mx=> y$ como reflexión, dilatación, contracción, rotación y/o proyeccion para cada uno de los siguientes 
flujos

$a: A\begin{bmatrix}
1 &  0\\
0 & -1
\end{bmatrix}; 
b: B\begin{bmatrix}
0 &  1\\
1 & 0
\end{bmatrix};
c:C\begin{bmatrix}
1 & 0\\
0 & 1
\end{bmatrix};
d:\Omega\begin{bmatrix}
0 & -1\\
1 & 0
\end{bmatrix};
e:\begin{bmatrix}
0 & 1\\
0 & 0
\end{bmatrix} $




Asuma que las constantes satisfacen A y B > 0; C > 0 y C < 0; $\Omega > 0$. Analice si cada caso representa una solución exacta de las ecuaciones de movimiento y verifique si el campo de velocidades es solenoidal (Opcional: escoja un solo tipo de flujo).

$\bf{Ejercicio 2}$. Escribe un código para resolver la siguiente ecuación de Laplace 2D:

$$\frac{\partial ^2 p}{\partial x^2} + \frac{\partial ^2 p}{\partial y^2} = 0$$

Sabemos cómo discretizar una derivada de segunda de orden. Pero piensa que la ecuación de Laplace tiene las características típicas de los fenómenos de difusión. Por esta razón, tiene que ser discretizado con *diferencias centradas*, de modo que la discretización sea consistente con la física que se desea simular.

Tenga en cuenta que la ecuación de Laplace no tiene una dependencia del tiempo - no hay $p^{n+1}$. En lugar del seguimiento de una onda a través del tiempo, la ecuación de Laplace calcula el estado de equilibrio de un sistema bajo las condiciones de contorno suministradas.

En lugar de calcular como estará el sistema en algún tiempo $t$, se debe resolver de manera  iterativamente por $p_{i,j}^n$ hasta que se llegue a una condición que se especifica. El sistema alcanza el equilibrio sólo cuando el número de iteraciones tiende a $\infty$, pero podemos aproximar el estado de equilibrio iterando hasta que el cambio entre una iteración y la siguiente sea *muy* pequeño.

Utilice esquemas de diferencias centradas de segundo orden en ambas direcciones.

Asuma un estado inicial de $p=0$ en todas partes (toda la grilla). Suponga en este caso una grilla x=0-2 e y=0-1. Entonces definimos las condiciones de contorno de la siguiente manera:

$p=0$ en $x=0$

$p=y$ en $x=2$

$\frac{\partial p}{\partial y}=0$ en $y=0, \ 1$

$\bf{Ejercicio 3}$. Construya en este caso un código para la representación de la ecuación de Poisson


La ecuación de Poisson se obtiene añadiendo un término fuente (*source term*) a la derecha de la ecuación de Laplace:

$$\frac{\partial ^2 p}{\partial x^2} + \frac{\partial ^2 p}{\partial y^2} = b$$

Resuelva la ecuación previa, suponiendo un estado inicial de $p=0$ en todas partes, y aplicando las siguientes condiciones de contorno:

$p=0$ en $x=0, \ 2$ y $y=0, \ 1$

y el término fuente se compone de dos picos iniciales en el interior del dominio, de la siguiente manera:

$b_{i,j}=100$ en $i=nx/4, j=ny/4$

$b_{i,j}=-100$ en $i=nx*3/4, j=ny*3/4$

$b_{i,j}=0$ en el resto de puntos.

