# Condicionantes If-Else
Una **declaración de bifurcación**, **declaración If-Else**, o **declaración If** para abreviar, es una construcción de código que ejecuta bloques de código solo si se cumplen ciertas condiciones. Estas condiciones se representan como expresiones lógicas. A continuación se muestra una construcción de declaración if.

#leemos los paquetes que usaremos en esta sección
import numpy as np

**CONSTRUCCION**: Estructura simple de un condicionante sintaxi If-Else

if expresión_logica:# Cabecera
    bloque de codigo #Cuerpo 

La palabra "if" es una palabra clave. Cuando Python ve una declaración if, determinará si la expresión lógica asociada es verdadera. Si es cierto, entonces se ejecutará el código en *bloque de código*. Si es falso, entonces el código de la sentencia if no se ejecutará. La forma de leer esto es "Si la expresión lógica es verdadera, entonces se ejecuta el bloque de código".

Cuando hay varias condiciones a considerar, puede incluir declaraciones elif; si desea una condición que cubra cualquier otro caso, puede usar una instrucción else.

**ESTRUCTURA**: Sintaxis extendida de If-Else

if expresion logica P:
    bloque de codigo 1
elif expresion logica Q:
    bloque de codigo 2
elif expresion logica R:
    bloque de codigo 3
else:
    bloque de codigo 4

Hagamos un ejemplo

def temperatura_termo(temp, temp_control):
    """
    Define cuando debe apagarse el termo 
    eléctrico en funcion de la temperatura del agua
    author
    date
    :type temp: Int
    :type temp_control: Int
    :rtype: String
    """
    if temp < temp_control:
        status = 'Falta mas calor'
    elif temp == temp_control:
        status = 'A punto'
    else:
        status = 'Muy caliente, Apagar'
    return status

status = temperatura_termo(65,100)
print(status)

status = temperatura_termo(102,100)
print(status)

status = temperatura_termo(100,100)
print(status)

**EJEMPLO** ¿Cuál será el valor de y después de que se ejecute el siguiente script?

x = 3
if x > 1:
    y = 2
elif x > 2:
    y = 4
else:
    y = 0
    
print(y)

Podemos incorporar mas condicionantes

x = 3
if x > 1 and x < 2:
    y = 2
elif x > 2 and x < 4:
    y = 4
else:
    y = 0
    
print(y)

Podemos expresar lo mismoa anterior usando

x = 3
if 1 < x < 2:
    y = 2
elif 2 < x < 4:
    y = 4
else:
    y = 0
print(y)

Una declaración se llama **anidada** si está completamente contenida dentro de otra declaración del mismo tipo que ella misma. Por ejemplo, una **instrucción if anidada** es una instrucción if que está completamente contenida dentro de una cláusula de otra instrucción if.

def condicionante_anidada(x,y):
    """
    Condicionante anidada
    author
    date
    :type x: Int
    :type y: Int
    :rtype: Int
    """
    if x > 2:
        if y < 2:
            out = x + y
        else:
            out = x - y
    else:
        if y > 2:
            out = x*y
        else:
            out = 0
    return out

**¡Nota!** Como antes, Python da el mismo nivel de sangría a cada línea de código dentro de una declaración condicional. La declaración if anidada tiene una sangría más al aumentar cuatro espacios en blanco. Obtendrá un **IndentationError** si la sangría no es correcta, como vimos en la definición de las funciones.

Hay muchas funciones lógicas que están diseñadas para ayudarlo a construir declaraciones de bifurcación. Por ejemplo, puede preguntar si una variable tiene un determinado tipo de datos con la función *isinstance*. También hay funciones que pueden brindarle información sobre matrices de lógicas como *any*, que se calcula como verdadero si algún elemento de una matriz es verdadero, y falso en caso contrario, y *all*, que se calcula como verdadero solo si todos los elementos en una matriz es verdadera.

A veces, es posible que desee diseñar su función para verificar las entradas de una función para asegurarse de que su función se utilizará correctamente. Por ejemplo, la función *mi_suma* en la sección previa Si el usuario ingresa una *list* o una *strings* como una de las variables de entrada, entonces la función arrojará un error o tendrá resultados inesperados. Para evitarlo, puede marcar para indicarle al usuario que la función no se ha utilizado correctamente. Por el momento, solo necesitamos saber que podemos usar la instrucción $ \texttt {raise} $ con una excepción $ \texttt {TypeError} $ para detener la ejecución de una función y lanzar un error con un texto específico.

**EJEMPLOS**: algunos ejemplos de funciones

def mi_suma(a, b, c):
    """
    Calcula la suma de tres numeros
    author
    date
    """
    
    # Check errores
    if not (isinstance(a, (int, float)) \
            or isinstance(b, (int, float)) \
            or isinstance(c, (int, float))):
        raise TypeError('La Entrada debe ser un numero.')
    # Salidas
    return a + b + c

def mi_circ_calc(r, calc):
    """
    Mide la geometria del circulo
    author
    date
    :type r: Int or Float
    :type calc: String
    :rtype: Int or Float
    """
    if calc == 'area':
        return round(np.pi*r**2,4)
    elif calc == 'circunferencia':
        return round(2*np.pi*r,4)

mi_circ_calc(2.5, 'area')

mi_circ_calc(3, 'circunferencia')

**Nota!** Podemos usar varias calculos.

mi_circ_calc(np.array([2, 3, 4]), 'circunferencia')

