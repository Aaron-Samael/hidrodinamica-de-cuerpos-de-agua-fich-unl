# Hidrodinámica de Cuerpos de Agua FICH-UNL
### Curso de postgrado, para Doctorado en Ingeniería. UNL

![giphy](images/untitled.gif)

* Este apunte pretende ser una guía para el alumno que curse la materia de postgrado denominada Hidrodinámica de Cuerpos de Agua. El mismo contiene una serie de notebooks que le permiten implementar algoritmos resolutivos de tópicos que se irán desarrollando en la materia. Para más información recurrir a [Repositorio](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl). Cualquier duda comunicarse con el docente responsable Dr. Lucas Dominguez Ruben [ldominguez@fich.unl.edu.ar](ldominguez@fich.unl.edu.ar).

* Los códigos se encuentran bajo la licencia [Licencia MIT](https://opensource.org/licenses/MIT)*

# Info General del Curso
* Breve presentación de que trata el curso [PRESENTACION](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/presentacioncurso.pdf)
* El respositorio donde se aloja todo es [REPOSITORIO](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl)
* La sala donde se dictarán las clases [SALA](https://meet.jit.si/hidrodinamicadecuerposdeagua)
* El programa esta subido aquí [PROGRAMA](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/programa.pdf)
* Los días de clases serán los Jueves de 9 a 11:30
* Las consultas día a definir
* Las horas de acreditación en el Doctorado de UNL son 60hs
* Los requisitos para la aprobación son la aprobación de los 5 prácticos y una evaluación final.
* Abajo se encuentran los contenidos y notebooks asociados

## Tabla de Contenidos


### [Tema I. Introducción a Python](notebooks/HCA01.00-pythonintro.ipynb)

* [**I.1 Iniciando el camino con Python**](notebooks/HCA01.01-iniciando-el-camino-con-python.ipynb)
* [**I.2 Paquetes y Entornos**](notebooks/HCA01.02-paquetes-y-entornos.ipynb)
* [**I.3 Jupyter Notebook y Lab**](notebooks/HCA01.03-jupyter-notebook-lab.ipynb)
* [**I.4 Expresiones Lógicas y Operadores**](notebooks/HCA01.04-expresiones-logicas-y-operadores.ipynb)
* [**I.5 Estructura de Datos**](notebooks/HCA01.05-estructura-de-datos.ipynb)
* [**I.6 Funciones**](notebooks/HCA01.06-funciones.ipynb)
* [**I.7 Condicionantes**](notebooks/HCA01.07-condicionantes.ipynb)
* [**I.8 Iteraciones. For y While**](notebooks/HCA01.08-iteraciones.ipynb)
* [**I.9 Calculamos?. Librería de computo Científico**](notebooks/HCA01.09-calculamos.ipynb)
* [**I.10 Ploteo de datos 2D y 3D**](notebooks/HCA01.10-plott2D-3D.ipynb)
* [**I.11 Lectura de datos**](notebooks/HCA01.11-read-data.ipynb)


### [Tema II. Mecánica de fluídos](notebooks/HCA02.00-fisica.ipynb)

* II.1 Funciones de variables complejas
* [II.2 Ley de Conservación](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/web/book/notebooks/ref/Ley_de_Conservacion_CH.pdf)
* II.3 Turbulencia
* [***II.3.1 Caos***](notebooks/HCA02.03.01-caos.ipynb)
* [**II.4 Ecuación de Navier Stokes. Resumen**](notebooks/HCA02.04-Navier-Stokes.ipynb)
* [**II.5 Condiciones de borde**](notebooks/HCA02.05-condiciones-de-borde.ipynb)


### [Tema III. Numérica](notebooks/HCA03.00-numerica.ipynb)

* [**III.1 Serie de Taylor**](notebooks/HCA03.01-serie-de-taylor.ipynb)
* [**III.2 Metodo de Diferencias Finitas**](notebooks/HCA03.02-metodos-de-diferencias-finitas.ipynb)
* [**III.3 Metodo de Volumenes Finitos (FVM). Resumen**](notebooks/HCA03.03-metodos-de-volumenes-finitos.ipynb)
* [**III.4 Metodo de Elementos Finitos (FEM). Resumen**](notebooks/HCA03.04-metodo-de-elementos-finitos.ipynb)
* [**III.5 Consistencia, estabilidad y convergencia**](notebooks/HCA03.05-consistencia-estabilidad-convergencia.ipynb)


### [Tema IV. Aplicaciones](notebooks/HCA04.00-aplicaciones.ipynb)
* [**IV.1 Streamline, streakline y pathline**](notebooks/HCA04.01-streamline-streakline-pathline.ipynb)
* [**IV.2 Resolutor de ecuación de Navier-Stokes**](notebooks/HCA04.02-ecuacion-de-navier-stokes.ipynb)
* [**IV.3 Resistencia hidráulica**](notebooks/HCA04.03-resistencia-hidraulica.ipynb)
* [**IV.4 Flujo en canales. TELEMAC2D**](notebooks/HCA04.04-telemac2d.ipynb)
* [**IV.5 Casos de estudio. Modelos Bidimensionales**](notebooks/HCA04.05-casos-de-estudio.ipynb)
* [***IV.5.1 Obstrucción de pila y espigones***](notebooks/HCA04.05.01-case-obstruccion.ipynb)
* [***IV.5.2 Flujo en curvas***](notebooks/HCA04.05.02-flujo-en-curvas.ipynb)
* [***IV.5.3 Flujo en condición de desborde***](notebooks/HCA04.05.03-condición-de-desborde.ipynb)

### Temas adicionales
* [Mas sobre python](notebooks/HCA_A_01.00-mas-sobre-python.ipynb)
* [Notación Tensorial](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/ref/notaciontensorial.pdf)
* [TELEMAC-MASCARET](http://opentelemac.org/)

### [Prácticos]
* [Práctico 1]
* [Práctico 2]
* [Práctico 3]
* [Práctico 4]
* [Práctico 5]

### EXAMEN FINAL
* [Examen HCA]

### Clases
* [Clase 13/04/21](https://youtu.be/1EYZ2yNbdvo)
* [Clase 21/04/21](https://youtu.be/lGt2c-Kk3k4)
* [Clase 28/04/21](https://youtu.be/ZAvDP3kVOlA)
* [Clase 05/05/21](https://youtu.be/DEHJLrsGP4s)
* [Clase 12/05/21](https://youtu.be/OZom6liSVIM)
* [Clase 19/05/21](https://youtu.be/vMTxLnqIJmQ)
* [Clase 26/05/21](https://youtu.be/XVb5z6wdYYQ)
* [Clase 03/06/21](https://youtu.be/Ov2YwqQzdHI)
* Clase 10/06/21
* [Clase 17/06/21](https://youtu.be/EK7zdRDV7r0)
* [Clase 24/06/21](https://youtu.be/7Kk70VQlucA)

## Bibligrafia 

- Batchelor, G.K. (2000), An Introduction to Fluid Dynamics, Cambridge University Press.
- Hirsch, C. (2007), Numerical Computation of Internal and Extenral flows: The fundamentals of Computational Fluid Dynamics. Elsevier.
- [Curso de Python para ciencias e ingenierías](https://github.com/mgaitan/curso-python-cientifico)
- [CFD An Open-Source Approach](https://users.encs.concordia.ca/~bvermeir/book/CFD%20-%20An%20Open-Source%20Approach.pdf)
- Hervouet, J.M. (2007), Hydrodynamics of Free Surface Flows: modeling with the finite element method, John Wiley & Sons.
- Stoker, J.J. (1992), Water Waves: The Mathematical Theory with Applications, Wiley Classics Library.
- Whitham, G.B. (1999), Linear and Nonlinear Waves, John Wiley & Sons.
- Vreugdenhil, C.B. (1994), Numerical Method for Shallow-Water Flow, Springer Science+Business Media, B.V., DOI: 10.1007/978-94-015-8354-1.
- [How to Think Like a Computer Scientist: Learning with Python 3](https://www.ict.ru.ac.za/Resources/cspw/thinkcspy3/thinkcspy3.pdf) (PDF)

### Agradecimientos
* Principalmente al proyecto [Jupyter](https://jupyter.org/).
* Al MSc. Emiliano Lopez por los consejos aportados.


```{toctree}
:hidden:
:titlesonly:


notebooks/HCA01.00-pythonintro.ipynb
notebooks/HCA01.01-iniciando-el-camino-con-python.ipynb
notebooks/HCA01.02-paquetes-y-entornos.ipynb
notebooks/HCA01.03-jupyter-notebook-lab.ipynb
notebooks/HCA01.04-expresiones-logicas-y-operadores.ipynb
notebooks/HCA01.05-estructura-de-datos.ipynb
notebooks/HCA01.06-funciones.ipynb
notebooks/HCA01.07-condicionantes.ipynb
notebooks/HCA01.08-iteraciones.ipynb
notebooks/HCA01.09-calculamos.ipynb
notebooks/HCA01.10-plott2D-3D.ipynb
notebooks/HCA01.11-read-data.ipynb
notebooks/HCA02.00-fisica.ipynb
notebooks/HCA02.02-sistemas-simplificados.ipynb
notebooks/HCA02.04-Navier-Stokes.ipynb
notebooks/HCA03.00-numerica.ipynb
notebooks/HCA03.01-serie-de-taylor.ipynb
notebooks/HCA03.02-metodos-de-diferencias-finitas.ipynb
notebooks/HCA03.03-metodos-de-volumenes-finitos.ipynb
notebooks/HCA03.04-consistencia-estabilidad-convergencia.ipynb
notebooks/HCA04.00-aplicaciones.ipynb
notebooks/HCA04.02-ecuacion-de-navier-stokes.ipynb
notebooks/HCA04.03-resistencia-hidraulica.ipynb
notebooks/HCA04.04-telemac2d.ipynb
notebooks/HCA04.05-casos-de-estudio.ipynb
notebooks/HCA_A_00.ipynb
```
