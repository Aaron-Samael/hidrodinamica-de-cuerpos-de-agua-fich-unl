��?}      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Condicionantes If-Else�h]�h	�Text����Condicionantes If-Else�����}�(h�Condicionantes If-Else��parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h�line�M'�source��xF:\projects\facultad\posgrado\hidrodinamica-de-cuerpos-de-agua-fich-unl\web\book\notebooks\HCA01.08-condicionantes.ipynb�hhubh	�	paragraph���)��}�(hXH  Una **declaración de bifurcación**, **declaración If-Else**, o **declaración If** para abreviar, es una construcción de código que ejecuta bloques de código solo si se cumplen ciertas condiciones. Estas condiciones se representan como expresiones lógicas. A continuación se muestra una construcción de declaración if.�h]�(h�Una �����}�(h�Una �hh/hhh+Nh*Nubh	�strong���)��}�(h�declaración de bifurcación�h]�h�declaración de bifurcación�����}�(h�declaración de bifurcación�hh:hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h8h*M'h+h,hh/hhubh�, �����}�(h�, �hh/hhh+Nh*Nubh9)��}�(h�declaración If-Else�h]�h�declaración If-Else�����}�(h�declaración If-Else�hhNhhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h8h*M'h+h,hh/hhubh�, o �����}�(h�, o �hh/hhh+Nh*Nubh9)��}�(h�declaración If�h]�h�declaración If�����}�(h�declaración If�hhbhhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h8h*M'h+h,hh/hhubh�� para abreviar, es una construcción de código que ejecuta bloques de código solo si se cumplen ciertas condiciones. Estas condiciones se representan como expresiones lógicas. A continuación se muestra una construcción de declaración if.�����}�(h�� para abreviar, es una construcción de código que ejecuta bloques de código solo si se cumplen ciertas condiciones. Estas condiciones se representan como expresiones lógicas. A continuación se muestra una construcción de declaración if.�hh/hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*M'h+h,hhhhub�myst_nb.nodes��CellNode���)��}�(hhh]�h|�CellInputNode���)��}�(hhh]�h	�literal_block���)��}�(h�E#leemos los paquetes que usaremos en esta sección
import numpy as np�h]�h�E#leemos los paquetes que usaremos en esta sección
import numpy as np�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve��language��ipython3�uh)h�hh�hhh+h,h*K ubah}�(h]�h!]��
cell_input�ah#]�h%]�h']�uh)h�h*M"Nh+h,hhhhubah}�(h]�h!]��cell�ah#]�h%]�h']��	cell_type��code�uh)h}hhhhh+h,h*K ubh.)��}�(h�G**CONSTRUCCION**: Estructura simple de un condicionante sintaxi If-Else�h]�(hh����}�(hhhh�hhh+Nh*Nubh9)��}�(h�CONSTRUCCION�h]�h�CONSTRUCCION�����}�(h�CONSTRUCCION�hh�hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h8h*M2uh+h,hh�hhubh�7: Estructura simple de un condicionante sintaxi If-Else�����}�(h�7: Estructura simple de un condicionante sintaxi If-Else�hh�hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*M2uh+h,hhhhubh�)��}�(h�+if expresión_logica:
    bloque de codigo
�h]�h�+if expresión_logica:
    bloque de codigo
�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']��language��
pseudocode�h�h�uh)h�h*M4uh+h,hhhhubh.)��}�(hXu  La palabra "if" es una palabra clave. Cuando Python ve una declaración if, determinará si la expresión lógica asociada es verdadera. Si es cierto, entonces se ejecutará el código en *bloque de código*. Si es falso, entonces el código de la sentencia if no se ejecutará. La forma de leer esto es "Si la expresión lógica es verdadera, entonces bloquee el código".�h]�(h��La palabra “if” es una palabra clave. Cuando Python ve una declaración if, determinará si la expresión lógica asociada es verdadera. Si es cierto, entonces se ejecutará el código en �����}�(h��La palabra "if" es una palabra clave. Cuando Python ve una declaración if, determinará si la expresión lógica asociada es verdadera. Si es cierto, entonces se ejecutará el código en �hh�hhh+Nh*Nubh	�emphasis���)��}�(h�bloque de código�h]�h�bloque de código�����}�(h�bloque de código�hh�hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*MB�h+h,hh�hhubh��. Si es falso, entonces el código de la sentencia if no se ejecutará. La forma de leer esto es “Si la expresión lógica es verdadera, entonces bloquee el código”.�����}�(h��. Si es falso, entonces el código de la sentencia if no se ejecutará. La forma de leer esto es "Si la expresión lógica es verdadera, entonces bloquee el código".�hh�hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*MB�h+h,hhhhubh.)��}�(h��Cuando hay varias condiciones a considerar, puede incluir declaraciones elif; si desea una condición que cubra cualquier otro caso, puede usar una instrucción else.�h]�h��Cuando hay varias condiciones a considerar, puede incluir declaraciones elif; si desea una condición que cubra cualquier otro caso, puede usar una instrucción else.�����}�(hj  hj  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h-h*MD�h+h,hhhhubh.)��}�(h�-**ESTRUCTURA**: Sintaxis extendida de If-Else�h]�(hh����}�(hhhj  hhh+Nh*Nubh9)��}�(h�
ESTRUCTURA�h]�h�
ESTRUCTURA�����}�(h�
ESTRUCTURA�hj  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h8h*MR�h+h,hj  hhubh�: Sintaxis extendida de If-Else�����}�(h�: Sintaxis extendida de If-Else�hj  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*MR�h+h,hhhhubh�)��}�(h��if expresion logica P:
    bloque de codigo 1
elif expresion logica Q:
    bloque de codigo 2
elif expresion logica R:
    bloque de codigo 3
else:
    bloque de codigo 4
   
�h]�h��if expresion logica P:
    bloque de codigo 1
elif expresion logica Q:
    bloque de codigo 2
elif expresion logica R:
    bloque de codigo 3
else:
    bloque de codigo 4
   
�����}�(hhhj0  ubah}�(h]�h!]�h#]�h%]�h']��language��
pseudocode�h�h�uh)h�h*MT�h+h,hhhhubh.)��}�(hX1  En el código anterior, Python primero verificará si $ \ textit {P} $ es verdadero. Si $ \ textit {P} $ es verdadero, entonces se ejecutará el bloque de código 1, y luego $ \ textit {if-statement} $ terminará. En otras palabras, Python *no* comprobará el resto de las declaraciones una vez que llegue a una declaración verdadera. Sin embargo, si $ \ textit {P} $ es falso, Python comprobará si $ \ textit {Q} $ es verdadero. Si $ \ textit {Q} $ es verdadero, entonces se ejecutará el bloque de código 2 y la sentencia if terminará. Si es falso, se ejecutará $ \ textit {R} $, y así sucesivamente. Si $ \ textit {P} $, $ \ textit {Q} $ y $ \ textit {R} $ son todos falsos, entonces se ejecutará el bloque de código 4. Puede tener cualquier número de declaraciones elif (o ninguna) siempre que haya al menos una declaración if (la primera declaración). No necesita una declaración else, pero puede tener como máximo una declaración else. Las expresiones lógicas después de if y elif (es decir, como P, Q y R) se denominarán declaraciones condicionales.�h]�(h�6En el código anterior, Python primero verificará si �����}�(h�6En el código anterior, Python primero verificará si �hj@  hhh+Nh*Nubh	�math���)��}�(h� \ textit {P} �h]�h� \ textit {P} �����}�(hhhjK  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)jI  h*Mb�h+h,hj@  hhubh� es verdadero. Si �����}�(h� es verdadero. Si �hj@  hhh+Nh*NubjJ  )��}�(h� \ textit {P} �h]�h� \ textit {P} �����}�(hhhj^  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)jI  h*Mb�h+h,hj@  hhubh�F es verdadero, entonces se ejecutará el bloque de código 1, y luego �����}�(h�F es verdadero, entonces se ejecutará el bloque de código 1, y luego �hj@  hhh+Nh*NubjJ  )��}�(h� \ textit {if-statement} �h]�h� \ textit {if-statement} �����}�(hhhjq  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)jI  h*Mb�h+h,hj@  hhubh�' terminará. En otras palabras, Python �����}�(h�' terminará. En otras palabras, Python �hj@  hhh+Nh*Nubh�)��}�(h�no�h]�h�no�����}�(h�no�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*Mb�h+h,hj@  hhubh�l comprobará el resto de las declaraciones una vez que llegue a una declaración verdadera. Sin embargo, si �����}�(h�l comprobará el resto de las declaraciones una vez que llegue a una declaración verdadera. Sin embargo, si �hj@  hhh+Nh*NubjJ  )��}�(h� \ textit {P} �h]�h� \ textit {P} �����}�(hhhj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)jI  h*Mb�h+h,hj@  hhubh�! es falso, Python comprobará si �����}�(h�! es falso, Python comprobará si �hj@  hhh+Nh*NubjJ  )��}�(h� \ textit {Q} �h]�h� \ textit {Q} �����}�(hhhj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)jI  h*Mb�h+h,hj@  hhubh� es verdadero. Si �����}�(hj]  hj@  hhh+h,h*K ubjJ  )��}�(h� \ textit {Q} �h]�h� \ textit {Q} �����}�(hhhj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)jI  h*Mb�h+h,hj@  hhubh�v es verdadero, entonces se ejecutará el bloque de código 2 y la sentencia if terminará. Si es falso, se ejecutará �����}�(h�v es verdadero, entonces se ejecutará el bloque de código 2 y la sentencia if terminará. Si es falso, se ejecutará �hj@  hhh+Nh*NubjJ  )��}�(h� \ textit {R} �h]�h� \ textit {R} �����}�(hhhj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)jI  h*Mb�h+h,hj@  hhubh�, y así sucesivamente. Si �����}�(h�, y así sucesivamente. Si �hj@  hhh+Nh*NubjJ  )��}�(h� \ textit {P} �h]�h� \ textit {P} �����}�(hhhj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)jI  h*Mb�h+h,hj@  hhubh�, �����}�(h�, �hj@  hhh+Nh*NubjJ  )��}�(h� \ textit {Q} �h]�h� \ textit {Q} �����}�(hhhj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)jI  h*Mb�h+h,hj@  hhubh� y �����}�(h� y �hj@  hhh+Nh*NubjJ  )��}�(h� \ textit {R} �h]�h� \ textit {R} �����}�(hhhj	  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)jI  h*Mb�h+h,hj@  hhubhX�   son todos falsos, entonces se ejecutará el bloque de código 4. Puede tener cualquier número de declaraciones elif (o ninguna) siempre que haya al menos una declaración if (la primera declaración). No necesita una declaración else, pero puede tener como máximo una declaración else. Las expresiones lógicas después de if y elif (es decir, como P, Q y R) se denominarán declaraciones condicionales.�����}�(hX�   son todos falsos, entonces se ejecutará el bloque de código 4. Puede tener cualquier número de declaraciones elif (o ninguna) siempre que haya al menos una declaración if (la primera declaración). No necesita una declaración else, pero puede tener como máximo una declaración else. Las expresiones lógicas después de if y elif (es decir, como P, Q y R) se denominarán declaraciones condicionales.�hj@  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*Mb�h+h,hhhhubh.)��}�(h�Hagamos un ejemplo�h]�h�Hagamos un ejemplo�����}�(hj$  hj"  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h-h*Jr h+h,hhhhubh~)��}�(hhh]�h�)��}�(hhh]�h�)��}�(hX�  def temperatura_termo(temp, temp_control):
    """
    Define cuando debe apagarse el termo 
    eléctrico en funcion de la temperatura del agua
    author
    date
    :type temp: Int
    :type temp_control: Int
    :rtype: String
    """
    if temp < temp_control:
        status = 'Falta mas calor'
    elif temp == temp_control:
        status = 'A punto'
    else:
        status = 'Muy caliente, Apagar'
    return status�h]�hX�  def temperatura_termo(temp, temp_control):
    """
    Define cuando debe apagarse el termo 
    eléctrico en funcion de la temperatura del agua
    author
    date
    :type temp: Int
    :type temp_control: Int
    :rtype: String
    """
    if temp < temp_control:
        status = 'Falta mas calor'
    elif temp == temp_control:
        status = 'A punto'
    else:
        status = 'Muy caliente, Apagar'
    return status�����}�(hhhj6  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)h�hj3  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�h*J�8 h+h,hj0  hhubah}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)h}hhhhh+h,h*K ubh~)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(h�0status = temperatura_termo(65,100)
print(status)�h]�h�0status = temperatura_termo(65,100)
print(status)�����}�(hhhjX  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)h�hjU  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�h*J�_ h+h,hjR  hhubh|�CellOutputNode���)��}�(hhh]�h|�CellOutputBundleNode���)��}�(�_outputs�]��nbformat.notebooknode��NotebookNode���)��(�name��stdout��output_type��stream��text��Falta mas calor
�u}��	_allownew��sba�	_renderer��default��	_metadata�jy  )��}�j�  �sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)jq  h*J�_ h+h,hjn  hhubah}�(h]�h!]��cell_output�ah#]�h%]�h']�uh)jl  hjR  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)h}hhhhh+h,h*K ubh~)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(h�1status = temperatura_termo(102,100)
print(status)�h]�h�1status = temperatura_termo(102,100)
print(status)�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)h�hj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�h*J�� h+h,hj�  hhubjm  )��}�(hhh]�jr  )��}�(ju  ]�jy  )��(j{  �stdout�j}  �stream�j  �Muy caliente, Apagar
�u}�j�  �sbaj�  j�  j�  jy  )��}�j�  �sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)jq  h*J�� h+h,hj�  hhubah}�(h]�h!]�j�  ah#]�h%]�h']�uh)jl  hj�  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)h}hhhhh+h,h*K ubh.)��}�(h�W**EJEMPLO** ¿Cuál será el valor de y después de que se ejecute el siguiente script?�h]�(hh����}�(hhhj�  hhh+Nh*Nubh9)��}�(h�EJEMPLO�h]�h�EJEMPLO�����}�(h�EJEMPLO�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h8h*J�� h+h,hj�  hhubh�L ¿Cuál será el valor de y después de que se ejecute el siguiente script?�����}�(h�L ¿Cuál será el valor de y después de que se ejecute el siguiente script?�hj�  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*J�� h+h,hhhhubh~)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(h�Hx = 3
if x > 1:
    y = 2
elif x > 2:
    y = 4
else:
    y = 0
print(y)�h]�h�Hx = 3
if x > 1:
    y = 2
elif x > 2:
    y = 4
else:
    y = 0
print(y)�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)h�hj   hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�h*J�� h+h,hj�  hhubjm  )��}�(hhh]�jr  )��}�(ju  ]�jy  )��(j{  �stdout�j}  �stream�j  �2
�u}�j�  �sbaj�  j�  j�  jy  )��}�j�  �sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)jq  h*J�� h+h,hj  hhubah}�(h]�h!]�j�  ah#]�h%]�h']�uh)jl  hj�  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)h}hhhhh+h,h*K ubh.)��}�(h�%Podemos incorporar mas condicionantes�h]�h�%Podemos incorporar mas condicionantes�����}�(hj<  hj:  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h-h*J�� h+h,hhhhubh~)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(h�\x = 3
if x > 1 and x < 2:
    y = 2
elif x > 2 and x < 4:
    y = 4
else:
    y = 0
print(y)�h]�h�\x = 3
if x > 1 and x < 2:
    y = 2
elif x > 2 and x < 4:
    y = 4
else:
    y = 0
print(y)�����}�(hhhjN  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)h�hjK  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�h*J�" h+h,hjH  hhubjm  )��}�(hhh]�jr  )��}�(ju  ]�jy  )��(j{  �stdout�j}  �stream�j  �4
�u}�j�  �sbaj�  j�  j�  jy  )��}�j�  �sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)jq  h*J�" h+h,hjb  hhubah}�(h]�h!]�j�  ah#]�h%]�h']�uh)jl  hjH  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)h}hhhhh+h,h*K ubh.)��}�(h�*Podemos expresar lo mismoa anterior usando�h]�h�*Podemos expresar lo mismoa anterior usando�����}�(hj�  hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h-h*J�I h+h,hhhhubh~)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(h�Px = 3
if 1 < x < 2:
    y = 2
elif 2 < x < 4:
    y = 4
else:
    y = 0
print(y)�h]�h�Px = 3
if 1 < x < 2:
    y = 2
elif 2 < x < 4:
    y = 4
else:
    y = 0
print(y)�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)h�hj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�h*Jq h+h,hj�  hhubjm  )��}�(hhh]�jr  )��}�(ju  ]�jy  )��(j{  �stdout�j}  �stream�j  �4
�u}�j�  �sbaj�  j�  j�  jy  )��}�j�  �sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)jq  h*Jq h+h,hj�  hhubah}�(h]�h!]�j�  ah#]�h%]�h']�uh)jl  hj�  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)h}hhhhh+h,h*K ubh.)��}�(hX  Una declaración se llama **anidada** si está completamente contenida dentro de otra declaración del mismo tipo que ella misma. Por ejemplo, una **instrucción if anidada** es una instrucción if que está completamente contenida dentro de una cláusula de otra instrucción if.�h]�(h�Una declaración se llama �����}�(h�Una declaración se llama �hj�  hhh+Nh*Nubh9)��}�(h�anidada�h]�h�anidada�����}�(h�anidada�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h8h*J� h+h,hj�  hhubh�n si está completamente contenida dentro de otra declaración del mismo tipo que ella misma. Por ejemplo, una �����}�(h�n si está completamente contenida dentro de otra declaración del mismo tipo que ella misma. Por ejemplo, una �hj�  hhh+Nh*Nubh9)��}�(h�instrucción if anidada�h]�h�instrucción if anidada�����}�(h�instrucción if anidada�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h8h*J� h+h,hj�  hhubh�j es una instrucción if que está completamente contenida dentro de una cláusula de otra instrucción if.�����}�(h�j es una instrucción if que está completamente contenida dentro de una cláusula de otra instrucción if.�hj�  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*J� h+h,hhhhubh~)��}�(hhh]�h�)��}�(hhh]�h�)��}�(hXP  def condicionante_anidada(x,y):
    """
    Condicionante anidada
    author
    date
    :type x: Int
    :type y: Int
    :rtype: Int
    """
    if x > 2:
        if y < 2:
            out = x + y
        else:
            out = x - y
    else:
        if y > 2:
            out = x*y
        else:
            out = 0
    return out�h]�hXP  def condicionante_anidada(x,y):
    """
    Condicionante anidada
    author
    date
    :type x: Int
    :type y: Int
    :rtype: Int
    """
    if x > 2:
        if y < 2:
            out = x + y
        else:
            out = x - y
    else:
        if y > 2:
            out = x*y
        else:
            out = 0
    return out�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)h�hj
  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�h*J"� h+h,hj  hhubah}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)h}hhhhh+h,h*K ubh.)��}�(hXG  **¡Nota!** Como antes, Python da el mismo nivel de sangría a cada línea de código dentro de una declaración condicional. La declaración if anidada tiene una sangría más al aumentar cuatro espacios en blanco. Obtendrá un **IndentationError** si la sangría no es correcta, como vimos en la definición de las funciones.�h]�(hh����}�(hhhj)  hhh+Nh*Nubh9)��}�(h�¡Nota!�h]�h�¡Nota!�����}�(h�¡Nota!�hj0  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h8h*J2� h+h,hj)  hhubh�� Como antes, Python da el mismo nivel de sangría a cada línea de código dentro de una declaración condicional. La declaración if anidada tiene una sangría más al aumentar cuatro espacios en blanco. Obtendrá un �����}�(h�� Como antes, Python da el mismo nivel de sangría a cada línea de código dentro de una declaración condicional. La declaración if anidada tiene una sangría más al aumentar cuatro espacios en blanco. Obtendrá un �hj)  hhh+Nh*Nubh9)��}�(h�IndentationError�h]�h�IndentationError�����}�(h�IndentationError�hjD  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h8h*J2� h+h,hj)  hhubh�N si la sangría no es correcta, como vimos en la definición de las funciones.�����}�(h�N si la sangría no es correcta, como vimos en la definición de las funciones.�hj)  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*J2� h+h,hhhhubh~)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(h�all([1, 1, 0])�h]�h�all([1, 1, 0])�����}�(hhhjd  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)h�hja  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�h*JB h+h,hj^  hhubjm  )��}�(hhh]�jr  )��}�(ju  ]�jy  )��(�data�jy  )���
text/plain��False�s}�j�  �sb�execution_count�K�metadata�jy  )��}�j�  �sbj}  �execute_result�u}�j�  �sbaj�  j�  j�  jy  )��}�j�  �sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)jq  h*JB h+h,hjx  hhubah}�(h]�h!]�j�  ah#]�h%]�h']�uh)jl  hj^  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)h}hhhhh+h,h*K ubh.)��}�(hX  Hay muchas funciones lógicas que están diseñadas para ayudarlo a construir declaraciones de bifurcación. Por ejemplo, puede preguntar si una variable tiene un determinado tipo de datos con la función *isinstance*. También hay funciones que pueden brindarle información sobre matrices de lógicas como *any*, que se calcula como verdadero si algún elemento de una matriz es verdadero, y falso en caso contrario, y *all *, que se calcula como verdadero solo si todos los elementos en una matriz es verdadera.�h]�(h��Hay muchas funciones lógicas que están diseñadas para ayudarlo a construir declaraciones de bifurcación. Por ejemplo, puede preguntar si una variable tiene un determinado tipo de datos con la función �����}�(h��Hay muchas funciones lógicas que están diseñadas para ayudarlo a construir declaraciones de bifurcación. Por ejemplo, puede preguntar si una variable tiene un determinado tipo de datos con la función �hj�  hhh+Nh*Nubh�)��}�(h�
isinstance�h]�h�
isinstance�����}�(h�
isinstance�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*JR4 h+h,hj�  hhubh�[. También hay funciones que pueden brindarle información sobre matrices de lógicas como �����}�(h�[. También hay funciones que pueden brindarle información sobre matrices de lógicas como �hj�  hhh+Nh*Nubh�)��}�(h�any�h]�h�any�����}�(h�any�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*JR4 h+h,hj�  hhubh��, que se calcula como verdadero si algún elemento de una matriz es verdadero, y falso en caso contrario, y *all *, que se calcula como verdadero solo si todos los elementos en una matriz es verdadera.�����}�(h��, que se calcula como verdadero si algún elemento de una matriz es verdadero, y falso en caso contrario, y *all *, que se calcula como verdadero solo si todos los elementos en una matriz es verdadera.�hj�  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*JR4 h+h,hhhhubh.)��}�(hX�  A veces, es posible que desee diseñar su función para verificar las entradas de una función para asegurarse de que su función se utilizará correctamente. Por ejemplo, la función *mi_suma* en la sección previa Si el usuario ingresa una *list* o una *strings* como una de las variables de entrada, entonces la función arrojará un error o tendrá resultados inesperados. Para evitarlo, puede marcar para indicarle al usuario que la función no se ha utilizado correctamente. Por el momento, solo necesitamos saber que podemos usar la instrucción $ \ texttt {raise} $ con una excepción $ \ texttt {TypeError} $ para detener la ejecución de una función y lanzar un error con un texto específico.�h]�(h��A veces, es posible que desee diseñar su función para verificar las entradas de una función para asegurarse de que su función se utilizará correctamente. Por ejemplo, la función �����}�(h��A veces, es posible que desee diseñar su función para verificar las entradas de una función para asegurarse de que su función se utilizará correctamente. Por ejemplo, la función �hj�  hhh+Nh*Nubh�)��}�(h�mi_suma�h]�h�mi_suma�����}�(h�mi_suma�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*JT4 h+h,hj�  hhubh�1 en la sección previa Si el usuario ingresa una �����}�(h�1 en la sección previa Si el usuario ingresa una �hj�  hhh+Nh*Nubh�)��}�(h�list�h]�h�list�����}�(h�list�hj�  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*JT4 h+h,hj�  hhubh� o una �����}�(h� o una �hj�  hhh+Nh*Nubh�)��}�(h�strings�h]�h�strings�����}�(h�strings�hj
  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h�h*JT4 h+h,hj�  hhubhX!   como una de las variables de entrada, entonces la función arrojará un error o tendrá resultados inesperados. Para evitarlo, puede marcar para indicarle al usuario que la función no se ha utilizado correctamente. Por el momento, solo necesitamos saber que podemos usar la instrucción �����}�(hX!   como una de las variables de entrada, entonces la función arrojará un error o tendrá resultados inesperados. Para evitarlo, puede marcar para indicarle al usuario que la función no se ha utilizado correctamente. Por el momento, solo necesitamos saber que podemos usar la instrucción �hj�  hhh+Nh*NubjJ  )��}�(h� \ texttt {raise} �h]�h� \ texttt {raise} �����}�(hhhj  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)jI  h*JT4 h+h,hj�  hhubh� con una excepción �����}�(h� con una excepción �hj�  hhh+Nh*NubjJ  )��}�(h� \ texttt {TypeError} �h]�h� \ texttt {TypeError} �����}�(hhhj1  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)jI  h*JT4 h+h,hj�  hhubh�W para detener la ejecución de una función y lanzar un error con un texto específico.�����}�(h�W para detener la ejecución de una función y lanzar un error con un texto específico.�hj�  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*JT4 h+h,hhhhubh.)��}�(h�+**EJEMPLOS**: algunos ejemplos de funciones�h]�(hh����}�(hhhjJ  hhh+Nh*Nubh9)��}�(h�EJEMPLOS�h]�h�EJEMPLOS�����}�(h�EJEMPLOS�hjQ  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h8h*Jb[ h+h,hjJ  hhubh�: algunos ejemplos de funciones�����}�(h�: algunos ejemplos de funciones�hjJ  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*Jb[ h+h,hhhhubh~)��}�(hhh]�h�)��}�(hhh]�h�)��}�(hXW  def mi_suma(a, b, c):
    """
    Calcula la suma de tres numeros
    author
    date
    """
    
    # Check errores
    if not (isinstance(a, (int, float)) \
            or isinstance(b, (int, float)) \
            or isinstance(c, (int, float))):
        raise TypeError('La Entrada debe ser un numero.')
    # Salidas
    return a + b + c�h]�hXW  def mi_suma(a, b, c):
    """
    Calcula la suma de tres numeros
    author
    date
    """
    
    # Check errores
    if not (isinstance(a, (int, float)) \
            or isinstance(b, (int, float)) \
            or isinstance(c, (int, float))):
        raise TypeError('La Entrada debe ser un numero.')
    # Salidas
    return a + b + c�����}�(hhhjq  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)h�hjn  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�h*Jr� h+h,hjk  hhubah}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)h}hhhhh+h,h*K ubh~)��}�(hhh]�h�)��}�(hhh]�h�)��}�(hX  def mi_circ_calc(r, calc):
    """
    Mide la geometria del circulo
    author
    date
    :type r: Int or Float
    :type calc: String
    :rtype: Int or Float
    """
    if calc == 'area':
        return np.pi*r**2
    elif calc == 'circunferencia':
        return 2*np.pi*r�h]�hX  def mi_circ_calc(r, calc):
    """
    Mide la geometria del circulo
    author
    date
    :type r: Int or Float
    :type calc: String
    :rtype: Int or Float
    """
    if calc == 'area':
        return np.pi*r**2
    elif calc == 'circunferencia':
        return 2*np.pi*r�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)h�hj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�h*J�� h+h,hj�  hhubah}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)h}hhhhh+h,h*K ubh~)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(h�mi_circ_calc(2.5, 'area')�h]�h�mi_circ_calc(2.5, 'area')�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)h�hj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�h*J�� h+h,hj�  hhubjm  )��}�(hhh]�jr  )��}�(ju  ]�jy  )��(j  jy  )��j�  �19.634954084936208�s}�j�  �sbj�  Kj�  jy  )��}�j�  �sbj}  �execute_result�u}�j�  �sbaj�  j�  j�  jy  )��}�j�  �sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)jq  h*J�� h+h,hj�  hhubah}�(h]�h!]�j�  ah#]�h%]�h']�uh)jl  hj�  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)h}hhhhh+h,h*K ubh~)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(h�!mi_circ_calc(3, 'circunferencia')�h]�h�!mi_circ_calc(3, 'circunferencia')�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)h�hj�  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�h*J�� h+h,hj�  hhubjm  )��}�(hhh]�jr  )��}�(ju  ]�jy  )��(j  jy  )��j�  �18.84955592153876�s}�j�  �sbj�  Kj�  jy  )��}�j�  �sbj}  �execute_result�u}�j�  �sbaj�  j�  j�  jy  )��}�j�  �sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)jq  h*J�� h+h,hj	  hhubah}�(h]�h!]�j�  ah#]�h%]�h']�uh)jl  hj�  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)h}hhhhh+h,h*K ubh.)��}�(h�'**Nota!** Podemos usar varias calculos.�h]�(hh����}�(hhhj/  hhh+Nh*Nubh9)��}�(h�Nota!�h]�h�Nota!�����}�(h�Nota!�hj6  hhh+Nh*Nubah}�(h]�h!]�h#]�h%]�h']�uh)h8h*J� h+h,hj/  hhubh� Podemos usar varias calculos.�����}�(h� Podemos usar varias calculos.�hj/  hhh+Nh*Nubeh}�(h]�h!]�h#]�h%]�h']�uh)h-h*J� h+h,hhhhubh~)��}�(hhh]�(h�)��}�(hhh]�h�)��}�(h�3mi_circ_calc(np.array([2, 3, 4]), 'circunferencia')�h]�h�3mi_circ_calc(np.array([2, 3, 4]), 'circunferencia')�����}�(hhhjV  ubah}�(h]�h!]�h#]�h%]�h']�h�h�h�h�uh)h�hjS  hhh+h,h*K ubah}�(h]�h!]�h�ah#]�h%]�h']�uh)h�h*J�E h+h,hjP  hhubjm  )��}�(hhh]�jr  )��}�(ju  ]�jy  )��(j  jy  )��j�  �.array([12.56637061, 18.84955592, 25.13274123])�s}�j�  �sbj�  Kj�  jy  )��}�j�  �sbj}  �execute_result�u}�j�  �sbaj�  j�  j�  jy  )��}�j�  �sbhhh]�h}�(h]�h!]�h#]�h%]�h']��output_count�Kuh)jq  h*J�E h+h,hjj  hhubah}�(h]�h!]�j�  ah#]�h%]�h']�uh)jl  hjP  hhh+h,h*K ubeh}�(h]�h!]�h�ah#]�h%]�h']��	cell_type��code�uh)h}hhhhh+h,h*K ubeh}�(h]��condicionantes-if-else�ah!]�h#]��condicionantes if-else�ah%]�h']�uh)h
h*M'h+h,hhhhubah}�(h]�h!]�h#]�h%]�h']��source�h,uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�j�  j�  s�	nametypes�}�j�  Nsh}�j�  hs�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhh�fm_substitutions�}�ub.